import GlobalLoader from "@/components/modules/globalLoader";
import { FC, ReactNode, createContext, useState } from "react";

type GlobalContextType = {
  popularData: any;
  setPopularData: (popularData: any) => void;
  getpopularData: () => void;
  headerHeight: any;
  isglobalLoaderActive: any;
  setIsglobalLoaderActive: (isglobalLoaderActive: any) => void;
};

export const GlobalContext = createContext<GlobalContextType>({
  popularData: undefined,
  setPopularData: () => {},
  getpopularData: () => {},
  headerHeight: null,
  isglobalLoaderActive: false,
  setIsglobalLoaderActive: () => {},
});

const GlobalProvider: FC<{ children: ReactNode; headerHeight: any }> = ({
  children,
  headerHeight,
}) => {
  const [popularData, setPopularData] = useState(null);
  const [isglobalLoaderActive, setIsglobalLoaderActive] = useState(false);
  const getpopularData = async () => {};
  const globalContextValue = {
    popularData,
    setPopularData,
    getpopularData,
    headerHeight,
    isglobalLoaderActive,
    setIsglobalLoaderActive,
  };

  return (
    <GlobalContext.Provider value={globalContextValue}>
      {isglobalLoaderActive && <GlobalLoader />}
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalProvider;
