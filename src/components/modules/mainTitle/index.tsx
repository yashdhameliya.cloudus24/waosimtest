import React from "react";
import styles from "./mainTitle.module.scss";
const VectorIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/vector.svg";

const MainTitle = ({ text }: any) => {
  return (
    <div className={styles.mainTitleContainer}>
      <h2
        dangerouslySetInnerHTML={{
          __html: text,
        }}
      />

      <div className={styles.botomSpiralLine}>
        <img src={VectorIcon} alt="Brand Logo" />
      </div>
    </div>
  );
};

export default MainTitle;
