import React, { useEffect, useRef } from "react";
import styles from "./modal.module.scss";
import classNames from "classnames";
import { addClassToDocumentElement, removeClassToDocumentElement } from "@/utils";
import { useOnClickOutside } from "@/utils/useHooks";
// import { CloseIcon } from "../../../assets/images/icons";

interface ModelProps {
  closePopupModel: () => void;
  isOpen: boolean;
  children: any;
  isOutSideClick?: boolean;
}

const Modal: React.FC<ModelProps> = ({ children, isOpen, closePopupModel, isOutSideClick }: ModelProps) => {
  const modalRef = useRef(null);

  const handleOnClose = () => {
    removeClassToDocumentElement("no-scroll");
    closePopupModel();
  };

  useEffect(() => {
    if (isOpen) {
      addClassToDocumentElement("no-scroll");
    }
  }, [isOpen]);

  useOnClickOutside(modalRef, isOutSideClick ? handleOnClose : () => {});

  return (
    <div className={classNames(styles.modalWrapper, isOpen ? styles.openModalWrapper : "")}>
      <div className={styles.modal} ref={modalRef}>
        <div className={styles.closeIconDiv} onClick={handleOnClose}>
          {/* <CloseIcon fill="#032715" /> */}
        </div>
        {children}
      </div>
    </div>
  );
};

export default Modal;
