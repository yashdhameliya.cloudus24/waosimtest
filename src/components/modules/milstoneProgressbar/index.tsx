import React, { useEffect, useState } from "react";
import { Circle } from "rc-progress";
import styles from "./milstoneProgressbar.module.scss";
import classNames from "classnames";

const FullProgressBar = ({isText = false}:any) => {
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    var count = 25;
    const timer = setInterval(() => {
      setProgress((prevProgress) => {
        if (prevProgress >= count) {
          clearInterval(timer);
          return count;
        }
        return prevProgress + 1;
      });
    }, 50);
    return () => clearInterval(timer);
  }, []);

  return (
    <div>
      <div className={styles.progressBarContainer}>
        <Circle
          percent={progress}
          trailWidth={10}
          strokeWidth={10}
          strokeColor="#00897F"
          className={styles.CircleProgress}
          trailColor="#E6E9F0"
        />
        <div className={classNames(styles.progressBarText,!isText && styles.progressBar)}
          style={{
      
          }}
        >
          {`${progress}%`}
        </div>
      </div>
    </div>
  );
};

export default FullProgressBar;
