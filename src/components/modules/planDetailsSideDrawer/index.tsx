import React, { useRef } from "react";
import styles from "./planDetailsSideDrawer.module.scss";
import { CopyIcon, MyPlanActiveIcon } from "@/assets/images/icons";
import classNames from "classnames";
import { toastError, toastSuccess } from "@/utils";
const plan = "https://stagecdn.waosim.com/landingpage/assets/icons/order_plan.svg";
const validity = "https://stagecdn.waosim.com/landingpage/assets/icons/order_validity.svg";
const data = "https://stagecdn.waosim.com/landingpage/assets/icons/order_data.svg";
const cpuSetting = "https://stagecdn.waosim.com/landingpage/assets/icons/cpuSetting.svg";
const downloadIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/downloadIcon.svg";
const downloadIconOrange = "https://stagecdn.waosim.com/landingpage/assets/icons/downloadIconOrange.svg";
const shareIconOrange = "https://stagecdn.waosim.com/landingpage/assets/icons/shareIconOrange.svg";
const shareIconOrangeImage = "https://stagecdn.waosim.com/landingpage/assets/icons/shareIconOrangeImage.svg";
const qrCodeLogo = "https://stagecdn.waosim.com/landingpage/assets/qrCodeLogo.png";
import { QRCode } from "react-qrcode-logo";
import Image from "next/image";
var htmlToImage = require("html-to-image");

const PlanDetailsSideDrawer = ({ selectedPlanDetailsData }: any) => {
  const planOrderArray = [
    {
      plan_img: plan,
      plan_detail: "Plan",
      plan_dataInfo: selectedPlanDetailsData?.name || "",
      plan_alt: "Plan",
    },
    {
      plan_img: validity,
      plan_detail: "Validity",
      plan_dataInfo: `${selectedPlanDetailsData?.plan_days} Days` || "",
      plan_alt: "Validity",
    },
    {
      plan_img: data,
      plan_detail: "Data",
      plan_dataInfo:
        `${selectedPlanDetailsData?.plan_name} ${
          selectedPlanDetailsData?.type === 1 ? `` : `/ Day`
        } ` || "",
      plan_alt: "Data",
    },
  ];

  const iccidCardArray = [
    {
      key: "sm_dp_address",
      label: "SM-DP+ Address",
      value: selectedPlanDetailsData?.sm_dp_address,
    },
    {
      key: "activation_code",
      label: "Android Activation Code",
      value: selectedPlanDetailsData?.activation_code,
    },
    {
      key: "ios_activation_code",
      label: "iOS Activation Code",
      value: selectedPlanDetailsData?.ios_activation_code,
    },
  ];

  const handleOnCopy = (textToCopy: any) => {
    if (textToCopy) {
      navigator.clipboard
        .writeText(textToCopy)
        .then(() => {
          toastSuccess("copied successfully!");
        })
        .catch((err) => {
          toastError("Failed to copy!");
        });
    }
  };

  const handleDownloadClick = () => {
    const content = document.getElementById("shareContent");

    htmlToImage
      .toPng(content)
      .then((image: any) => {
        const link = document.createElement("a");
        link.download = `waosim-${selectedPlanDetailsData?.name}-${selectedPlanDetailsData?.order_id}.png`;
        link.href = image;
        link.click();
      })
      .catch((error: any) => {
        toastError(error);
      });
  };

  return (
    <div className={styles.sideBarDrawerMain}>
      <div className={styles.planDetailsMainSection}>
        <div className={styles.planDetailsHeading}>
          <h1>Plan Detail</h1>
        </div>
        <div className={styles.borderdiv}></div>

        <div className={styles.scrollDivs}>
          <div className={styles.shareContent} id="shareContent">
            <div className={styles.shareContentTop}>
              <div className={styles.shareContentTopLeft}>
                <div className={styles.planListCountryCard}>
                  <div className={styles.planListCountryCard_image}>
                    <Image
                      src={selectedPlanDetailsData?.card_image}
                      alt="selectedPlanDetailsData?.card_image"
                      height={100}
                      width={100}
                    />
                  </div>
                  <p>{selectedPlanDetailsData?.name}</p>
                </div>

                <div className={styles.planValiditySection}>
                  {planOrderArray?.map((items, index) => {
                    return (
                      <div
                        className={styles.planValiditySectionCard}
                        key={`planOrder=${index}`}
                      >
                        <div className={styles.planImage}>
                          <img src={items?.plan_img} alt={items?.plan_alt} />
                          <p>{items?.plan_detail}</p>
                        </div>

                        <h3>{items?.plan_dataInfo}</h3>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div className={styles.shareContentTopRight}>
                <QRCode
                  value={selectedPlanDetailsData?.activation_code}
                  logoImage={qrCodeLogo}
                  bgColor={"transparent"}
                  removeQrCodeBehindLogo={true}
                  // logoWidth={50}
                  // logoHeight={50}
                  fgColor={"#979797"}
                  style={{
                    width: "100%",
                    height: "100%",
                  }}
                />

                <p>Scan Me</p>
              </div>
            </div>

            <div className={styles.shareContentBottom}>
              <div className={styles.iccidCard}>
                <div className={styles.transparentRound}></div>
                <div
                  className={classNames(styles.transparentRound, styles.left)}
                ></div>
                <div className={styles.iccidCardInner}>
                  <div className={styles.myPlanactiveIconBack}>
                    <MyPlanActiveIcon fill={"#F47500"} />
                  </div>
                  {selectedPlanDetailsData?.iccid && (
                    <div className={styles.detaisSectionCombaine}>
                      <div className={styles.detaisSection}>
                        <h4>ICCID</h4>
                        <p>{selectedPlanDetailsData?.iccid}</p>
                      </div>
                    </div>
                  )}
                </div>
              </div>
              <div className={styles.iccidCard}>
                {iccidCardArray?.map(
                  (item) =>
                    item.value && (
                      <div className={styles.iccidCardInner} key={item.key}>
                        <div className={styles.myPlanactiveIconBack}>
                          <img src={cpuSetting} alt="cpuSetting" />
                        </div>
                        <div className={styles.detaisSectionCombaine}>
                          <div className={styles.detaisSection}>
                            <h4>{item.label} :</h4>
                            <p>{item.value}</p>
                          </div>
                          <div onClick={() => handleOnCopy(item.value)}>
                            <CopyIcon fill="#F47500" />
                          </div>
                        </div>
                      </div>
                    )
                )}
              </div>
            </div>
          </div>
          <div className={styles.newEsimSubmitButton}>
            <button onClick={handleDownloadClick}>
              <img
                src={downloadIcon}
                alt="downloadIcon"
                className={styles.arrowBack}
              />
               <img
                src={downloadIconOrange}
                alt="downloadIconOrange"
                className={styles.arrowBackOrange}
              />
              <p>Download Plan</p>
            </button>

            <button
              className={styles.sharePlanButton}
              onClick={handleDownloadClick}
            >
              <img
                src={shareIconOrange}
                alt="shareIconOrange"
                className={styles.arrowBack}
              />
              <img
                src={shareIconOrangeImage}
                alt="shareIconOrangeImage"
                className={styles.arrowBackOrange}
              />
              <p>Share Plan</p>
            </button>
          </div>{" "}
        </div>
      </div>
    </div>
  );
};

export default PlanDetailsSideDrawer;
