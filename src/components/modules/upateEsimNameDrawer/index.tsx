import React, { useState } from "react";
import styles from "./upateEsimNameDrawer.module.scss";
import { UpateEsimName } from "@/utils/api";
import { toastError, toastSuccess } from "@/utils";
import Spinner from "../spinner";
import { MyPlanActiveIcon } from "@/assets/images/icons";
import classNames from "classnames";
const arrow_back = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back.svg";
const arrow_back_orange = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back-orange.svg";

const UpateEsimNameDrawer = ({
  initialValue,
  setESimList,
  eSimList,
  closeSidebar,
  openSectionDetails,
  setOpenSectionDetails,
}: any) => {
  const [updateEsimName, setUpdateEsimName] = useState(initialValue?.esim_name);
  const [updateLoader, setUpdateLoader] = useState(false);

  const handleOnUpateEsimName = async (e: any) => {
    e.preventDefault();

    setUpdateLoader(true);
    let payload = {
      iccid: initialValue?.iccid,
      esim_name: updateEsimName,
    };
    const upateEsimNameRes = await UpateEsimName({
      method: "POST",
      body: payload,
    });

    if (upateEsimNameRes?.success) {
      setUpdateLoader(false);

      let newEsim = eSimList?.map((item: any, index: any) => {
        if (item?.iccid === upateEsimNameRes?.data?.iccid) {
          return { ...item, ...upateEsimNameRes?.data };
        } else {
          return item;
        }
      });
      setOpenSectionDetails({
        ...openSectionDetails,
        ...upateEsimNameRes?.data,
      });
      setESimList(newEsim);
      toastSuccess("eSIM name updated successfully!");
      closeSidebar();
    } else {
      setUpdateLoader(false);
      toastError(upateEsimNameRes?.message);
    }
  };

  const handleUpateNameChange = (e: any) => {
    const { name, value } = e.target;
    const emojiRegex = /[\p{Emoji_Presentation}\p{Extended_Pictographic}]/gu;
    let sanitizedValue = value.replace(emojiRegex, "");

    sanitizedValue = sanitizedValue.trimStart();

    if (sanitizedValue?.length > 40) {
      return;
    }

    setUpdateEsimName(sanitizedValue);
  };

  return (
    <div className={styles.updateEsimNameSection}>
      <div className={styles.updateEsimHeading}>
        <h1>Update eSIM Name</h1>
      </div>
      <div className={styles.borderdiv}></div>

      <div className={styles.esimInputPart}>
        <label>eSIM Name</label>

        <div className={styles.enterCouponBalance}>
          <input
            type="text"
            name="referral"
            placeholder="eSIM Name"
            value={updateEsimName}
            onChange={handleUpateNameChange}
            disabled={updateLoader}
            required
          />
          <div className={styles.myPlanActiveicon}>
            <MyPlanActiveIcon fill={"white"} />
          </div>
        </div>

        <div className={styles.updateButtonEsim}>
          <button
            type="submit"
            onClick={handleOnUpateEsimName}
            disabled={updateLoader}
          >
            {updateLoader ? (
              <Spinner />
            ) : (
              <>
                <p>Update</p>
                <img
                src={arrow_back}
                alt="back arrow"
                className={styles.arrowBack}
              />
              <img
                src={arrow_back_orange}
                alt="back arrow"
                className={styles.arrowBackOrange}
              />
              </>
            )}
          </button>
        </div>
      </div>
    </div>
  );
};

export default UpateEsimNameDrawer;
