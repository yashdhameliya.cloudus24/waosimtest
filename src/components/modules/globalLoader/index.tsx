import React from "react";
import styles from "./globalLoader.module.scss";
import Spinner from "../spinner";

const GlobalLoader = () => {
  return (
    <div className={styles.globalLoaderContainer}>
      {/* <span className={styles.loader}></span> */}
      <div className={styles.loader}>
        <div className={styles.loader_spinner}></div>
      </div>
    </div>
  );
};

export default GlobalLoader;
