import {
  interBold,
  interMedium,
  interRegular,
  interSemiBold,
  kabutHitamRegular,
} from "@/assets/fonts/fonts";
import GlobalProvider from "@/components/contexts/globalContext/context";
import LanguageProvider from "@/components/contexts/languageContext";
import { SidebarProvider } from "@/components/contexts/sidebarContext/context";
import UserProvider from "@/components/contexts/userContext/context";
import Footer from "@/components/rendering/footer";
import Header from "@/components/rendering/header";
import classNames from "classnames";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { Toaster } from "react-hot-toast";

export interface LayoutPropType {
  page: any;
  children: any;
  popup?: any;
  router?: any;
}
export default function Layout({
  page,
  children,
  popup,
  router,
}: LayoutPropType) {
  let globalFontVariableClass = classNames(
    interSemiBold.className,
    interRegular.variable,
    interMedium.variable,
    interBold.variable,
    kabutHitamRegular.variable
  );

  const TransparentRoutes = ["/"];

  const isTransparentBackground = TransparentRoutes.includes(router.pathname);
  const headerRef = useRef<any>(null);

  const [headerHeight, setHeaderHeight] = useState(0);

  useEffect(() => {
    const handleResize = () => {
      if (headerRef.current) {
        const height = headerRef.current.offsetHeight;
        setHeaderHeight(height);
      }
    };

    handleResize();

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <LanguageProvider>
      <SidebarProvider>
        <UserProvider>
          <Toaster />
          <GlobalProvider headerHeight={headerHeight}>
            <main
              className={globalFontVariableClass}
              style={{
                paddingTop: isTransparentBackground ? "0px" : headerHeight,
              }}
            >
              <Header
                headerRef={headerRef}
                isTransparentBackground={isTransparentBackground}
              />
              {children}
              <Footer />
            </main>
          </GlobalProvider>
        </UserProvider>
      </SidebarProvider>
    </LanguageProvider>
  );
}
