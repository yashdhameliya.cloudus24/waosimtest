import React, { useContext, useEffect, useRef } from "react";
import styles from "./sideBarDrawer.module.scss";
import { useSidebar } from "@/components/contexts/sidebarContext/context";

const SideBarDrawer = () => {
  const { open, content, closeSidebar } = useSidebar();
  let placement = "left";
  const ref = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    function handleClickOutside(event: any) {
      if (ref?.current && !ref?.current?.contains(event.target)) {
        closeSidebar();
      }
    }

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref, closeSidebar]);

  return (
    <div className={`${open && styles.drawerOpen}`}>
      <div
        className={`${styles.drawer} ${open ? styles.open : styles.close} ${
          placement && placement === "left" ? styles.left : styles.right
        }`}
        ref={ref}
      >
        <div className={styles.drawerCloseMain}>
          <div className={styles.drawerHeadContainer}>
            <button className={styles.closebtn} onClick={closeSidebar}>
              Close
            </button>
          </div>
        </div>
        {content}
      </div>
    </div>
  );
};

export default SideBarDrawer;
