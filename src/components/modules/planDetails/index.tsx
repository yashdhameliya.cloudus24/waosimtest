import React, { useContext, useEffect, useState } from "react";
import styles from "./planDetails.module.scss";
import Image from "next/image";
import Button from "../button";
import { AdditionInfoArray } from "../planOrderSection/planOrderStatic";
import classNames from "classnames";
import { MyPlanActiveIcon, RecycleIcon } from "@/assets/images/icons";
import { useSidebar } from "@/components/contexts/sidebarContext/context";
import PlanDetailsSideDrawer from "../planDetailsSideDrawer";
import DeleteSideDrawer from "../deleteSideDrawer";
import {
  ArchivePlanRoute,
  CancelPlanRoute,
  DeletePlanRoute,
  getEsimStatusRouteApi,
} from "@/utils/api";
import { toastError, toastSuccess } from "@/utils";
import { GlobalContext } from "@/components/contexts/globalContext/context";
import { useRouter } from "next/router";
const backIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/backIcon.svg";
const Provider = "https://stagecdn.waosim.com/landingpage/assets/icons/order_provider.svg";
const plan_coverage = "https://stagecdn.waosim.com/landingpage/assets/icons/order_coverage.svg";
const plan = "https://stagecdn.waosim.com/landingpage/assets/icons/order_plan.svg";
const validity = "https://stagecdn.waosim.com/landingpage/assets/icons/order_validity.svg";
const data = "https://stagecdn.waosim.com/landingpage/assets/icons/order_data.svg";
const plan_type = "https://stagecdn.waosim.com/landingpage/assets/icons/order_plan_type.svg";
const phone_number = "https://stagecdn.waosim.com/landingpage/assets/icons/order_phone_number.svg";
const cancleIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/cancleIcon.svg";
const bookmark = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/bookmark.svg";
const shareIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/shareIcon.svg";
const mangeIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/mangeIcon.svg";
const deleteIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/deleteIcon.svg";
const installEsimIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/installEsimIcon.png";
const dataValidateIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/dataValidateIcon.png";
import ArcProgress from "react-arc-progress";

const PlanDetailsModule = ({
  handleOnClosePlanDetail,
  selectedPlanDetailsData,
  handleOnDeleteClose,
  setPlanDetailsOpen,
  setSelectedESimPlansData,
  selectedESimPlansData,
}: any) => {
  const { openSidebar, closeSidebar } = useSidebar();
  const { setIsglobalLoaderActive } = useContext(GlobalContext);
  const [esimStatus, setEsimStatus] = useState<any>(null);
  const router = useRouter();
  const [progress, setProgress] = useState(
    selectedPlanDetailsData?.used_percentage
  );

  const planOrderArray = [
    {
      plan_img: plan,
      plan_detail: "Plan",
      plan_dataInfo: selectedPlanDetailsData?.name || "",
      plan_alt: "Plan",
    },
    {
      plan_img: validity,
      plan_detail: "Validity",
      plan_dataInfo: `${selectedPlanDetailsData?.plan_days} Days` || "",
      plan_alt: "Validity",
    },
    {
      plan_img: data,
      plan_detail: "Data",
      plan_dataInfo:
        `${selectedPlanDetailsData?.plan_name} ${
          selectedPlanDetailsData?.type === 1 ? `` : `/ Day`
        } ` || "",
      plan_alt: "Data",
    },
    {
      plan_img: plan_type,
      plan_detail: "Plan Type",
      plan_dataInfo: selectedPlanDetailsData?.plan_type_text || "",
      plan_alt: "Plan Type",
    },
    {
      plan_img: phone_number,
      plan_detail: "Phone Number (SMS)",
      plan_dataInfo: selectedPlanDetailsData?.phone_number_sms || "",
      plan_alt: "Phone Number",
    },
    {
      plan_img: data,
      plan_detail: "Network",
      plan_dataInfo: selectedPlanDetailsData?.network || "",
      plan_alt: "Network",
    },
  ];

  const customText = [
    { text: "dsdsdsdsd", size: "34px", color: "red", x: 100, y: 94 },
    { text: "/100", size: "14px", color: "red", x: 100, y: 122 },
  ];

  const handleOnDeletePlan = async (e: any) => {
    e.preventDefault();

    setIsglobalLoaderActive(true);
    let payload = {
      iccid: selectedPlanDetailsData?.iccid,
      order_id: selectedPlanDetailsData?.order_id,
    };
    const deletePlanRoute = await DeletePlanRoute({
      method: "POST",
      body: payload,
    });

    if (deletePlanRoute?.success) {
      closeSidebar();
      toastSuccess(deletePlanRoute?.message);
      handleOnDeleteClose(e);
    } else {
      closeSidebar();
      setIsglobalLoaderActive(false);
      toastError(deletePlanRoute?.message);
    }
  };

  const handleOnCancleDeleteDrawer = (e: any) => {
    e.preventDefault();
    closeSidebar();
  };

  const handleOnCancelPlan = async () => {
    setIsglobalLoaderActive(true);
    let payload = {
      iccid: selectedPlanDetailsData?.iccid,
      order_id: selectedPlanDetailsData?.order_id,
    };
    const cancelPlanRoute = await CancelPlanRoute({
      method: "POST",
      body: payload,
    });

    if (cancelPlanRoute?.success) {
      setIsglobalLoaderActive(false);
      toastSuccess(cancelPlanRoute?.message);
    } else {
      setIsglobalLoaderActive(false);
      toastError(cancelPlanRoute?.message);
    }
  };

  const handleOnArchivePlan = async () => {
    setIsglobalLoaderActive(true);
    let payload = {
      iccid: selectedPlanDetailsData?.iccid,
      order_id: selectedPlanDetailsData?.order_id,
      achieve_status: 1,
    };
    const archivePlanRoute = await ArchivePlanRoute({
      method: "POST",
      body: payload,
    });
    if (archivePlanRoute?.success) {
      let newEsim = selectedESimPlansData?.filter(
        (item: any, index: any) => item?.id !== selectedPlanDetailsData?.id
      );

      setSelectedESimPlansData(newEsim);
      setPlanDetailsOpen(false);
      setIsglobalLoaderActive(false);
      toastSuccess(archivePlanRoute?.message);
    } else {
      setIsglobalLoaderActive(false);
      toastError(archivePlanRoute?.message);
    }
  };

  const handleOnOpenSideBar = (type: any) => {
    openSidebar(
      <>
        {type === "coverageList" && (
          <div className={styles.regionListSection}>
            <div className={styles.regionsHeading}>
              <h1>{selectedPlanDetailsData?.name}</h1>
              <span>
                {`${selectedPlanDetailsData?.coverage_data?.length} Destinations`}{" "}
              </span>
            </div>
            <div className={styles.borderdiv}></div>
            {selectedPlanDetailsData?.coverage_data && (
              <div className={styles.regionsDetailList}>
                {selectedPlanDetailsData?.coverage_data?.map(
                  (coverageItam: string, provideIndex: number) => {
                    return (
                      <p key={`provideIndex - ${provideIndex}`}>
                        {coverageItam}
                      </p>
                    );
                  }
                )}
              </div>
            )}
          </div>
        )}

        {type === "providerList" && (
          <div className={styles.regionListSection}>
            <div className={styles.regionsHeading}>
              <h1>Provider</h1>
              <span>
                {`${selectedPlanDetailsData?.providers?.length} Providers`}{" "}
              </span>
            </div>
            <div className={styles.borderdiv}></div>
            {selectedPlanDetailsData?.providers && (
              <div className={styles.regionsDetailList}>
                {selectedPlanDetailsData?.providers?.map(
                  (providerItam: string, provideIndex: number) => {
                    return (
                      <p key={`provideIndex - ${provideIndex}`}>
                        {providerItam}
                      </p>
                    );
                  }
                )}
              </div>
            )}
          </div>
        )}

        {type === "planDetails" && (
          <PlanDetailsSideDrawer
            selectedPlanDetailsData={selectedPlanDetailsData}
          />
        )}

        {type === "deletePlan" && (
          <DeleteSideDrawer
            title={`Delete Plan`}
            description={`Are you sure you want to delete your plan?`}
            handleOnDeletePlan={handleOnDeletePlan}
            handleOnCancle={handleOnCancleDeleteDrawer}
          />
        )}
      </>
    );
  };

  const getEsimStatus = async () => {
    setIsglobalLoaderActive(true);
    let payload = {
      iccid: selectedPlanDetailsData?.iccid,
    };
    const getEsimStatusRouteApiRes = await getEsimStatusRouteApi({
      method: "POST",
      body: payload,
    });

    if (getEsimStatusRouteApiRes?.success) {
      setIsglobalLoaderActive(false);

      setEsimStatus(getEsimStatusRouteApiRes?.data);
    } else {
      setIsglobalLoaderActive(false);
    }
  };
  useEffect(() => {
    getEsimStatus();
  }, []);

  return (
    <div className={styles.planDetailsModuleContainer}>
      <div className={styles.backToPlaneButton}>
        <button onClick={handleOnClosePlanDetail}>
          <Image src={backIcon} width={12} height={12} alt="backIcon" />
          <span>Back to plans</span>
        </button>
      </div>

      <div className={styles.detailsSectionContainer}>
        <div className={styles.detailsSectionLeft}>
          <div className={styles.OrderInformationMain}>
            {planOrderArray?.map((items, index) => {
              return (
                <div
                  className={styles.countryInfoMain}
                  key={`planOrder=${index}`}
                >
                  <div className={styles.countryInfoCard}>
                    <div className={styles.planImage}>
                      <img src={items?.plan_img} alt={items?.plan_alt} />
                    </div>

                    <p>{items?.plan_detail}</p>

                    <h2>{items?.plan_dataInfo}</h2>
                  </div>
                </div>
              );
            })}
          </div>

          <div className={styles.additionalInfoMain}>
            <h1>Additional Information</h1>
            <div className={styles.reviewYourOrderBlockMain}>
              {selectedPlanDetailsData?.coverage_data?.length > 0 && (
                <div className={styles.reviewOrderBlock}>
                  <div className={styles.blockLeftData}>
                    <div className={styles.dataImage}>
                      <img src={plan_coverage} alt="coverage" />
                    </div>
                    <div className={styles.dataDescription}>
                      <p>Coverage</p>
                      <h3>{selectedPlanDetailsData?.coverage_data?.length}</h3>
                    </div>
                  </div>
                  <div className={styles.checkListButton}>
                    <Button
                      color={"solid"}
                      type={"orange"}
                      title={`Check List`}
                      clickHandler={() => handleOnOpenSideBar("coverageList")}
                    />
                  </div>
                </div>
              )}

              {selectedPlanDetailsData?.providers?.length > 0 && (
                <div className={styles.reviewOrderBlock}>
                  <div className={styles.blockLeftData}>
                    <div className={styles.dataImage}>
                      <img src={Provider} alt="Provider" />
                    </div>
                    <div className={styles.dataDescription}>
                      <p>Provider</p>
                      <h3>{selectedPlanDetailsData?.providers[0]}</h3>
                    </div>
                  </div>
                  <div className={styles.checkListButton}>
                    <Button
                      color={"solid"}
                      type={"orange"}
                      title={`Check List`}
                      clickHandler={() => handleOnOpenSideBar("providerList")}
                    />
                  </div>
                </div>
              )}

              {AdditionInfoArray?.map((items, index) => {
                return (
                  <div className={styles.reviewOrderBlock} key={index}>
                    <div className={styles.blockLeftData}>
                      <div className={styles.dataImage}>
                        <img src={items?.InfoIcon} alt="Provider" />
                      </div>
                      <div className={styles.dataDescription}>
                        <p>{items?.InfoName}</p>
                        <h3>{items?.InfoDetail}</h3>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className={styles.detailsSectionRight}>
          <div className={styles.dataUsage}>
            <div
              className={classNames(
                styles.esimStatusButton,
                esimStatus &&
                  (esimStatus?.installed
                    ? styles.greenColorButton
                    : styles.whiteColorButton)
              )}
              // onClick={() =>
              //   !esimStatus?.installed &&
              //   router.push(
              //     `/profile/my-esim?iccid=${selectedPlanDetailsData?.iccid}`
              //   )
              // }
            >
              {esimStatus && (
                <>
                  {esimStatus?.installed ? (
                    <p>eSIM Installed</p>
                  ) : (
                    <>
                      <img src={installEsimIcon} alt="installEsimIcon" />
                      <p>Install eSIM</p>
                    </>
                  )}
                </>
              )}
            </div>

            {selectedPlanDetailsData?.status === 3 && (
              <div
                className={classNames(
                  styles.esimStatusButton,
                  styles.blueColorButton
                )}
              >
                <RecycleIcon fill="white" />
                <p>In-Use</p>
              </div>
            )}

            {selectedPlanDetailsData?.status === 1 && (
              <div
                className={classNames(
                  styles.esimStatusButton,
                  styles.yellowColorButton
                )}
              >
                <RecycleIcon fill="#000000" />
                <p>Unused</p>
              </div>
            )}

            {selectedPlanDetailsData?.status === 2 && (
              <>
                {selectedPlanDetailsData?.activeTime?.length > 0 ? (
                  <div
                    className={classNames(
                      styles.esimStatusButton,
                      styles.greyColorButton
                    )}
                  >
                    <RecycleIcon fill="white" />
                    <p>Used</p>
                  </div>
                ) : (
                  <div
                    className={classNames(
                      styles.esimStatusButton,
                      styles.redColorButton
                    )}
                  >
                    <RecycleIcon fill="white" />
                    <p>Expired</p>
                  </div>
                )}
              </>
            )}

            {selectedPlanDetailsData?.status === 3 && (
              <>
                <div className={styles.dataUsageTop}>
                  <div className={styles.progressBar}>
                    {/* <Flat
                      progress={selectedPlanDetailsData?.used_percentage || 0}
                      text={"Used"}
                      showMiniCircle={true}
                      sx={{
                        strokeColor: "#F47500",
                        bgStrokeColor: "#000000",
                        barWidth: 8,
                        bgColor: { value: "#EEF1F5", transparency: "0" },
                        valueSize: 18,
                        valueWeight: "bold",
                        textSize: 12,
                        shape: "threequarters",
                        miniCircleColor: "#f47500",
                        strokeLinecap: "round",
                        miniCircleSize: 5,
                      }}
                    /> */}

                    <>
                      {/* <div
                              style={{
                                // height: "100%",
                                // width: "100%",
                                borderRadius: 50,
                                position: "absolute",
                                left: 58,
                                marginTop: 50,
                                boxShadow: `1px 3px 1px rgba(0,0,0,0.1)`,
                              }}
                            >
                            </div> */}

                      <ArcProgress
                        progress={
                          selectedPlanDetailsData?.used_percentage / 100
                        }
                        fillColor={"#f47500"}
                        text={`${progress}%`}
                        size={73}
                        textStyle={{
                          color: "#000000",
                          size: "16px",
                          length: 10,
                          text: "inter",
                        }}
                        emptyColor={"#F2F2F2"}
                        thickness={8}
                        customText={customText}
                        lineCap="round"
                        observer={(current) => {
                          const { percentage, currentText } = current;
                          setProgress(percentage);
                        }}
                        animationEnd={({ progress, text }) => {}}
                      />
                    </>
                  </div>
                  <div className={styles.usedRemainingButton}>
                    <div className={styles.usedSection}>
                      <p>
                        {selectedPlanDetailsData?.used_data} <span> Used</span>
                      </p>
                    </div>
                    <div
                      className={classNames(
                        styles.usedSection,
                        styles.greenBox
                      )}
                    >
                      <p>
                        {selectedPlanDetailsData?.remain_data}{" "}
                        <span> Remaining</span>
                      </p>
                    </div>
                  </div>
                </div>

                <div className={styles.dataUsageBottom}>
                  <div className={styles.dataUsageNw}>
                    <h4>
                      {" "}
                      <Image
                        src={dataValidateIcon}
                        width={18}
                        height={18}
                        alt="dataValidateIcon"
                      />
                      Data Validity{" "}
                    </h4>
                    {selectedPlanDetailsData?.activeDay ? (
                      <p>
                        <span>{selectedPlanDetailsData?.activeDay} </span>
                        {`/ ${selectedPlanDetailsData?.plan_days} ${
                          selectedPlanDetailsData?.plan_days > 1
                            ? "Days"
                            : "Day"
                        }`}{" "}
                      </p>
                    ) : (
                      <b> -- / --</b>
                    )}
                  </div>
                </div>
              </>
            )}

            <div
              className={classNames(
                styles.esimStatusButton,
                styles.lightRedColorButton
              )}
            >
              {selectedPlanDetailsData?.status === 2 &&
                (selectedPlanDetailsData?.activeTime?.length > 0 ? (
                  <p>{`Plan Used on ${selectedPlanDetailsData?.expireTime}`}</p>
                ) : (
                  <p>{`Plan Expired on ${selectedPlanDetailsData?.endTime}`}</p>
                ))}

              {selectedPlanDetailsData?.status === 3 && (
                <p>{`Plan Expire on ${selectedPlanDetailsData?.expireTime}`}</p>
              )}

              {/* Unused */}
              {selectedPlanDetailsData?.status === 1 && (
                <p>{`Plan Expire on ${selectedPlanDetailsData?.expireTime}`}</p>
              )}
            </div>
          </div>

          {/* <div className={styles.eSIMDetails}>
            <MyPlanActiveIcon fill={"white"} />
            <p>eSIM Details</p>
          </div> */}

          {selectedPlanDetailsData?.status === 1 &&
            esimStatus &&
            !esimStatus?.installed && (
              <div className={styles.archivePlan} onClick={handleOnCancelPlan}>
                <img src={cancleIcon} alt="cancleIcon" />
                <p>
                  Cancel Plan <span>Free Refund (Till 1 Aug 2024)</span>
                </p>
              </div>
            )}

          <div className={styles.archivePlan} onClick={handleOnArchivePlan}>
            <img src={bookmark} alt="bookmark" />
            <p>Archive Plan</p>
          </div>

          <div
            className={styles.archivePlan}
            onClick={() => handleOnOpenSideBar("planDetails")}
          >
            <img src={shareIcon} alt="shareIcon" />
            <p>Share Plan</p>
          </div>

          <div
            className={styles.archivePlan}
            onClick={() =>
              // !esimStatus?.installed &&
              router.push(
                `/profile/my-esim?iccid=${selectedPlanDetailsData?.iccid}`
              )
            }
          >
            <img src={mangeIcon} alt="mangeIcon" />
            <p>Manage Plan eSIM</p>
          </div>

          <div
            className={styles.archivePlan}
            onClick={() => handleOnOpenSideBar("deletePlan")}
          >
            <img src={deleteIcon} alt="deleteIcon" />
            <p>Delete Plan</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PlanDetailsModule;
