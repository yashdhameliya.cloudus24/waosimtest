import React, { useEffect } from "react";
import styles from "./PurchasedSuccessModal.module.scss";
import { useRouter } from "next/router";
import moment from "moment-timezone";
const success_icon = "https://stagecdn.waosim.com/landingpage/assets/icons/payment_success_icon.svg";

const PurchasedSuccessModal = ({ content, closePopupModel }: any) => {
  const router = useRouter();

  const userTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;

  // Parse the original date string and set it to the user's time zone
  const parsedDate = moment.tz(
    content?.date_time,
    "YYYY-MM-DD HH:mm:ss",
    userTimeZone
  );
  const formattedDate = parsedDate.format("MMM DD, YYYY");
  const formattedTime = parsedDate.format("hh:mm A");

  return (
    <div>
      <div className={styles.purchasedSuccessModalMain}>
        <div className={styles.esimPurchasedSuccessMassage}>
          <div className={styles.successImage}>
            <img src={success_icon} alt="success image" />
          </div>

          <h1>eSIM Purchased</h1>
          <h1>Successfully</h1>
        </div>
        <div className={styles.successModalBorder}></div>
        <div className={styles.successModalDateTimeMain}>
          {formattedDate && (
            <div className={styles.modalDateTimeData}>
              <p>Date</p>
              <h4>{formattedDate}</h4>
            </div>
          )}

          {formattedTime && (
            <div className={styles.modalDateTimeData}>
              <p>Time</p>
              <h4>{formattedTime}</h4>
            </div>
          )}
        </div>
        <div className={styles.successModalBorder}></div>
        <div className={styles.successTotal}>
          <h2>Amount</h2>
          <h1>$ {content?.price}</h1>
        </div>
        <div className={styles.successModalBorder}></div>
        <div
          className={styles.backToMyPlanButton}
          onClick={() => {
            closePopupModel();
            router.push(`/profile/my-plans`);
          }}
        >
          <button>Back to my PLans</button>
        </div>
      </div>
    </div>
  );
};

export default PurchasedSuccessModal;
