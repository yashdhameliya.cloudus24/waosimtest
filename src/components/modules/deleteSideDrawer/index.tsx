import React from "react";
import styles from "./deleteSideDrawer.module.scss";
import Spinner from "../spinner";

const DeleteSideDrawer = ({
  title,
  description,
  handleOnDeletePlan,
  handleOnCancle,
  updateLoader,
}: any) => {
  return (
    <div className={styles.deleteSideDrawerMain}>
      <div className={styles.deleteMainSection}>
        <div className={styles.deleteHeading}>
          <h1> {title}</h1>
        </div>
        <div className={styles.borderdiv}></div>
        <div className={styles.deleteContainMain}>
          <div className={styles.deleteContain}>
            <img
              src="https://stagecdn.waosim.com/myprofile/deleteEsimPlanCardImage.png"
              alt="delateSideBarBAckground"
            />
            <p>{description}</p>
            <div className={styles.newEsimSubmitButton}>
              <button
                className={styles.redButton}
                onClick={handleOnDeletePlan}
                disabled={updateLoader}
              >
                {updateLoader ? <Spinner /> : <span>Delete</span>}
              </button>
              <button
                className={styles.greyButton}
                onClick={handleOnCancle}
                disabled={updateLoader}
              >
                <span>Cancel</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeleteSideDrawer;
