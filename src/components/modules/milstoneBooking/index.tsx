import React from "react";
import styles from "./milestoneBooking.module.scss";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import { FaLock, FaUnlock } from "react-icons/fa";

const MilestoneBooking = () => {
  const bookings = [
    { id: 1, title: "Booking 1", date: "2024/04/12 13:00", active: true },
    { id: 2, title: "Booking 2", date: "2024/04/12 13:00", active: true },
    { id: 3, title: "Booking 3", date: "2024/04/12 13:00", active: true },
    { id: 4, title: "Booking 4", date: "2024/04/12 13:00", active: true },
    { id: 5, title: "Booking 5", date: "", active: true },
    { id: 6, title: "Booking 6", date: "", active: false },
    { id: 7, title: "Booking 7", date: "", active: false },
    { id: 8, title: "Booking 8", date: "", active: false },
    { id: 9, title: "Booking 9", date: "", active: false },
  ];

  return (
    <div>
      <VerticalTimeline
        layout={"1-column"}
        lineColor={"lightgray"}
        className={styles.verticalTimeline}
      >
        {bookings.map((booking) => (
          <VerticalTimelineElement
            key={booking.id}
            date={booking.date}
            className={`${styles.verticalTimelineElement} ${
              booking.active ? styles.orangeLine : ""
            }`}
            iconStyle={{
              background: booking.active ? "#F47500" : "#fff",
              color: "#fff",
              border: booking.active ? "1px solid #fff" : "1px solid #B9B9B9",
            }}
            contentStyle={{
              borderRadius: "10px",
              height: "65px",
              bottom: "28px",
              background: booking.active ? "#F47500" : "#F2F2F2",
              color: "#fff",
              padding: "10px 15px",
              boxShadow: "unset",
              display:"flex",
              justifyContent:"center",
              alignItems:"center",
              flexFlow: "column",
            }}
            contentArrowStyle={{
              top: "25px",
              borderRight: `7px solid ${
                booking.active ? "#F47500" : "#F2F2F2"
              }`,
            }}
            icon={
              booking.active ? (
                <FaUnlock />
              ) : (
                <FaLock style={{ color: "black" }} />
              )
            }
            // lineColor={booking.active ? "black" : "#F47500"}
          >
            <div className={styles.bookingText}>
              <h3
                style={{ color: booking.active ? "white" : "black" }}
                className={styles.verticalTimelineElementTitle}
              >
                {booking.title}
              </h3>
            </div>
          </VerticalTimelineElement>
        ))}
      </VerticalTimeline>
    </div>
  );
};

export default MilestoneBooking;
