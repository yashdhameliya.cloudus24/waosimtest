import React from "react";
import styles from "./googleLogin.module.scss";
import { signInWithPopup } from "firebase/auth";
import { auth, provider } from "../../../utils/socialLogin/Firebase";
import { toastError, toastSuccess } from "@/utils";
import { signIn, signupNewUser } from "@/utils/api";
import { setCookie } from "@/utils/useHooks/useCookies";
import {
  getItemFromSession,
  removeItemFromSession,
  setItemInLocal,
} from "@/utils/useHooks/useStorage";
import { useRouter } from "next/router";
import { requestNotificationPermission } from "@/utils/api/firebase/fcm";
const GoogleIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/flat-color-icons_google.svg";

const GoogleLogin = ({ type }: any) => {
  const router = useRouter();

  const handleOnSignUp = async (user: any) => {
    // const fcmToken = await requestNotificationPermission();
    const fcmToken =
      "f7I7KPWww7vjRV-JLa8ZYM:APA91bHx9uDcLvWC4zNXKNFuXRt1KJ6Rs_sIjAbM8pwto1cic2KJgDg7ylWJe_0TwHB4avAGLGIeknujj3UVq0Acq0UFawx0r7gnJzpoNNpIm8bdYvDqHaOqrme7HLHCq377cnyMTlHw";

    let payload = {
      account_type: 3, //1 => email, 2 =>phone number, 3 => google login, 4 => apple login
      full_name: user?.user?.displayName,
      email: user?.user?.email,
      fcm_id: fcmToken || "",
      device: 3,
      social_token: user?.user?.uid,
    };

    const signup = await signupNewUser({
      method: "POST",
      body: payload,
    });
    if (signup?.success) {
      let userDetails = {
        name: signup?.data?.name,
        is_email_verified: signup?.data?.is_email_verified,
        is_mobile_verified: signup?.data?.is_mobile_verified,
        email: signup?.data?.email,
        mobile: signup?.data?.mobile,
        user_unique_id: signup?.data?.user_unique_id,
      };

      setCookie("waotoken", signup?.data?.auth_token, {});
      setItemInLocal("userDetails", userDetails);
      if (getItemFromSession("lastroute")) {
        router.push(getItemFromSession("lastroute"));
        removeItemFromSession("lastroute");
      } else {
        router.push(`/`);
      }

      signup?.message && toastSuccess(signup?.message);
    } else {
      toastError(signup?.message);
    }
  };

  const handleOnSignIn = async (user: any) => {
    // const fcmToken = await requestNotificationPermission();
    const fcmToken =
      "f7I7KPWww7vjRV-JLa8ZYM:APA91bHx9uDcLvWC4zNXKNFuXRt1KJ6Rs_sIjAbM8pwto1cic2KJgDg7ylWJe_0TwHB4avAGLGIeknujj3UVq0Acq0UFawx0r7gnJzpoNNpIm8bdYvDqHaOqrme7HLHCq377cnyMTlHw";
    let payload = {
      account_type: 3, //1 => email, 2 =>phone number, 3 => google login, 4 => apple login
      full_name: user?.user?.displayName,
      email: user?.user?.email,
      fcm_id: fcmToken || "",
      device: 3,
      social_token: user?.user?.uid,
    };

    const signInRes = await signIn({
      method: "POST",
      body: payload,
    });
    if (signInRes?.success) {
      let userDetails = {
        name: signInRes?.data?.name,
        is_email_verified: signInRes?.data?.is_email_verified,
        is_mobile_verified: signInRes?.data?.is_mobile_verified,
        email: signInRes?.data?.email,
        mobile: signInRes?.data?.mobile,
        user_unique_id: signInRes?.data?.user_unique_id,
      };

      setCookie("waotoken", signInRes?.data?.auth_token, {});
      setItemInLocal("userDetails", userDetails);
      if (getItemFromSession("lastroute")) {
        router.push(getItemFromSession("lastroute"));
        removeItemFromSession("lastroute");
      } else {
        router.push(`/`);
      }

      signInRes?.message && toastSuccess(signInRes?.message);
    } else {
      signInRes?.message && toastError(signInRes?.message);
    }
  };
  const handleOnClick = () => {
    signInWithPopup(auth, provider).then((result) => {
      if (type == "signIn") {
        handleOnSignIn(result);
      } else {
        handleOnSignUp(result);
      }
    });
  };

  return (
    <div className={styles.googleLoginMain} onClick={() => handleOnClick()}>
      <div className={styles.IconImage}>
        <img src={GoogleIcon} alt="GoogleIcon" />
      </div>
      <h2>
        {type == "signIn" ? "Sign In With Google" : "Sign Up With Google"}{" "}
      </h2>
    </div>
  );
};

export default GoogleLogin;
