import React, { useEffect, useRef, useState } from "react";
import styles from "./desktopHeader.module.scss";
import Link from "next/link";
import classNames from "classnames";
import { useRouter } from "next/router";
import LocaleSelector from "../localeSelector";
import { useUser } from "@/components/contexts/userContext/context";
import { setItemInSession } from "@/utils/useHooks/useStorage";
import Dropdown from "react-bootstrap/Dropdown";
const myaccount = "https://stagecdn.waosim.com/landingpage/assets/icons/my_account_icon.svg";
const logout = "https://stagecdn.waosim.com/landingpage/assets/icons/logout_header_icon.svg";
const myaccount_white = "https://stagecdn.waosim.com/landingpage/assets/icons/my_account_icon_white.svg";
const logout_white = "https://stagecdn.waosim.com/landingpage/assets/icons/logout_header_icon_white.svg";
const MobileMenuIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/mobileMenu.svg";

let navigation = [
  {
    title: "Home",
    url: "/",
  },
  {
    title: "Plans",
    url: "/plans",
  },
  {
    title: "WaoClub",
    url: "/waoclub",
  },
  {
    title: "Device",
    url: "/device",
  },
  {
    title: "How it works",
    url: "/works",
  },
  {
    title: "Operator / APN",
    url: "/operator",
  },
  // {
  //   title: "Installation",
  //   url: "/installation",
  // },
  {
    title: "News",
    url: "/news",
  },
  // {
  //   title: "Contact Us",
  //   url: "/contact-us",
  // },
];

const DesktopHeader = ({
  isTransparentBackground = true,
  headerRef,
}: {
  isTransparentBackground?: boolean;
  headerRef: any;
}) => {
  const [isSticky, setIsSticky] = useState(false);
  const [isSideNavOpen, setIsSideNavOpen] = useState(false);
  const scrollThreshold = 10;
  const router = useRouter();
  const { pathname } = router;
  const { userDetails, handleOnLogOut } = useUser();

  const openNav = () => {
    setIsSideNavOpen(true);
  };

  const closeNav = () => {
    setIsSideNavOpen(false);
  };

  useEffect(() => {
    const handleScroll = () => {
      setIsSticky(window.pageYOffset > scrollThreshold);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const handleOnSignIn = () => {
    if (router?.asPath !== "/auth/login" && router?.asPath !== "/auth/signup") {
      setItemInSession("lastroute", router?.asPath);
    }
    router.push(`/auth/login`);
  };

  return (
    <div
      className={classNames(
        styles.desktopNavigation,
        isSticky || !isTransparentBackground
          ? styles.headerSticky
          : styles.headerFixed
      )}
      ref={headerRef}
    >
      <div className={styles.desktopHeaderContainer}>
        <div className={styles.brand_logo_main}>
          <Link href="/">
            <img src="https://stagecdn.waosim.com/landingpage/assets/brand_logo.svg" alt="Brand Logo" />
          </Link>
        </div>

        <>
          <div className={styles.desktopNavigationMenu}>
            {navigation?.map(({ title, url }: any, currentIndex: number) => (
              <Link
                key={`navigation-${currentIndex}`}
                className={classNames(
                  styles.tablink,
                  `/${pathname?.split("/")[1]}` === url && styles.active
                )}
                href={url.length > 0 ? url : "/"}
              >
                {title}
              </Link>
            ))}
          </div>
          <div className={styles.userSelectContainer}>
            {!userDetails?.name ? (
              <div
                className={classNames(
                  styles.login_sing_up,
                  pathname.includes("auth") && styles.active
                )}
                onClick={handleOnSignIn}
              >
                Log in / Sign up
              </div>
            ) : (
              <Dropdown className={styles.dropdown}>
                <Dropdown.Toggle
                  id="dropdown-basic"
                  className={styles.dropdownToggle}
                >
                  <h5 style={{ cursor: "pointer" }}>{userDetails?.name}</h5>
                </Dropdown.Toggle>
                <Dropdown.Menu className={styles.dropdownMenu}>
                  <Dropdown.Item
                    href="/profile/my-plans"
                    className={styles.dropdownItem}
                  >
                    <img
                      src={myaccount}
                      alt="My account"
                      className={styles.defaulticon}
                    />
                    <img
                      src={myaccount_white}
                      alt="My account"
                      className={styles.hovericon}
                    />
                    <h3>My account</h3>
                  </Dropdown.Item>
                  <Dropdown.Item
                    href="#"
                    className={styles.dropdownItem}
                    onClick={handleOnLogOut}
                  >
                    <img
                      src={logout}
                      alt="Logout"
                      className={styles.defaulticon}
                    />
                    <img
                      src={logout_white}
                      alt="My account"
                      className={styles.hovericon}
                    />
                    <h3>Logout</h3>
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            )}

            <div className={styles.userDataSection}>
              <LocaleSelector />
            </div>
            <div className={styles.mobielMenuIcon} onClick={openNav}>
              <img src={MobileMenuIcon} alt="Brand Logo" />
            </div>
          </div>
        </>
      </div>

      <div className={`${styles.sidenav} ${isSideNavOpen ? styles.open : ""}`}>
        {/* <div className={styles.sidenavCloseContainer}>
          <img src="https://stagecdn.waosim.com/landingpage/assets/brand_logo.svg" alt="Brand Logo" />
          <span className={styles.closebtn} onClick={closeNav}>
            Close
          </span>
        </div> */}
        <div className={styles.firstMenuContainer}>
          {navigation?.map(
            ({ title, openNewTab, url }: any, currentIndex: number) => (
              <div
                key={`${title}-${currentIndex}`}
                className={styles.menuLinks}
              >
                <p className={styles.listName}>{title}</p>
              </div>
            )
          )}
        </div>
      </div>
    </div>
  );
};

export default DesktopHeader;
