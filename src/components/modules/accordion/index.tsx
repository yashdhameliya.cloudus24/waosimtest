import React, { useState } from "react";
import styles from "./accordion.module.scss";
import LazyImage from "../lazyImage";
import { Accordion, Button } from "react-bootstrap";
import classNames from "classnames";
import Link from "next/link";
import { slugify } from "@/utils";
import Skeleton from "react-loading-skeleton";

const CustomAccordion = ({ data, isPopular }: any) => {
  const [activeKey, setActiveKey] = useState<any>(null);
  const displayData = isPopular ? data?.slice(0, 5) : data;

  const handleToggle = (key: any) => {
    setActiveKey(activeKey === key ? null : key);
  };

  return (
    <>
      <Accordion activeKey={activeKey} className={styles.accordianMain}>
        <div className={styles.customAccordion}>
          {displayData ? (
            displayData?.map((plansItem: any, index: number) => {
              return (
                <div
                  className={styles.all_regions_section}
                  key={`plansItem=${index}`}
                >
                  <div
                    className={`${styles.accordion_heading}`}
                    style={{ justifyContent: "center" }}
                  >
                    <div
                      className={classNames(
                        styles.accordion_header,
                        activeKey == index && styles.activeBorder
                      )}
                    >
                      <div className={styles.accordian_data}>
                        <div className={styles.accordian_leftside}>
                          <div className={styles.accordian_country_image}>
                            <LazyImage
                              image={{
                                src: plansItem?.package_plan_image,
                                alt: plansItem?.name,
                              }}
                              className={styles.gridSection_card_image}
                              disableSkeleton
                            />
                          </div>
                          <div className={styles.country_text}>
                            {plansItem?.name.length > 0 ? (
                              <h2>{plansItem?.name}</h2>
                            ) : null}

                            <div className={styles.destination_button}>
                              <h4>{`${
                                plansItem?.mccs?.length || 0
                              } DESTINATION`}</h4>
                              <Button
                                className={styles.checklist_button}
                                onClick={() => handleToggle(`${index}`)}
                              >
                                Check List
                              </Button>
                            </div>
                          </div>
                        </div>
                        <div className={styles.accordian_rightside}>
                          <Link
                            href={`/plans/regions/${slugify(
                              plansItem?.name
                            )}?id=${plansItem?.id}`}
                            key={index}
                          >
                            <Button className={styles.checkplan_button}>
                              Check Plan
                            </Button>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Accordion.Collapse eventKey={`${index}`}>
                    <div className={styles.accordion_body}>
                      <div className="row">
                        {plansItem?.mccs?.map(
                          (plansItem: any, index: number) => {
                            return (
                              <div
                                className="col-lg-2 col-md-3 col-sm-4 col-6 mb-4"
                                key={`planList-${index}`}
                              >
                                <div
                                  key={`countryList-${index}`}
                                  className={styles.accordian_country_image}
                                >
                                  <LazyImage
                                    image={{
                                      src: plansItem?.country_plan_image,
                                      alt: plansItem?.country_name,
                                    }}
                                    className={styles.gridSection_card_image}
                                  />

                                  <div
                                    className={styles.gridSection_countryName}
                                  >
                                    {plansItem?.country_name && (
                                      <span>{plansItem?.country_name}</span>
                                    )}
                                  </div>
                                </div>
                              </div>
                            );
                          }
                        )}
                      </div>
                    </div>
                  </Accordion.Collapse>
                </div>
              );
            })
          ) : (
            <Skeleton
              count={6}
              className={styles.skeletonClass}
              baseColor="#EEF1F5"
              highlightColor="#ffffff"
            />
          )}
        </div>
      </Accordion>
      {/* 
      <Accordion defaultActiveKey="0" flush>
        {displayData &&
          displayData?.map((plansItem: any, index: number) => {
            return (
              <Accordion.Item eventKey={`${index}`} key={index}>
                <Accordion.Header className={styles.accodionHeader}>
                asddasasdasdasdasdasd
                </Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                  irure dolor in reprehenderit in voluptate velit esse cillum
                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                  cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </Accordion.Body>
              </Accordion.Item>
            );
          })}
      </Accordion> */}
    </>
  );
};

export default CustomAccordion;
