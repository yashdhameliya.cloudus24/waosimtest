import React from "react";
import styles from "./googlePlayAppStore.module.scss";
import Link from "next/link";
import classNames from "classnames";


const GooglePlayAppStore = ({buttonBorder = false}:any) => {
  return (
    <div className={classNames(styles.googlePlayAppStore,!buttonBorder && styles.buttonMain)}>
      <Link href="/">
        <div className={classNames(styles.googlePlayAppStore_image,!buttonBorder && styles.buttonBorder)}>
          <img src="https://stagecdn.waosim.com/landingpage/home/appStore.svg" alt="app store" />
        </div>
      </Link>
      <Link href="/">
        <div className={styles.googlePlayAppStore_image}>
          <img
            src="https://stagecdn.waosim.com/landingpage/icons/googlePlayStore.png"
            alt="google play"
          />
        </div>
      </Link>
    </div>
  );
};

export default GooglePlayAppStore;
