import React, { useEffect, useState } from "react";
import styles from "./transferEsimDrawer.module.scss";
import classNames from "classnames";
import { InformationIcon } from "@/assets/images/icons";
import {
  TransferEsimToNewAccountRouteApi,
  getEsimStatusRouteApi,
} from "@/utils/api";
import Skeleton from "react-loading-skeleton";
import { toastError, toastSuccess } from "@/utils";
import Spinner from "../spinner";
const arrow_back = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back.svg";
const arrow_back_orange = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back-orange.svg";

const TransferEsimDrawer = ({
  openSectionDetails,
  closeSidebar,
  eSimList,
  setESimList,
  setOpenSectionDetails,
}: any) => {
  const [esimStatus, setEsimStatus] = useState<any>(null);
  const [esimStatusLoader, setEsimStatusLoader] = useState(false);
  const [transferEsimLoader, setTransferEsimLoader] = useState(false);
  const [inputValue, setinputValue] = useState({
    account_id: "",
    email_or_phone: "",
  });

  const getEsimStatus = async () => {
    setEsimStatusLoader(true);
    let payload = {
      iccid: openSectionDetails?.iccid,
    };
    const getEsimStatusRouteApiRes = await getEsimStatusRouteApi({
      method: "POST",
      body: payload,
    });

    if (getEsimStatusRouteApiRes?.success) {
      setEsimStatusLoader(false);

      setEsimStatus(getEsimStatusRouteApiRes?.data);
    } else {
      setEsimStatusLoader(false);
    }
  };
  useEffect(() => {
    getEsimStatus();
  }, []);

  const handleOnClickTransferEsimToNewAcc = async (e: any) => {
    e.preventDefault();

    setTransferEsimLoader(true);
    let payload = {
      esim_id: openSectionDetails?.id,
      account_id: inputValue?.account_id,
      email_or_phone: inputValue?.email_or_phone,
    };
    const transferEsimToNewAccountRes = await TransferEsimToNewAccountRouteApi({
      method: "POST",
      body: payload,
    });

    if (transferEsimToNewAccountRes?.success) {
      setESimList(transferEsimToNewAccountRes?.data);
      setOpenSectionDetails(null);
      setTransferEsimLoader(false);
      toastSuccess("eSIM transfered successfully!");
      closeSidebar();
    } else {
      setTransferEsimLoader(false);
      toastError(transferEsimToNewAccountRes?.message);
    }
  };

  const handlechange = (e: any) => {
    const { name, value } = e.target;
    const emojiRegex = /[\p{Emoji_Presentation}\p{Extended_Pictographic}]/gu;
    let sanitizedValue = value.replace(emojiRegex, "");

    sanitizedValue = sanitizedValue.trimStart();

    const finalValue =
      name === "password" ? sanitizedValue.replace(/\s/g, "") : sanitizedValue;

    if (finalValue?.length > 40) {
      return;
    }

    setinputValue({ ...inputValue, [name]: finalValue });
  };

  return (
    <div className={styles.deleteSideDrawerMain}>
      <div className={styles.deleteMainSection}>
        <div className={styles.deleteHeading}>
          <h1>Transfer this eSIM to different account</h1>
        </div>
        <div className={styles.borderdiv}></div>

        <div className={styles.deleteContain}>
          <form onSubmit={(e) => handleOnClickTransferEsimToNewAcc(e)}>
            {esimStatusLoader ? (
              <Skeleton
                baseColor="#EEF1F5"
                highlightColor="#ffffff"
                height={85}
                className={styles.informationDetailsSkeleton}
              />
            ) : esimStatus?.installed ? (
              <div className={styles.esimInstallStatus}>
                <div className={styles.informationIcon}>
                  <InformationIcon fill="#F47500" />
                </div>

                <div className={styles.informationDetails}>
                  <h5>{`"eSIM is already installed on (${esimStatus?.installed}).”`}</h5>
                  <p>
                    Changing the account will not provide the option to
                    reinstall it on a different device.
                  </p>
                </div>
              </div>
            ) : (
              <div className={styles.esimInstallStatus}>
                <div className={styles.informationIcon}>
                  <InformationIcon fill="#00897F" />
                </div>

                <div className={styles.informationDetails}>
                  <h5>"eSIM has not been installed yet.”</h5>
                  <p>It can be installed on a new device."</p>
                </div>
              </div>
            )}

            <div className={styles.enterCouponBalance}>
              <div className={styles.useReferralCode}>
                <h3>Transfer Account ID</h3>
              </div>
              <input
                type="text"
                name="account_id"
                placeholder="Transfer Account ID"
                value={inputValue?.account_id}
                onChange={handlechange}
                disabled={transferEsimLoader}
                required
              />
            </div>
            <div className={styles.enterCouponBalance}>
              <div className={styles.useReferralCode}>
                <h3>Transfer Account Email / Account Phone Number</h3>
              </div>
              <input
                type="text"
                name="email_or_phone"
                placeholder="Transfer Account Email or Phone Number"
                value={inputValue?.email_or_phone}
                onChange={handlechange}
                disabled={transferEsimLoader}
                required
              />
            </div>
            <div className={styles.coupanApplayButton}>
              <button type="submit" disabled={transferEsimLoader}>
                {transferEsimLoader ? (
                  <Spinner />
                ) : (
                  <>
                    <p>Update</p>
                    <img
                      src={arrow_back}
                      alt="back arrow"
                      className={styles.arrowBack}
                    />
                    <img
                      src={arrow_back_orange}
                      alt="back arrow"
                      className={styles.arrowBackOrange}
                    />
                  </>
                )}
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default TransferEsimDrawer;
