import React, { useState } from "react";
import styles from "./orderSummary.module.scss";
import Button from "@/components/modules/button";
import classNames from "classnames";
const totalPrice = "https://stagecdn.waosim.com/landingpage/assets/order_summary_total_price.png";
const credit_debit_card = "https://stagecdn.waosim.com/landingpage/assets/icons/credit_debit_card.svg";
const apple_pay = "https://stagecdn.waosim.com/landingpage/assets/icons/apple_pay.svg";
const google_pay = "https://stagecdn.waosim.com/landingpage/assets/icons/google_pay.svg";
const arrow_back = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back.svg";
const arrow_back_orange = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back-orange.svg";
const delete_icon = "https://stagecdn.waosim.com/landingpage/assets/icons/delete_icon.svg";
import Modal from "../modal";
import PurchasedSuccessModal from "../purchasedSuccessModal";
import { paymentIntent, signIn } from "@/utils/api";
import { useRouter } from "next/router";
import {
  handleOnApplyPaymentIntent,
  handleOnVerifyPurchase,
} from "@/utils/api/order";
import {
  removeClassToDocumentElement,
  toastError,
  toastSuccess,
} from "@/utils";
import Spinner from "../spinner";

const OrderSummary = ({
  handleOnOpenSideBar,
  reviewOrderData,
  appiedCoupon,
  handleOnRemoveAppiedCoupon,
  setAppiedCoupon,
  discount,
  setDiscount,
  plan_id,
  selectedESim,
  topupId,
}: any) => {
  const route = useRouter();
  const mcc = route?.query?.mcc;
  const region_id = route?.query?.region_id;
  let orderSummeryData = reviewOrderData?.plans[0]?.data[0];

  const [loading, setLoading] = useState(false);

  const [successModel, setSuccessModel] = useState({
    isOpen: false,
    content: null,
  });
  const [errorModel, setErrorModel] = useState(false);

  const [permission, setPermission] = useState({
    eSimSupport: false,
    lockRestrictions: false,
  });

  const [selectedPayment, setSelectedPayment] = useState(0);
  const paymentMethodArray = [
    {
      payment_method_img: credit_debit_card,
      payment_method_alt: "credit_debit_card",
      payment_method_text: "Credit/Debit Card",
      payment_method_description:
        " Visa, Mastercard, AMEX, JCB, Dinner Club,Discover, Union Pay",
    },
    {
      payment_method_img: apple_pay,
      payment_method_alt: "apple_pay",
      payment_method_text: "Apple Pay",
    },
    {
      payment_method_img: google_pay,
      payment_method_alt: "google_pay",
      payment_method_text: "Google Pay",
    },
  ];

  const handleOnChange = (e: any, index: any) => {
    setSelectedPayment(index);
  };

  const handleClick1 = () => {
    handleOnOpenSideBar("useCouponCode");
  };

  const closePopupModel = () => {
    removeClassToDocumentElement("no-scroll");
    setSuccessModel({
      isOpen: false,
      content: null,
    });
  };

  const handleOnSubmitForm = async (e: any) => {
    e.preventDefault();

    if (!permission?.eSimSupport || !permission?.lockRestrictions) {
      toastError(
        "Kindly accept all the terms and conditions to complete your order"
      );
    } else {
      setLoading(true);
      let payload = {
        plan_id: plan_id,
        coupon_type: appiedCoupon?.coupon_type,
        exist_esim_iccid: selectedESim?.iccid,
        is_topup: topupId ? 1 : 0,
        country_mcc: mcc,
        region_id: region_id,
        referral_balance: appiedCoupon?.referral_balance,
        code: appiedCoupon?.code,
      };

      const { isError, response } = await handleOnApplyPaymentIntent({
        body: payload,
      });

      // const paymentIntentRes = await paymentIntent({
      //   method: "POST",
      //   body: payload,
      // });

      if (response?.success) {
        if (response?.data?.payment_type === 1) {
          let purchasePayload = {
            transaction_id: response?.data?.transaction_id,
          };
          const { isError, responseNew } = await handleOnVerifyPurchase({
            body: purchasePayload,
          });

          if (responseNew?.success) {
            setLoading(false);
            setSuccessModel({
              isOpen: true,
              content: {
                ...responseNew?.data,
                title: "eSIM Purchased Successfully",
              },
            });
          } else {
            setLoading(false);
            toastError(responseNew?.message);
            setErrorModel(true);
          }
        } else {
          setLoading(false);
          toastError("first create full payment");
        }
      } else {
        setLoading(false);
        toastError(response?.message);
        setErrorModel(true);
      }
    }
  };

  return (
    <div className={styles.orderSummaryMain}>
      <div className={styles.orderSummaryHeading}>
        <h1>Order Summary</h1>
      </div>
      <div className={styles.orderSummaryBorder}></div>
      <div className={styles.orderSummaryPriceListMain}>
        <div className={styles.priceDetail}>
          <div className={styles.priceDiscountList}>
            <p>Price</p>
            <h2>
              {`$ ${
                discount?.core_price?.toString() ||
                orderSummeryData?.core_price?.toString() ||
                "0"
              }`}{" "}
            </h2>
          </div>
          <div className={styles.priceDiscountList}>
            <p>WaoClub Discount </p>
            <h2>
              {`- $ ${
                discount?.waoclub_discount?.toString() ||
                orderSummeryData?.waoclub_discount?.toString() ||
                " 0"
              }`}{" "}
            </h2>
          </div>
          <div className={styles.priceDiscountList}>
            <p>Deals Discount </p>
            <h2>
              {`- $ ${
                discount?.deals_discount?.toString() ||
                orderSummeryData?.deals_discount?.toString() ||
                "0"
              }`}{" "}
            </h2>
          </div>

          <div className={styles.priceDiscountList}>
            <p>Coupon Discount </p>
            <h2>
              {`- $ ${
                discount?.discount_price?.toString() ||
                orderSummeryData?.discount_price?.toString() ||
                "0"
              }`}{" "}
            </h2>
          </div>
        </div>
        <div className={styles.totalPrice}>
          <div className={styles.totalPriceText}>
            <img src={totalPrice} alt="Total Price" />
            <h1>Total Price</h1>
          </div>
          <div className={styles.displayPriceMain}>
            <h1>
              {`$ ${
                discount?.price?.toString() ||
                orderSummeryData?.price?.toString() ||
                "0"
              }`}{" "}
            </h1>
          </div>
        </div>
      </div>
      <div className={styles.orderSummaryBorder}></div>
      <div className={styles.applayCouponCodeHeading}>
        <h2>Apply Coupon Code</h2>
      </div>

      {appiedCoupon ? (
        <div className={styles.applyActiveCouponCodeButton}>
          <h3>
            {" "}
            {appiedCoupon?.coupon_type === 2
              ? `${appiedCoupon?.code} HAS BEEN APPLIED.`
              : `${appiedCoupon?.percentage}% HAS BEEN APPLIED.`}{" "}
          </h3>
          <button onClick={handleOnRemoveAppiedCoupon}>
            <img src={delete_icon} alt="delete icon" />
          </button>
        </div>
      ) : (
        <div className={styles.applyCouponButton}>
          <Button
            color={"orange"}
            type={"transparent"}
            title={`APPLY / USE COUPON CODE`}
            clickHandler={handleClick1}
          />
        </div>
      )}

      <div className={styles.orderSummaryBorder}></div>

      {appiedCoupon?.percentage === 100 ? null : (
        <>
          <div className={styles.applayCouponCodeHeading}>
            <h2>Please Select Your Payment Method</h2>
          </div>
          <div className={styles.PaymentMethodMain}>
            {paymentMethodArray?.map((items, index) => {
              return (
                <div className={styles.paymentMethodBlock} key={index}>
                  <div className={styles.paymentMethodLeftBlock}>
                    <div className={styles.paymentCardImage}>
                      <img
                        src={items.payment_method_img}
                        alt={items.payment_method_alt}
                      />
                    </div>
                    <div className={styles.paymentMethodName}>
                      <h3>{items.payment_method_text}</h3>
                      <p>{items.payment_method_description}</p>
                    </div>
                  </div>
                  <div className={styles.paymentMethodrightBlock}>
                    <input
                      name="payment"
                      className="radio"
                      type="radio"
                      checked={selectedPayment === index}
                      onChange={(e) => handleOnChange(e, index)}
                    />
                    <label htmlFor="payment"></label>
                  </div>
                </div>
              );
            })}
          </div>
        </>
      )}

      <div className={styles.esimToggleSwitch}>
        <div className={styles.toggle_switch}>
          <label className={styles.switch}>
            <input
              type="checkbox"
              checked={permission?.eSimSupport}
              onChange={() =>
                setPermission({
                  ...permission,
                  eSimSupport: !permission.eSimSupport,
                })
              }
            />
            <span className={classNames(styles.slider, styles.round)}></span>
          </label>
          <p>I am using a device that supports an eSim</p>
        </div>
        <div className={styles.toggle_switch}>
          <label className={styles.switch}>
            <input
              type="checkbox"
              checked={permission?.lockRestrictions}
              onChange={() =>
                setPermission({
                  ...permission,
                  lockRestrictions: !permission.lockRestrictions,
                })
              }
            />
            <span className={classNames(styles.slider, styles.round)}></span>
          </label>
          <p>I am using a device that has no SIM lock restrictions</p>
        </div>
      </div>
      <div className={styles.paymentContinueButton}>
        {/* onClick={openPopupModel}  */}
        <button onClick={handleOnSubmitForm} disabled={loading}>
          {!loading ? (
            <>
              <p>{`$ ${
                discount?.price?.toString() ||
                orderSummeryData?.price?.toString() ||
                "0"
              }`}</p>
              <p>Continue</p>
              <img
                src={arrow_back}
                alt="back arrow"
                className={styles.arrowBack}
              />
              <img
                src={arrow_back_orange}
                alt="back arrow"
                className={styles.arrowBackOrange}
              />
            </>
          ) : (
            <Spinner />
          )}
        </button>
        <Modal
          closePopupModel={closePopupModel}
          isOpen={successModel?.isOpen}
          isOutSideClick={false}
        >
          <PurchasedSuccessModal
            content={successModel?.content}
            closePopupModel={closePopupModel}
          />
        </Modal>
      </div>
    </div>
  );
};
export default OrderSummary;
