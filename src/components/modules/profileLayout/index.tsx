import React from "react";
import styles from "./profileLayout.module.scss";
import Link from "next/link";
import classNames from "classnames";
import { useRouter } from "next/router";
import { useUser } from "@/components/contexts/userContext/context";
import { getItemFromSession } from "@/utils/useHooks/useStorage";
const backBackground = "https://stagecdn.waosim.com/landingpage/assets/nesw_articals_bg.png";
const waosimBorderLogo = "https://stagecdn.waosim.com/landingpage/assets/icons/waosimBorderLogo.svg";
const profileEditIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/profileEditIcon.svg";
const profileTabTopRound = "https://stagecdn.waosim.com/landingpage/assets/icons/profileTabTopRound.svg";
const profileTabBottomRound = "https://stagecdn.waosim.com/landingpage/assets/icons/profileTabBottomRound.svg";
const myPlans = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/myPlans.png";
const contactUs = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/contactUs.png";
const myEsim = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/myEsim.png";
const wishlist = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/wishlist.png";

const ProfileLayout = ({ isRemoveSidePadding = false, children }: any) => {
  const router = useRouter();
  let selectedEsim = getItemFromSession("selectedEsim");

  const { userDetails, handleOnLogOut } = useUser();

  let profilSideBarTab = [
    {
      name: "Plans",
      icon_url: myPlans,
      route: selectedEsim
        ? `/profile/my-plans?iccid=${selectedEsim}`
        : "/profile/my-plans",
    },

    {
      name: "My eSIM",
      icon_url: myEsim,
      route: "/profile/my-esim",
    },
    {
      name: "Wishlist",
      icon_url: wishlist,
      route: "/profile/wishlist",
    },
    {
      name: "Refer & Earn",
      icon_url: wishlist,
      route: "/profile/refer-earn",
    },
    {
      name: "Invoice",
      icon_url: wishlist,
      route: "/profile/invoice",
    },
    {
      name: "Language & Currency",
      icon_url: wishlist,
      route: "/profile/language-currency",
    },
    {
      name: "Settings",
      icon_url: wishlist,
      route: "/profile/settings",
    },
    {
      name: "Archived Plans",
      icon_url: wishlist,
      route: "/profile/archived-plans",
    },
    {
      name: "Notification",
      icon_url: wishlist,
      route: "/profile/notification",
    },
    {
      name: "Contact Us",
      icon_url: contactUs,
      route: "/profile/contact-us",
    },
    {
      name: "Log Out",
      icon_url: wishlist,
      route: "/profile/log-out",
    },
  ];

  return (
    <div>
      <div className={styles.profileLayoutMainSection}>
        <div className={styles.profileLayoutGrid}>
          <div className={styles.menuListSection}>
            <div className={styles.myprofileData}>
              <div className={styles.profileTabTopRoundStyle}>
                <img src={profileTabTopRound} alt="profileTabTopRound" />
              </div>
              <div className={styles.profileTabBottomRoundStyle}>
                <img src={profileTabBottomRound} alt="profileTabBottomRound" />
              </div>

              <img src={waosimBorderLogo} alt="waosimBorderLogo" />

              <div className={styles.myprofileDataCombine}>
                <div className={styles.profileNameId}>
                  <h3>{userDetails?.name}</h3>
                  <p>
                    Account ID : <span> {userDetails?.user_unique_id}</span>
                  </p>
                  <p>{userDetails?.email || userDetails?.mobile}</p>
                </div>
                <img
                  src={profileEditIcon}
                  alt="profileEditIcon"
                  style={{ cursor: "pointer" }}
                />
              </div>
            </div>

            {profilSideBarTab?.map((item: any, index: number) => {
              return (
                <Link href={item?.route} key={`profilSideBarTab-${index}`}>
                  <div
                    className={classNames(
                      styles.myprofileMenuItems,
                      router?.asPath?.includes(item?.route) && styles.activeTab
                    )}
                  >
                    <div className={styles.myprofileMenuItemsImage}>
                      <img src={item?.icon_url} alt={item?.name} />
                    </div>
                    <p>{item?.name}</p>
                  </div>
                </Link>
              );
            })}
          </div>

          <div
            className={classNames(
              styles.profileMenuItemDetails,
              isRemoveSidePadding && styles.sidePaddingRemove
            )}
          >
            {children}
          </div>
        </div>
        <div className={styles.profileLayoutbackBackgroundImage}>
          <img src={backBackground} alt="Backgroung Image" />
        </div>
        <div className={styles.footer_color}></div>
      </div>
    </div>
  );
};

export default ProfileLayout;
