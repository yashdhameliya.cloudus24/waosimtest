import React, { useState, useRef } from "react";
import styles from "./toolTip.module.scss";

type TooltipProps = {
  content: React.ReactNode;
  position: "right" | "left" | "top" | "bottom";
  children: React.ReactElement;
  title: any;
};

export const Tooltip: React.FC<TooltipProps> = ({
  content,
  position,
  title,
  children,
}) => {
  const [isVisible, setIsVisible] = useState(false);
  const childRef = useRef<HTMLDivElement>(null);

  const handleMouseEnter = () => setIsVisible(true);
  const handleMouseLeave = () => setIsVisible(false);

  return (
    <div
      className={styles.tooltipWrapper}
      // ref={childRef}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      {isVisible && (
        <div className={`${styles.tooltip} ${styles[position]}`}>
          <div className={styles.headerToolTip}>
            <h4>{title}</h4>
          </div>
          <div className={styles.headerToolContent}>
            <p>{content}</p>
          </div>
        </div>
      )}
      {React.cloneElement(children, {
        className: `${children.props.className || ""} ${styles.tooltipTarget}`,
      })}
    </div>
  );
};
