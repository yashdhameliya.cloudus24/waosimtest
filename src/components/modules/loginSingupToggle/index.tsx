import React, { useState } from "react";
import styles from "./loginSingupToggle.module.scss";
import { useRouter } from "next/router";
import { usePathname } from "next/navigation";
import Link from "next/link";
const logoImage = "https://stagecdn.waosim.com/landingpage/assets/brand_logo.svg";

const LoginSingupToggle = () => {
  const pathname = usePathname();
  const activeRoute = pathname?.split("/")[2];

  return (
    <div className={styles.loginSingupToggleMain}>
      <div className={styles.brandLogo}>
        <img src={logoImage} alt="logo" />
      </div>

      <div className={styles.toggleSection}>
        <Link
          href={"/auth/login"}
          className={`${styles.option} ${
            activeRoute === "login" ? styles.selected : ""
          }`}
        >
          <span>Sign In</span>
        </Link>
        <Link
          href={"/auth/signup"}
          className={`${styles.option} ${
            activeRoute === "signup" ? styles.selected : ""
          }`}
        >
          <span>Sign Up </span>
        </Link>
      </div>
    </div>
  );
};

export default LoginSingupToggle;
