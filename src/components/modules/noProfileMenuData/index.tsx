import React from "react";
import styles from "./noProfileMenuData.module.scss";
import Button from "../button";
import LazyImage from "../lazyImage";

const NoProfileMenuData = ({
  mainTitle,
  backgroundImage,
  middleTitle,
  buttonTitle,
  buttonLink,
}: any) => {
  return (
    <div className={styles.NoProfileMenuDataContainer}>
      {mainTitle && <h1>{mainTitle}</h1>}

      <div className={styles.noProfileMenuDataMiddle}>
        <LazyImage
          image={{
            src: backgroundImage,
            alt: mainTitle,
          }}
          className={styles.noProfileMenuDataMiddle_image}
          disableSkeleton
        />

        {/* <img src={backgroundImage} alt={mainTitle} /> */}
        {middleTitle && <p>{middleTitle}</p>}
        <div className={styles.noProfileMenuDataMiddle_button}>
          <Button
            color={"solid"}
            type={"orange"}
            title={buttonTitle}
            link={buttonLink}
          />
        </div>
      </div>
    </div>
  );
};

export default NoProfileMenuData;
