import React from "react";
import styles from "./phoneInputCommon.module.scss";
import PhoneInput, { isValidPhoneNumber } from "react-phone-number-input";
const phoneInputIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/phoneInputIcon.svg";
import "react-phone-number-input/style.css"; // Make sure to include the PhoneInput styles

export default function PhoneInputCommon({
  value,
  handlePhoneNumber,
  required,
  name,
  selectedCountryCode,
}: any) {
  return (
    <div className={styles.phoneInputMain}>
      <PhoneInput
        international
        defaultCountry={selectedCountryCode || "IN"}
        countryCallingCodeEditable={false}
        value={value}
        onChange={handlePhoneNumber}
        placeholder={"Enter mobile number"}
        className={styles.phoneInputClass}
        required={required}
        name={name}
        pattern={required ? ".{6,}" : ".{0,}"}
        // pattern={required ? "^[+(s.-/d)]{5,30}$" : null}
        // title={required ? "Please enter a valid phone number." : null}
        error={
          value
            ? isValidPhoneNumber(value)
              ? undefined
              : "Invalid phone number"
            : "Phone number required"
        }
      />

      <div className={styles.inputTextIconphoneInputClass}>
        <img src={phoneInputIcon} alt="phoneInputIcon" />
      </div>
    </div>
  );
}
