import React from "react";
import styles from "./countryCard.module.scss";
import LazyImage from "../lazyImage";
import LazyLoad from "../lazyLoad";
import Skeleton from "react-loading-skeleton";
const esim_background = "https://stagecdn.waosim.com/landingpage/assets/esim_background.png";
const countryCardEsimIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/countryCardEsimIcon.png";

const CountryCard = ({
  countryName,
  countryImage,
  countryFlag,
  isLoading,
}: any) => {
  return (
    <div className={styles.card}>
      <div
        className={styles.cardContainer}
        style={
          countryImage && !isLoading
            ? {
                backgroundImage: `url(${countryImage})`,
              }
            : {}
        }
      >
        {isLoading ? (
          <div
            className={styles.cardCountryImage}
            style={{ position: "relative" }}
          >
            <Skeleton
              style={{ width: "100%", height: "100%" }}
              className={styles.cardCountryImage}
              baseColor="#EEF1F5"
              highlightColor="#ffffff"
            />
          </div>
        ) : null}

        {countryFlag && !isLoading && (
          <div className={styles.cardCountryFlag}>
            <img src={countryFlag} alt="countryFlag" />
          </div>
        )}

        {!isLoading && (
          <div className={styles.cardBlackBackgroundImage}>
            <img src={esim_background} alt="esim_background" />
          </div>
        )}

        {countryName && !isLoading && (
          <div className={styles.cardCountryName}>
            <p> {countryName}</p>
          </div>
        )}

        {!isLoading && (
          <div className={styles.cardEsimSection}>
            <img src={countryCardEsimIcon} alt="countryCardEsimIcon" />
          </div>
        )}
      </div>
    </div>
  );
};

export default CountryCard;
