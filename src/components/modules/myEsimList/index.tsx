import React, { useState } from "react";
import styles from "./myEsimList.module.scss";
import classNames from "classnames";
import Button from "@/components/modules/button";
import { useRouter } from "next/router";
import { setItemInSession } from "@/utils/useHooks/useStorage";
const esim_icon = "https://stagecdn.waosim.com/landingpage/assets/icons/my_new_esim-icon.svg";
const newEsim = "https://stagecdn.waosim.com/landingpage/assets/purchese_new_esim.png";
const imageVectore = "https://stagecdn.waosim.com/landingpage/assets/icons/purchese_new_esim_vectore.svg";
const arrow_back = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back.svg";
const arrow_back_orange = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back-orange.svg";

const MyEsimList = ({
  eSimList,
  setSelectedESim,
  closeSidebar,
  selectedESim,
  isProfileScreen = false,
  setSelectedESimSub,
}: any) => {
  const [activeSim, setActiveSim] = useState(selectedESim);
  const router = useRouter();

  const handleOnGotItNow = () => {
    closeSidebar();
    router.push(`/plans`);
  };

  return (
    <div className={styles.sideBarDrawerMain}>
      <div className={styles.myNewEsimMainSection}>
        <div className={styles.myNewEsimHeading}>
          <h1>My eSIM</h1>
        </div>
        <div className={styles.borderdiv}></div>
        <div className={styles.newEsimListMain}>
          <div className={styles.newEsimDeatilsList}>
            {eSimList?.map((eSimListItem: any, eSimListIndex: number) => {
              return (
                <div
                  className={classNames(
                    styles.newEsimDeatilBlock,
                    activeSim === eSimListItem && styles.activeEsim
                  )}
                  key={eSimListIndex}
                  onClick={() => {
                    setActiveSim(eSimListItem);
                  }}
                >
                  <div className={styles.esim_icon_image}>
                    <img src={esim_icon} alt="esim icon" />
                  </div>
                  <div className={styles.newEsimDeatilText}>
                    <h2>{eSimListItem?.esim_name}</h2>
                    <div className={styles.detail}>
                      <label>ICCID :</label>
                      <span>{eSimListItem?.iccid}</span>
                    </div>
                    {/* <div className={styles.detail}>
                      <label>Installed :</label>
                      <span>{eSimListItem?.installed}</span>
                    </div> */}
                  </div>
                </div>
              );
            })}
            {/* {isProfileScreen ? null : ( */}
            <div className={styles.purchaseNewEsim}>
              <div className={styles.purchaseNewEsim_left}>
                <h1>Do you want to get a new eSIM?</h1>
                <div className={styles.gotItNowButton}>
                  <Button
                    color={"orange"}
                    type={"transparent"}
                    title={`Got It Now`}
                    clickHandler={() => {
                      if (isProfileScreen) {
                        handleOnGotItNow();
                      } else {
                        setSelectedESim(null);
                        closeSidebar();
                        setActiveSim(null);
                      }
                    }}
                  />
                </div>
              </div>
              <div className={styles.purchaseNewEsimImageMain}>
                <div className={styles.purchaseNewEsimImage}>
                  <img src={newEsim} alt="New Esim" />
                </div>
              </div>
              <div className={styles.ImageVectore}>
                <img src={imageVectore} alt="image vectore" />
              </div>
            </div>
            {/* )} */}
          </div>
          {/* <div className={styles.getNewSimSection}>
            <div className={styles.newSimVector}>
              <img src={newSimVector} alt="newSimVector" />
            </div>
            <div className={styles.newSimImage}>
              <img
                src="https://stagecdn.waosim.com/myprofile/noWishlistCardImage.png"
                alt="noWishlistCardImage"
              />
            </div>

            <p>Do you want a new eSIM?</p>

            <Button
              color={"orange"}
              type={"transparent"}
              title={`GOT IT NOW`}
              // link="/plans"
              clickHandler={handleOnGotItNow}
            />
          </div> */}

          <div className={styles.newEsimSubmitButton}>
            <button
              onClick={() => {
                if (isProfileScreen) {
                  setSelectedESimSub(null);
                  setItemInSession("selectedEsim", activeSim?.iccid);
                }
                setSelectedESim(activeSim);
                closeSidebar();
              }}
              disabled={!activeSim}
            >
              <p>Submit</p>
              <img
                src={arrow_back}
                alt="back arrow"
                className={styles.arrowBack}
              />
              <img
                src={arrow_back_orange}
                alt="back arrow"
                className={styles.arrowBackOrange}
              />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyEsimList;
