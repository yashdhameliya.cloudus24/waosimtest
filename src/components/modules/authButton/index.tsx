import React from "react";
import styles from "./authButton.module.scss";
import Spinner from "../spinner";
import classNames from "classnames";

const AuthButton = ({ title, loading }: any) => {
  return (
    <div className={classNames(loading ? styles.disabled : styles.formSubmit)}>
      <button type="submit" disabled={loading}>
        {!loading ? title : <Spinner />}
      </button>
    </div>
  );
};


export default AuthButton;
