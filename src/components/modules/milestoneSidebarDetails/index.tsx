import React from "react";
import styles from "./milestoneSidebarDetails.module.scss";
import Button from "../button";
import FullProgressBar from "../milstoneProgressbar";
import MilestoneBooking from "../milstoneBooking";

const MilestoneSidebarDetails = () => {
  return (
    <div className={styles.milestoneSidebarDetailsMain}>
      <div className={styles.waoClubMilestoneMain}>
        <div className={styles.waoClubMilestoneInner}>
          <div className={styles.MilestoneImage}>
            <img src={"https://stagecdn.waosim.com/landingpage/waoClub/waoClub_milestone_viewDetails.png"} alt="milestone Detail Image"></img>
          </div>
          <div className={styles.MilestoneText}>
            <h1>Gold Level Milestone</h1>
          </div>
          <div className={styles.milestoneButton}>
            <FullProgressBar isText={true} />
          </div>
        </div>
      </div>
      <div className={styles.milestoneBookingProgressBar}>
        <MilestoneBooking />
      </div>
    </div>
  );
};

export default MilestoneSidebarDetails;
