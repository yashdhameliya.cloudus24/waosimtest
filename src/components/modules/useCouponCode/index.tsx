import React, { useEffect, useState } from "react";
import styles from "./useCouponCode.module.scss";
import classNames from "classnames";
import { getReferralBalance } from "@/utils/api/auth";
import { toastError, toastSuccess } from "@/utils";
import { handleOnApplyCoupon } from "@/utils/api/order";
import Skeleton from "react-loading-skeleton";
import { useRouter } from "next/router";
const arrow_back = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back.svg";
const arrow_back_orange = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back-orange.svg";

const UseCouponCode = ({
  plan_id,
  setAppiedCoupon,
  handleOnCloseSideBar,
  reviewOrderData,
  setReviewOrderData,
  setDiscount,
}: any) => {
  const route = useRouter();
  const mcc = route?.query?.mcc;
  const [couponBalance, setCouponBalance] = useState("");
  const [referralCode, setReferralCode] = useState("");
  const [referralBalance, setReferralBalance] = useState<any>(null);
  const [loaging, setLoading] = useState(false);
  const [couponLoaging, setCouponLoaging] = useState(false);

  const handleCouponChange = (e: any) => {
    const value = e.target.value;
    const regex = /^\d+$/;
    setReferralCode("");
    if (regex.test(value)) {
      const numberValue = parseInt(value, 10);
      if (numberValue > 0 && numberValue <= 100) {
        setCouponBalance(value);
      } else {
        setCouponBalance("");
      }
    } else {
      setCouponBalance("");
    }
  };

  const handleReferralChange = (e: any) => {
    const value = e.target.value;
    const emojiRegex = /[\p{Emoji_Presentation}\p{Extended_Pictographic}]/gu;
    let sanitizedValue = value.replace(emojiRegex, "");

    setCouponBalance("");
    setReferralCode(sanitizedValue);
  };

  const hanldeApplyCoupon = async (e: any) => {
    e.preventDefault();
    if (couponBalance) {
      setCouponLoaging(true);

      let payload;
      if (mcc) {
        payload = {
          coupon_type: 1, // (1 => referral balance, 2 => coupon code or season  code)
          plan_id: plan_id,
          referral_balance: +couponBalance,
          mcc: mcc,
        };
      } else {
        payload = {
          coupon_type: 1, // (1 => referral balance, 2 => coupon code or season  code)
          plan_id: plan_id,
          referral_balance: +couponBalance,
        };
      }

      const { isError, response } = await handleOnApplyCoupon({
        body: payload,
      });

      if (response?.success) {
        // const updatedPlans = reviewOrderData?.plans?.map((plan: any) => {
        //   const updatedData = plan?.data?.map((data: any) => {
        //     return { ...data, ...response?.data };
        //   });
        //   return { ...plan, data: updatedData };
        // });

        // setReviewOrderData({ ...reviewOrderData, plans: updatedPlans });

        setDiscount(response?.data);
        // toastSuccess(response?.message);
        setAppiedCoupon({
          percentage: +couponBalance,
          coupon_type: 1,
          referral_balance: +couponBalance,
        });
        handleOnCloseSideBar();
        setCouponLoaging(false);
      } else {
        toastError(response?.message);
        setCouponLoaging(false);
      }
    } else {
      toastError("The entered value exceeds your available coupon balance.");
      setCouponLoaging(false);
    }
  };

  const handleApplyReferralCode = async (e: any) => {
    e.preventDefault();

    if (referralCode) {
      setCouponLoaging(true);
      let payload;
      if (mcc) {
        payload = {
          coupon_type: 2, // (1 => referral balance, 2 => coupon code or season  code)
          plan_id: plan_id,
          code: referralCode,
          mcc: mcc,
        };
      } else {
        payload = {
          coupon_type: 2, // (1 => referral balance, 2 => coupon code or season  code)
          plan_id: plan_id,
          code: referralCode,
        };
      }

      const { isError, response } = await handleOnApplyCoupon({
        body: payload,
      });

      if (response?.success) {
        setDiscount(response?.data);
        // toastSuccess(response?.message);
        setAppiedCoupon({
          percentage: +response?.data?.discount_percentage,
          coupon_type: 2,
          code: referralCode,
        });

        handleOnCloseSideBar();
        setCouponLoaging(false);
      } else {
        toastError(response?.message);
        setCouponLoaging(false);
      }
    }
  };

  async function fetchReferralBalance() {
    try {
      setLoading(true);
      const { isError, response } = await getReferralBalance();
      setReferralBalance(response?.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.error(error);
    }
  }

  useEffect(() => {
    fetchReferralBalance();
  }, []);

  return (
    <div className={styles.applyCouponCodeSection}>
      <div className={styles.applyCouponCodeHeading}>
        <h1>APPLY / USE COUPON CODE</h1>
      </div>
      <div className={styles.borderdiv}></div>
      <div className={styles.coupanApplayMain}>
        {!loaging ? (
          <div className={styles.myCoupanBalanceMain}>
            <h2>My Coupon Balance</h2>
            <div className={styles.couponBalanceCode}>
              <h2>
                {!loaging ? (
                  referralBalance?.referral_balance ? (
                    `${referralBalance?.referral_balance}%`
                  ) : (
                    `0%`
                  )
                ) : (
                  <Skeleton width={115} />
                )}
              </h2>
            </div>
          </div>
        ) : (
          <Skeleton className={styles.myCoupanBalanceMainSkeleton} />
        )}
        <form onSubmit={(e) => hanldeApplyCoupon(e)}>
          <div className={styles.enterCouponBalance}>
            <input
              type="number"
              required
              name="coupon"
              placeholder="0%"
              value={couponBalance}
              onChange={handleCouponChange}
              disabled={couponLoaging}
            />
          </div>
          <div className={styles.coupanApplayButton}>
            <button type="submit" disabled={couponLoaging}>
              <p>Apply</p>
              <img
                src={arrow_back}
                alt="back arrow"
                className={classNames(
                  styles.arrowBack,
                  !couponBalance && styles.imageDisable
                )}
              />
              <img
                src={arrow_back_orange}
                alt="back arrow"
                className={classNames(
                  styles.arrowBackOrange,
                  !couponBalance && styles.imageDisable
                )}
              />
            </button>
          </div>
        </form>
        <div className={styles.couponOrOption}>
          <p>OR</p>
          <div className={styles.borderOr}></div>
        </div>
        <div className={styles.useReferralCode}>
          <h3>Use referral code or coupon code</h3>
        </div>
        <form onSubmit={(e) => handleApplyReferralCode(e)}>
          <div className={styles.enterCouponBalance}>
            <input
              type="text"
              name="referral"
              placeholder="Please enter your code"
              value={referralCode}
              onChange={handleReferralChange}
              disabled={couponLoaging}
              required
            />
          </div>
          <div className={styles.coupanApplayButton}>
            <button type="submit" disabled={couponLoaging}>
              <p>Apply</p>
              <img
                src={arrow_back}
                alt="back arrow"
                className={styles.arrowBack}
              />
              <img
                src={arrow_back_orange}
                alt="back arrow"
                className={styles.arrowBackOrange}
              />
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default UseCouponCode;
