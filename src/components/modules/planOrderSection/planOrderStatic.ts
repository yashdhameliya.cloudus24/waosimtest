
const hotspot = "https://stagecdn.waosim.com/landingpage/assets/icons/order_hotspot.svg";
const activation_policy = "https://stagecdn.waosim.com/landingpage/assets/icons/order_activation_policy.svg";
const cancellation_policy = "https://stagecdn.waosim.com/landingpage/assets/icons/order_cancellation_policy.svg";
const plan_expiration = "https://stagecdn.waosim.com/landingpage/assets/icons/order_plan_expiration.svg";
const top_up = "https://stagecdn.waosim.com/landingpage/assets/icons/order_top_up.svg";
const installtion = "https://stagecdn.waosim.com/landingpage/assets/icons/order_installtion.svg";
const re_installtion = "https://stagecdn.waosim.com/landingpage/assets/icons/order_re_installtion.svg";


export const AdditionInfoArray = [
  {
    InfoIcon: hotspot,
    InfoName: "Hotspot",
    InfoDetail:
      "Yes, the availability of data sharing may vary depending on the destination",
  },
  {
    InfoIcon: activation_policy,
    InfoName: "Activation Policy",
    InfoDetail:
      "The validity period begins when the eSIM connects to any supported network",
  },
  {
    InfoIcon: cancellation_policy,
    InfoName: "Cancellation Policy",
    InfoDetail: "You have thirty (30) days from your purchase date to cancel",
  },
  {
    InfoIcon: plan_expiration,
    InfoName: "Plan Expiration",
    InfoDetail:
      "The plan expires six months from the purchase date if not used",
  },
  {
    InfoIcon: top_up,
    InfoName: "Top-Up",
    InfoDetail:
      "Yes, additional credit will be activated the day after your current plan ends",
  },
  {
    InfoIcon: installtion,
    InfoName: "Installation",
    InfoDetail:
      "eSIM can only be installed and used in one device. Once it's installed, it can't be used or transferred to another device",
  },
  {
    InfoIcon: re_installtion,
    InfoName: "Re-Installation",
    InfoDetail:
      "You can reinstall the eSIM on the same device only up to ten times",
  },
];
