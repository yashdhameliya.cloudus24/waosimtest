import React from "react";
import styles from "./planOrderSection.module.scss";
import Button from "@/components/modules/button";
import { AdditionInfoArray } from "./planOrderStatic";
import Skeleton from "react-loading-skeleton";
const newEsim = "https://stagecdn.waosim.com/landingpage/assets/purchese_new_esim.png";
const imageVectore = "https://stagecdn.waosim.com/landingpage/assets/icons/purchese_new_esim_vectore.svg";
const imageVectore_regions = "https://stagecdn.waosim.com/landingpage/assets/icons/plaOrder_regions_new_esim_img.svg";
const Provider = "https://stagecdn.waosim.com/landingpage/assets/icons/order_provider.svg";
const plan_coverage = "https://stagecdn.waosim.com/landingpage/assets/icons/order_coverage.svg";
const plan = "https://stagecdn.waosim.com/landingpage/assets/icons/order_plan.svg";
const validity = "https://stagecdn.waosim.com/landingpage/assets/icons/order_validity.svg";
const data = "https://stagecdn.waosim.com/landingpage/assets/icons/order_data.svg";
const plan_type = "https://stagecdn.waosim.com/landingpage/assets/icons/order_plan_type.svg";
const phone_number = "https://stagecdn.waosim.com/landingpage/assets/icons/order_phone_number.svg";
const esim_icon = "https://stagecdn.waosim.com/landingpage/assets/icons/my_new_esim-orange-icon.svg";

const PlanOrderSection = ({
  handleOnOpenSideBar,
  reviewOrderData,
  eSimList,
  selectedESim,
  loaging,
  topupId,
}: any) => {
  const planOrderArray = [
    {
      plan_img: plan,
      plan_detail: "Plan",
      plan_dataInfo: reviewOrderData?.name || "",
      plan_alt: "Plan",
    },
    {
      plan_img: validity,
      plan_detail: "Validity",
      plan_dataInfo:
        `${reviewOrderData?.plans[0]?.data[0]?.plan_days} Days` || "",
      plan_alt: "Validity",
    },
    {
      plan_img: data,
      plan_detail: "Data",
      plan_dataInfo:
        `${reviewOrderData?.plans[0]?.data_name} ${
          reviewOrderData?.plans[0]?.data_type === "Fix" ? `` : `/ Day`
        } ` || "",
      plan_alt: "Data",
    },
    {
      plan_img: plan_type,
      plan_detail: "Plan Type",
      plan_dataInfo: reviewOrderData?.plan_type_text || "",
      plan_alt: "Plan Type",
    },
    {
      plan_img: phone_number,
      plan_detail: "Phone Number (SMS)",
      plan_dataInfo: reviewOrderData?.phone_number_sms || "",
      plan_alt: "Phone Number",
    },
    {
      plan_img: data,
      plan_detail: "Network",
      plan_dataInfo: reviewOrderData?.network || "",
      plan_alt: "Network",
    },
  ];

  return (
    <div className={styles.reviewPlanOrderMain}>
      <div className={styles.planoOrderHeading}>
        <h1>Review Your Order</h1>
      </div>
      <div className={styles.planOrderDetailMain}>
        <div className={styles.OrderInformationMain}>
          {planOrderArray?.map((items, index) => {
            return (
              <div
                className={styles.countryInfoMain}
                key={`planOrder=${index}`}
              >
                <div className={styles.countryInfoCard}>
                  <div className={styles.planImage}>
                    <img src={items?.plan_img} alt={items?.plan_alt} />
                  </div>

                  <p>{items?.plan_detail}</p>
                  <div className={styles.planDataInfo}>
                    <h2>{items?.plan_dataInfo}</h2>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        {/* Sim Detail */}

        {!loaging ? (
          selectedESim ? (
            <div className={styles.simDetailSection}>
              <div className={styles.simDetail}>
                <h1>Sim Detail</h1>
              </div>
              <div className={styles.simDetailSection_inner}>
                <div className={styles.newEsimDeatilBlock}>
                  <div className={styles.newesimDetail}>
                    <div className={styles.esim_icon_image}>
                      <img src={esim_icon} alt="esim icon" />
                    </div>
                    <div className={styles.newEsimDeatilText}>
                      {selectedESim && <h2>{selectedESim?.esim_name}</h2>}
                      <div className={styles.detail}>
                        <label>ICCID :</label>
                        {selectedESim && <span>{selectedESim?.iccid}</span>}
                      </div>
                      {/* <div className={styles.detail}>
                        <label>Installed :</label>
                        {selectedESim && <span>{selectedESim?.installed}</span>}
                      </div> */}
                    </div>
                  </div>
                  {!topupId && (
                    <div
                      className={styles.purchaseNewEsimImage_regions}
                      onClick={() => {
                        handleOnOpenSideBar("myESim");
                      }}
                    >
                      <img
                        src={imageVectore_regions}
                        alt="image vectore_regions"
                      />
                    </div>
                  )}
                </div>
              </div>
              <div className={styles.purchaseNewEsimImageMain}></div>
            </div>
          ) : (
            <div className={styles.purchaseNewEsim}>
              <h1>Purchase for a new eSIM</h1>
              <div className={styles.purchaseNewEsimImageMain}>
                {eSimList?.length > 0 && (
                  <div
                    className={styles.purchaseNewEsimImage_regions}
                    onClick={() => {
                      handleOnOpenSideBar("myESim");
                    }}
                  >
                    <img
                      src={imageVectore_regions}
                      alt="image vectore_regions"
                    />
                  </div>
                )}

                <div className={styles.purchaseNewEsimImage}>
                  <img src={newEsim} alt="New Esim" />
                </div>
              </div>
              <div className={styles.ImageVectore}>
                <img src={imageVectore} alt="image vectore" />
              </div>
            </div>
          )
        ) : (
          <Skeleton className={styles.simDetailSection} height={165.19} />
        )}

        {/* Do you want to get a new eSIM? */}

        {/* <div className={styles.purchaseNewEsim}>
          <div className={styles.purchaseNewEsim_left}>
            <h1>Do you want to get a new eSIM?</h1>
            <div className={styles.gotItNowButton}>
              <Button
                color={"orange"}
                type={"transparent"}
                title={`Got It Now`}
              />
            </div>
          </div>
          <div className={styles.purchaseNewEsimImageMain}>
            <div className={styles.purchaseNewEsimImage}>
              <img src={newEsim} alt="New Esim" />
            </div>
          </div>
          <div className={styles.ImageVectore}>
            <img src={imageVectore} alt="image vectore" />
          </div>
        </div> */}

        <div className={styles.additionalInfoMain}>
          <h1>Additional Information</h1>
          <div className={styles.reviewYourOrderBlockMain}>
            {reviewOrderData?.coverage_data?.length > 0 && (
              <div className={styles.reviewOrderBlock}>
                <div className={styles.blockLeftData}>
                  <div className={styles.dataImage}>
                    <img src={plan_coverage} alt="coverage" />
                  </div>
                  <div className={styles.dataDescription}>
                    <p>Coverage</p>
                    <h3>{reviewOrderData?.coverage_data?.length}</h3>
                  </div>
                </div>
                <div className={styles.checkListButton}>
                  <Button
                    color={"solid"}
                    type={"orange"}
                    title={`Check List`}
                    clickHandler={() => handleOnOpenSideBar("coverageList")}
                  />
                </div>
              </div>
            )}

            {reviewOrderData?.providers?.length > 0 && (
              <div className={styles.reviewOrderBlock}>
                <div className={styles.blockLeftData}>
                  <div className={styles.dataImage}>
                    <img src={Provider} alt="Provider" />
                  </div>
                  <div className={styles.dataDescription}>
                    <p>Provider</p>
                    <h3>{reviewOrderData?.providers[0]}</h3>
                  </div>
                </div>
                <div className={styles.checkListButton}>
                  <Button
                    color={"solid"}
                    type={"orange"}
                    title={`Check List`}
                    clickHandler={() => handleOnOpenSideBar("providerList")}
                  />
                </div>
              </div>
            )}

            {AdditionInfoArray?.map((items, index) => {
              return (
                <div className={styles.reviewOrderBlock} key={index}>
                  <div className={styles.blockLeftData}>
                    <div className={styles.dataImage}>
                      <img src={items?.InfoIcon} alt="Provider" />
                    </div>
                    <div className={styles.dataDescription}>
                      <p>{items?.InfoName}</p>
                      <h3>{items?.InfoDetail}</h3>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default PlanOrderSection;
