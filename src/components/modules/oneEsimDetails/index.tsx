import React, { useContext, useEffect, useState } from "react";
import styles from "./oneEsimDetails.module.scss";
const qrCodeLogo = "https://stagecdn.waosim.com/landingpage/assets/qrCodeLogo.png";
import { QRCode } from "react-qrcode-logo";
import { toastError, toastSuccess } from "@/utils";
import { CopyIcon } from "@/assets/images/icons";
import classNames from "classnames";
import { useSidebar } from "@/components/contexts/sidebarContext/context";
import TransferEsimDrawer from "../transferEsimDrawer";
import DeleteSideDrawer from "../deleteSideDrawer";
import Image from "next/image";
import { DeleteEsim, HideUnHideEsim, getEsimStatusRouteApi } from "@/utils/api";
import { GlobalContext } from "@/components/contexts/globalContext/context";
import UpateEsimNameDrawer from "../upateEsimNameDrawer";
import { useRouter } from "next/router";
import moment from "moment-timezone";
const cpuSetting = "https://stagecdn.waosim.com/landingpage/assets/icons/cpuSetting.svg";
const delete_icon_light = "https://stagecdn.waosim.com/landingpage/assets/icons/delete_icon_light.png";
const updateIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/updateIcon.png";
const eyeHideIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/eyeHideIcon.png";
const eyeUnHideIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/eyeUnHideIcon.svg";
const transferIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/transferIcon.png";
const backIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/backIcon.svg";

const OneEsimDetails = ({
  openSectionDetails,
  setOpenSectionDetails,
  setESimList,
  eSimList,
}: any) => {
  const { openSidebar, closeSidebar } = useSidebar();
  const [updateLoader, setUpdateLoader] = useState(false);
  const { setIsglobalLoaderActive } = useContext(GlobalContext);

  const router = useRouter();
  const [eSimActiveStatus, setESimActiveStatus] = useState({
    installed: "",
    installTime: "",
    installCount: "",
    iccid: null,
  });

  const iccidCardArray = [
    {
      key: "sm_dp_address",
      label: "SM-DP+ Address",
      value: openSectionDetails?.sm_dp_address,
    },
    {
      key: "activation_code",
      label: "Android Activation Code",
      value: openSectionDetails?.activation_code,
    },
    {
      key: "ios_activation_code",
      label: "iOS Activation Code",
      value: openSectionDetails?.ios_activation_code,
    },
  ];

  const installStatusArray = [
    {
      key: "install_device",
      label: "Installation Device ",
      value: eSimActiveStatus?.installed,
    },
    {
      key: "install_date",
      label: "Installation Date",
      value: moment(eSimActiveStatus?.installTime).format("YYYY/M/D"),
    },
    {
      key: "reinstall_times",
      label: "Reinstall Times",
      value: `${eSimActiveStatus?.installCount ?? 0}/10`,
    },
  ];

  const handleOnCopy = (textToCopy: any) => {
    if (textToCopy) {
      navigator.clipboard
        .writeText(textToCopy)
        .then(() => {
          toastSuccess("copied successfully!");
        })
        .catch((err) => {
          toastError("Failed to copy!");
        });
    }
  };

  const handleOnCancleDeleteDrawer = (e: any) => {
    e.preventDefault();
    setESimActiveStatus({
      installed: "",
      installTime: "",
      installCount: "",
      iccid: null,
    });
    if (router?.query?.iccid) {
      router.push(
        {
          pathname: router.pathname,
          query: null,
        },
        undefined,
        { shallow: true }
      );
    }
    setOpenSectionDetails(null);
    closeSidebar();
  };

  const handleOnDeletePlan = async (e: any) => {
    e.preventDefault();

    setUpdateLoader(true);
    let payload = {
      esim_id: openSectionDetails?.id,
    };
    const deleteEsimRes = await DeleteEsim({
      method: "POST",
      body: payload,
    });

    if (deleteEsimRes?.success) {
      setUpdateLoader(false);

      let newEsim = eSimList?.filter(
        (item: any, index: any) => item?.id !== openSectionDetails?.id
      );
      setESimList(newEsim);
      setOpenSectionDetails(null);
      toastSuccess("eSIM delete successfully!");
      closeSidebar();
    } else {
      setUpdateLoader(false);
      toastError(deleteEsimRes?.message);
    }
  };

  const handleOnHidePlanScreen = async () => {
    setIsglobalLoaderActive(true);
    let payload = {
      iccid: openSectionDetails?.iccid,
      is_hide: openSectionDetails?.is_hide === 0 ? 1 : 0,
    };
    const hideUnHideEsimRes = await HideUnHideEsim({
      method: "POST",
      body: payload,
    });

    if (hideUnHideEsimRes?.success) {
      let newEsim = eSimList?.map((item: any, index: any) => {
        if (item?.iccid === hideUnHideEsimRes?.data?.iccid) {
          return { ...item, ...hideUnHideEsimRes?.data };
        } else {
          return item;
        }
      });
      setESimList(newEsim);
      setIsglobalLoaderActive(false);
      setOpenSectionDetails({
        ...openSectionDetails,
        ...hideUnHideEsimRes?.data,
      });
      toastSuccess(
        hideUnHideEsimRes?.data?.is_hide === 0
          ? `eSIM unhide successfully!`
          : `eSIM hide successfully!`
      );
      closeSidebar();
    } else {
      setIsglobalLoaderActive(false);
      toastError(hideUnHideEsimRes?.message);
    }
  };

  const handleOnOpenSideBar = (type: any) => {
    openSidebar(
      <>
        {type === "updateEsimName" && (
          <UpateEsimNameDrawer
            initialValue={openSectionDetails}
            setESimList={setESimList}
            eSimList={eSimList}
            closeSidebar={closeSidebar}
            openSectionDetails={openSectionDetails}
            setOpenSectionDetails={setOpenSectionDetails}
          />
        )}

        {type === "transferEsim" && (
          <TransferEsimDrawer
            openSectionDetails={openSectionDetails}
            closeSidebar={closeSidebar}
            setESimList={setESimList}
            eSimList={eSimList}
            setOpenSectionDetails={setOpenSectionDetails}
          />
        )}

        {type === "deleteEsim" && (
          <DeleteSideDrawer
            title={`Delete eSIM`}
            description={`Are you sure you want to delete your plan?`}
            handleOnDeletePlan={handleOnDeletePlan}
            handleOnCancle={handleOnCancleDeleteDrawer}
            updateLoader={updateLoader}
          />
        )}
      </>
    );
  };

  const fetchEimStatus = async () => {
    setIsglobalLoaderActive(true);
    let payload = {
      iccid: openSectionDetails?.iccid,
    };
    const getEsimStatusRouteApiRes = await getEsimStatusRouteApi({
      method: "POST",
      body: payload,
    });

    if (getEsimStatusRouteApiRes?.success) {
      setESimActiveStatus({
        ...getEsimStatusRouteApiRes?.data,
        iccid: openSectionDetails?.iccid,
      });
      setIsglobalLoaderActive(false);
    } else {
      setIsglobalLoaderActive(false);
    }
  };

  useEffect(() => {
    if (eSimActiveStatus?.iccid !== openSectionDetails?.iccid) {
      fetchEimStatus();
    }
  }, [openSectionDetails]);

  return (
    <div className={styles.oneEsimDetailsContainer}>
      <div className={styles.backToPlaneButton}>
        <button onClick={handleOnCancleDeleteDrawer}>
          <Image src={backIcon} width={12} height={12} alt="backIcon" />
          <span>Back to My eSIM</span>
        </button>
      </div>

      <div className={styles.esimNameTag}>
        <p>{openSectionDetails?.esim_name}</p>
      </div>

      <div className={styles.alldetialsWithQr}>
        <div className={styles.alldetialsWithQrLeft}>
          <div className={styles.alldetialsWithQrLeftChild}>
            <div className={styles.myEsimQrCode}>
              <div className={styles.myEsimQrCodeImage}>
                <QRCode
                  value={openSectionDetails?.activation_code}
                  logoImage={qrCodeLogo}
                  bgColor={"transparent"}
                  removeQrCodeBehindLogo={true}
                  // logoWidth={50}
                  // logoHeight={50}
                  fgColor={"#979797"}
                  style={{
                    width: "100%",
                    height: "100%",
                  }}
                />
              </div>
            </div>

            <div className={styles.iccidCard}>
              <div className={styles.iccidCardInner}>
                <div className={styles.myPlanactiveIconBack}>
                  <img src={cpuSetting} alt="cpuSetting" />
                </div>
                <div className={styles.detaisSectionCombaine}>
                  <div className={styles.detaisSection}>
                    <h4>ICCID :</h4>
                    <p>{openSectionDetails?.iccid}</p>
                  </div>
                </div>
              </div>

              <div className={styles.middleLine}></div>
              {iccidCardArray?.map(
                (item) =>
                  item?.value && (
                    <div className={styles.iccidCardInner} key={item.key}>
                      <div className={styles.myPlanactiveIconBack}>
                        <img src={cpuSetting} alt="cpuSetting" />
                      </div>
                      <div className={styles.detaisSectionCombaine}>
                        <div className={styles.detaisSection}>
                          <h4>{item.label} :</h4>
                          <p>{item.value}</p>
                        </div>
                        <div onClick={() => handleOnCopy(item.value)}>
                          <CopyIcon fill="#F47500" isRoundShape={true} />
                        </div>
                      </div>
                    </div>
                  )
              )}

              <div className={styles.middleLine}></div>
              {installStatusArray?.map(
                (item) =>
                  item?.value && (
                    <div className={styles.iccidCardInner} key={item.key}>
                      <div className={styles.myPlanactiveIconBack}>
                        <img src={cpuSetting} alt="cpuSetting" />
                      </div>
                      <div className={styles.detaisSectionCombaine}>
                        <div className={styles.detaisSection}>
                          <h4>{item.label} :</h4>
                          <p>{item.value}</p>
                        </div>
                      </div>
                    </div>
                  )
              )}
            </div>
          </div>
        </div>

        <div className={styles.updateHideButtonConatiner}>
          <div
            className={styles.updateHideButton}
            onClick={() => handleOnOpenSideBar("updateEsimName")}
          >
            <img src={updateIcon} alt="updateIcon" />
            <p>Update eSIM Name</p>
          </div>
          <div
            className={classNames(styles.updateHideButton, styles.yellowButton)}
            onClick={handleOnHidePlanScreen}
          >
            <img
              src={
                openSectionDetails?.is_hide === 1 ? eyeUnHideIcon : eyeHideIcon
              }
              alt="eyeHideIcon"
            />
            <p>{`${
              openSectionDetails?.is_hide === 1 ? "Unhide" : "Hide"
            } From Plan Screen`}</p>
          </div>
          <div
            className={classNames(styles.updateHideButton, styles.blueButton)}
            onClick={() => handleOnOpenSideBar("transferEsim")}
          >
            {" "}
            <img src={transferIcon} alt="transferIcon" />
            <p>Transfer eSIM</p>
          </div>
          <div
            className={classNames(styles.updateHideButton, styles.greyButton)}
            onClick={() => handleOnOpenSideBar("deleteEsim")}
          >
            {" "}
            <img src={delete_icon_light} alt="delete_icon_light" />
            <p>Delete eSIM</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OneEsimDetails;
