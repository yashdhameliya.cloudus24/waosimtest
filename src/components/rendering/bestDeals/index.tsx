import React, { useEffect, useRef, useState } from "react";
import styles from "./bestDeals.module.scss";
import MainTitle from "@/components/modules/mainTitle";
import classNames from "classnames";
import Button from "@/components/modules/button";
import LazyImage from "@/components/modules/lazyImage";
import Slider from "react-slick";
const number1 = "https://stagecdn.waosim.com/landingpage/assets/number1.png";
const number2 = "https://stagecdn.waosim.com/landingpage/assets/number2.png";
const number3 = "https://stagecdn.waosim.com/landingpage/assets/number3.png";
const number4 = "https://stagecdn.waosim.com/landingpage/assets/number4.png";
const number5 = "https://stagecdn.waosim.com/landingpage/assets/number5.png";

const screenArray = [
  {
    srcnumber: number1,
    src: "https://stagecdn.waosim.com/landingpage/home/waoSimWorkCard1.png",
    alt: "Screen 1",
    desc1: "Welcome to Waosim,",
    desc2: "your gateway to global",
    desc3: "connectivity! 🚀",
  },
  {
    srcnumber: number2,
    src: "https://stagecdn.waosim.com/landingpage/home/waoSimWorkCard2.png",
    alt: "Screen 2",
    desc1: "Explore plans from 150+",
    desc2: "countries and unlock a world",
    desc3: " of connectivity! 🌍",
  },
  {
    srcnumber: number3,
    src: "https://stagecdn.waosim.com/landingpage/home/waoSimWorkCard3.png",
    alt: "Screen 3",
    desc1: "Find your perfect fit with our",
    desc2: "amazing packages designed to",
    desc3: "cater to your every need! 🎁",
  },
  {
    srcnumber: number4,
    src: "https://stagecdn.waosim.com/landingpage/home/waoSimWorkCard4.png",
    alt: "Screen 4",
    desc1: "Quick and simple",
    desc2: "Install and manage your",
    desc3: "eSIM with ease!",
  },
  {
    srcnumber: number5,
    src: "https://stagecdn.waosim.com/landingpage/home/waoSimWorkCard5.png",
    alt: "Screen 5",
    desc1: "Spark the Connectivity",
    desc2: "Activate, Monitor, and Ride the",
    desc3: "Internet Wave! 🌟",
  },
];
const BestDeals = ({ data }: any) => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const percentageSliderRef = useRef<Slider | null>(null);
  const nameSliderRef = useRef<Slider | null>(null);
  const imageSliderRef = useRef<Slider | null>(null);

  const handleSlideChange = (oldIndex: number, newIndex: number) => {
    setCurrentSlide(newIndex);
  };

  useEffect(() => {
    if (percentageSliderRef.current) {
      percentageSliderRef.current.slickGoTo(currentSlide);
    }
    if (nameSliderRef.current) {
      nameSliderRef.current.slickGoTo(currentSlide);
    }
    if (imageSliderRef.current) {
      imageSliderRef.current.slickGoTo(currentSlide);
    }
  }, [currentSlide]);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    vertical: true,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: false,
    arrows: false,
    beforeChange: handleSlideChange,
  };

  const settings1 = {
    ...settings,
    fade: true,
  };

  return (
    <div className={styles.bestDealsSection}>
      <div className="container">
        <div className={styles.bestDealsMain}>
          <MainTitle text="Best <span>Deals</span>" />

          <div className={styles.getBestDealsMain}>
            <div className="row align-items-center">
              <div className="col-md-8">
                <div className={styles.getBestDealsLeft}>
                  <div className={styles.getBestDealsTitle}>
                    <h4>
                      Get <span>Best </span>Deals
                    </h4>
                    <div className={styles.inlineContent}>
                      Get extra&nbsp;
                      <div className={styles.percentageSliderDesign}>
                        <Slider {...settings} ref={percentageSliderRef}>
                          {data?.map((item: any, index: any) => (
                            <p key={index}>{`${item?.deal_percentage}%`}</p>
                          ))}
                        </Slider>
                      </div>
                      &nbsp;discount on &nbsp;
                      <div className={styles.nameSliderDesign}>
                        <Slider {...settings} ref={nameSliderRef}>
                          {data?.map((item: any, index: any) => (
                            <p key={index}>{item?.country_name}</p>
                          ))}
                        </Slider>
                      </div>
                    </div>
                    <div className={styles.discoverNowButton}>
                      <Button
                        color={"orange"}
                        type={"transparent"}
                        title={`Discover Now`}
                        link="/best-deals"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className={styles.getBestDealsRight}>
                  <div className={styles.planDetailMainDiv}>
                    <Slider {...settings1} ref={imageSliderRef}>
                      {data?.map((item: any, index: any) => {
                        return (
                          <div
                            className={styles.planImage}
                            key={`countryName=${index}`}
                          >
                            <div className={styles.country_image}>
                              <LazyImage
                                image={{
                                  src: item?.country_image,
                                  alt: item?.country_name,
                                }}
                                className={
                                  styles.gridSection_country_flag_design
                                }
                                disableSkeleton
                              />
                            </div>
                            <div className={styles.planecoupan}>
                              <p>Discount</p>
                              <h1>{item?.deal_percentage}%</h1>
                            </div>
                            <div className={styles.esimcoupandiv}>
                              <div className={styles.esimcoupanImg}>
                                <img
                                  src="https://stagecdn.waosim.com/landingpage/assets/esim_icon.png"
                                  alt="esim_coupon"
                                />
                              </div>
                            </div>
                            <div className={styles.gridSection_background}>
                              <div
                                className={styles.gridSection_background_image}
                              >
                                <img
                                  src="https://stagecdn.waosim.com/landingpage/assets/esim_background.png"
                                  alt="esim_background"
                                />
                              </div>
                            </div>
                            <div className={styles.gridSection_countryName}>
                              <span>{item?.country_name}</span>
                            </div>

                            <div className={styles.gridSection_country_flag}>
                              <LazyImage
                                image={{
                                  src: item?.country_flag,
                                  alt: item?.country_name,
                                }}
                                className={
                                  styles.gridSection_country_flag_design
                                }
                                disableSkeleton
                              />
                            </div>
                          </div>
                        );
                      })}
                    </Slider>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={styles.howWoasimWorkSection}>
        <MainTitle text="How <span>WaoSim</span> Works" />

        <div className={styles.howWaosimScreenMain}>
          <div className="container">
            <div className={styles.AllwaosimScreen}>
              {screenArray.map((item, index) => {
                return (
                  <div
                    className={classNames(
                      styles.waosimScreen,
                      index % 2 != 0 && styles.backBackground
                    )}
                    key={`screenArray - ${index}`}
                  >
                    <div className={styles.number}>
                      <img src={item?.srcnumber} alt="number" />
                    </div>
                    <div className={styles.waosimScreenImage}>
                      <img src={item?.src} alt={item?.alt} />
                    </div>

                    <div className={styles.text}>
                      <p>{item?.desc1}</p>
                      <p>{item?.desc2}</p>
                      <p>{item?.desc3}</p>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div
          className={classNames(
            styles.bestDealsMain,
            styles.getInstantDiscount
          )}
        >
          <div className={styles.getBestDealsMain}>
            <div className="row align-items-center">
              <div className="col-md-8">
                <div className={styles.getBestDealsLeft}>
                  <div className={styles.getBestDealsTitle}>
                    <h4>Get Instant Disconts</h4>
                    <p>Adventure Awaits: Dive into Wao Rewards!</p>
                  </div>
                  <div className={styles.esayCloudMain}>
                    <div className={styles.cloudDiv}>
                      <img src="https://stagecdn.waosim.com/landingpage/assets/icons/basil_cloud.svg" />
                      <p>Easy to Get</p>
                    </div>
                    <div className={styles.cloudDiv}>
                      <img src="https://stagecdn.waosim.com/landingpage/assets/icons/basil_cloud.svg" />
                      <p>Easy to Keep</p>
                    </div>
                    <div className={styles.cloudDiv}>
                      <img src="https://stagecdn.waosim.com/landingpage/assets/icons/basil_cloud.svg" />
                      <p>Easy to Grow</p>
                    </div>
                  </div>
                  <div className={styles.learnMore_Button}>
                    <Button
                      color={"orange"}
                      type={"transparent"}
                      title={`Learn More`}
                    />
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className={styles.getBestDealsRight}>
                  <div className={styles.planDetailMainDiv}>
                    <div className={styles.getBestDealsImage}>
                      <img
                        src="https://stagecdn.waosim.com/landingpage/home/getInstantDiscount.png"
                        alt="getInstant_Discount"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BestDeals;
