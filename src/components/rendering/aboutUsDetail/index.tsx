import React from "react";
import styles from "./aboutUsDetail.module.scss";
import Link from "next/link";
const backBackground = "https://stagecdn.waosim.com/landingpage/assets/about_us_paper_textured_background.png";
const backBackground2 = "https://stagecdn.waosim.com/landingpage/assets/about_us_paper_textured_background2.png";
const s1 = "https://stagecdn.waosim.com/landingpage/assets/aboutUs_image1.png";

const AboutUsDetail = () => {
  return (
    <div className={styles.AboutUsDetailSection}>
      <div className={styles.AboutUsDetailDetailContainer}>
        {/* Block 1 */}
        <div className={styles.AboutUsBlockMainOdd}>
          <div className={styles.AboutBlockBannerImage}>
            <img src={backBackground} alt="backgroundImage" />

            <div className={styles.AboutBlockInnerContant}>
              <div className={styles.aboutTextDetail}>
                <p>
                  WaoSim is more than just an ordinary eSIM store; it is a
                  revolutionary solution that addresses the common frustration
                  of exorbitant roaming bills faced by traveler's and digital
                  nomads alike. With WaoSim, you can bid farewell to the hassle
                  of searching for local SIM cards or dealing with expensive
                  international roaming plans from traditional carriers.
                </p>
              </div>

              <div className={styles.AboutBlockImage}>
                <img
                  src="https://stagecdn.waosim.com/landingpage/commonimage/aboutUs_image1.png"
                  alt="AboutUs Image1"
                />
              </div>
            </div>
          </div>
        </div>
        {/* Block 2 */}
        <div className={styles.AboutUsBlockMainEven}>
          <div className={styles.AboutBlockBannerImage}>
            <img src={backBackground2} alt="backgroundImage2" />

            <div className={styles.AboutBlockInnerContant}>
              <div className={styles.AboutBlockImage}>
                <img
                  src="https://stagecdn.waosim.com/landingpage/commonimage/aboutUs_image2.png"
                  alt="AboutUs Image2"
                />
              </div>
              <div className={styles.aboutTextDetail}>
                <p>
                  At the core of WaoSim's service lies the availability of 150+
                  eSIMs from various regions and network providers across the
                  globe. <br />
                  These eSIMs offer the same convenience and functionality as
                  physical SIM cards but without the need for a physical swap.{" "}
                  <br />
                  Instead, you can instantly switch between different eSIM
                  profiles right on your device, allowing you to access local
                  data, calls, and texts seamlessly as you move between
                  countries.
                </p>
              </div>
            </div>
          </div>
        </div>
        {/* Block 3 */}
        <div className={styles.AboutUsBlockMainOdd}>
          <div className={styles.AboutBlockBannerImage}>
            <img src={backBackground} alt="backgroundImage" />

            <div className={styles.AboutBlockInnerContant}>
              <div className={styles.aboutTextDetail}>
                <p>
                  The key advantage of WaoSim is affordability. By offering
                  eSIMs at competitive prices, WaoSim empowers travellers to
                  stay connected without the fear of returning home to massive
                  roaming charges. With the wide range of eSIM options
                  available, you can select the most suitable plan for your
                  needs, whether you're on a short vacation, extended business
                  trip, or an adventurous world tour.
                </p>
              </div>

              <div className={styles.AboutBlockImage}>
                <img
                  src="https://stagecdn.waosim.com/landingpage/commonimage/aboutUs_image3.png"
                  alt="AboutUs Image3"
                />
              </div>
            </div>
          </div>
        </div>
        {/* Block 4 */}
        <div className={styles.AboutUsBlockMainEven}>
          <div className={styles.AboutBlockBannerImage}>
            <img src={backBackground2} alt="backgroundImage2" />

            <div className={styles.AboutBlockInnerContant}>
              <div className={styles.AboutBlockImage}>
                <img
                  src="https://stagecdn.waosim.com/landingpage/commonimage/aboutUs_image4.png"
                  alt="AboutUs Image4"
                />
              </div>
              <div className={styles.aboutTextDetail}>
                <p>
                  WaoSim's user-friendly platform ensures that you can manage
                  and activate your eSIMs effortlessly. No more waiting in long
                  queues or dealing with language barriers to purchase local SIM
                  cards.
                  <br />
                  With just a few taps on your smartphone, you can access a
                  world of connectivity possibilities.
                </p>
              </div>
            </div>
          </div>
        </div>
        {/* Block 5 */}
        <div className={styles.AboutUsBlockMainOdd}>
          <div className={styles.AboutBlockBannerImage}>
            <img src={backBackground} alt="backgroundImage" />

            <div className={styles.AboutBlockInnerContant}>
              <div className={styles.aboutTextDetail}>
                <p>
                  The security and reliability of WaoSim's eSIM service are
                  top-notch, providing peace of mind during your travels. Rest
                  assured that your personal information remains protected, and
                  you'll experience consistent network coverage, ensuring a
                  smooth digital experience wherever you go.
                </p>
              </div>

              <div className={styles.AboutBlockImage}>
                <img
                  src="https://stagecdn.waosim.com/landingpage/commonimage/aboutUs_image5.png"
                  alt="AboutUs Image5"
                />
              </div>
            </div>
          </div>
        </div>
        {/* Block 6 */}
        <div className={styles.AboutUsBlockMainEven}>
          <div className={styles.AboutBlockBannerImage}>
            <img src={backBackground2} alt="backgroundImage2" />

            <div className={styles.AboutBlockInnerContant}>
              <div className={styles.AboutBlockImage}>
                <img
                  src="https://stagecdn.waosim.com/landingpage/commonimage/aboutUs_image6.png"
                  alt="AboutUs Image6"
                />
              </div>
              <div className={styles.aboutTextDetail}>
                <p>
                  Say goodbye to high roaming bills and welcome a new era of
                  affordable, hassle-free connectivity with WaoSim's extensive
                  selection of eSIMs. Stay connected, stay flexible, and explore
                  the world without boundaries. WaoSim has got you covered.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutUsDetail;
