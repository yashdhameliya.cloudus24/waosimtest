import React, { useEffect, useState } from "react";
import styles from "./reviewPlanOrder.module.scss";
import OrderSummary from "@/components/modules/orderSummary";
import UseCouponCode from "@/components/modules/useCouponCode";
import PlanOrderSection from "@/components/modules/planOrderSection";
import Link from "next/link";
import { useRouter } from "next/router";
import { slugify } from "@/utils";
import { useSidebar } from "@/components/contexts/sidebarContext/context";
import MyEsimList from "@/components/modules/myEsimList";
const backBackground = "https://stagecdn.waosim.com/landingpage/assets/nesw_articals_bg.png";
const double_right = "https://stagecdn.waosim.com/landingpage/assets/icons/icon-park-outline_double-right.svg";

const ReviewPlanOrder = ({
  reviewOrderData,
  plan_id,
  eSimList,
  setReviewOrderData,
  loaging,
  topupId
}: any) => {
  const route = useRouter();

  const [selectedESim, setSelectedESim] = useState<any>(null);
  const [appiedCoupon, setAppiedCoupon] = useState<any>(null);
  const [discount, setDiscount] = useState<any>(null);
  const { openSidebar, closeSidebar } = useSidebar();

  const handleOnOpenSideBar = (type: any) => {
    openSidebar(
      <>
        {type === "myESim" && (
          <MyEsimList
            closeSidebar={closeSidebar}
            eSimList={eSimList}
            setSelectedESim={setSelectedESim}
            selectedESim={selectedESim}
          />
        )}

        {type === "coverageList" && (
          <div className={styles.regionListSection}>
            <div className={styles.regionsHeading}>
              <h1>{reviewOrderData?.name}</h1>
              <span>
                {`${reviewOrderData?.coverage_data?.length} Destinations`}{" "}
              </span>
            </div>
            <div className={styles.borderdiv}></div>
            {reviewOrderData?.coverage_data && (
              <div className={styles.regionsDetailList}>
                {reviewOrderData?.coverage_data?.map(
                  (coverageItam: string, provideIndex: number) => {
                    return (
                      <p key={`provideIndex - ${provideIndex}`}>
                        {coverageItam}
                      </p>
                    );
                  }
                )}
              </div>
            )}
          </div>
        )}

        {type === "providerList" && (
          <div className={styles.regionListSection}>
            <div className={styles.regionsHeading}>
              <h1>Provider</h1>
              <span>{`${reviewOrderData?.providers?.length} Providers`} </span>
            </div>
            <div className={styles.borderdiv}></div>
            {reviewOrderData?.providers && (
              <div className={styles.regionsDetailList}>
                {reviewOrderData?.providers?.map(
                  (providerItam: string, provideIndex: number) => {
                    return (
                      <p key={`provideIndex - ${provideIndex}`}>
                        {providerItam}
                      </p>
                    );
                  }
                )}
              </div>
            )}
          </div>
        )}

        {type === "useCouponCode" && (
          <UseCouponCode
            plan_id={plan_id}
            setAppiedCoupon={setAppiedCoupon}
            handleOnCloseSideBar={closeSidebar}
            reviewOrderData={reviewOrderData}
            setReviewOrderData={setReviewOrderData}
            setDiscount={setDiscount}
          />
        )}
      </>
    );
  };

  useEffect(() => {
    eSimList && setSelectedESim(eSimList[0]);
  }, [eSimList]);

  const handleOnRemoveAppiedCoupon = () => {
    setAppiedCoupon(null);
    setDiscount(null);
  };

  return (
    <div className={styles.reviewPlanOrderSection}>
      <div className={styles.breadcrumbMain}>
        <ol>
          <li>
            <Link
              href={
                route?.query?.mcc
                  ? `/plans/countries/${slugify(reviewOrderData?.name)}?mcc=${
                      route?.query?.mcc
                    }`
                  : `/plans/regions/${slugify(reviewOrderData?.name)}?id=${
                      route?.query?.region_id
                    }`
              }
            >
              {reviewOrderData?.name}
            </Link>
          </li>
          <span aria-hidden="true" className={styles.breadcrumbSeparator}>
            <img src={double_right} alt="double right" />
          </span>
          <li className={styles.active}>
            <Link href={route.asPath}>Review your order</Link>
          </li>
        </ol>
      </div>
      <div className={styles.reviewPlanOrderContainer}>
        <PlanOrderSection
          handleOnOpenSideBar={handleOnOpenSideBar}
          handleOnCloseSideBar={closeSidebar}
          reviewOrderData={reviewOrderData}
          eSimList={eSimList}
          selectedESim={selectedESim}
          loaging={loaging}
          topupId={topupId}
        />
        <OrderSummary
          handleOnOpenSideBar={handleOnOpenSideBar}
          reviewOrderData={reviewOrderData}
          appiedCoupon={appiedCoupon}
          handleOnRemoveAppiedCoupon={handleOnRemoveAppiedCoupon}
          setAppiedCoupon={setAppiedCoupon}
          discount={discount}
          setDiscount={setDiscount}
          plan_id={plan_id}
          selectedESim={selectedESim}
          topupId={topupId}
        />
      </div>
      <div className={styles.backBackgroundImage}>
        <img src={backBackground} alt="Backgroung Image" />
      </div>
      <div className={styles.footer_color}></div>
    </div>
  );
};

export default ReviewPlanOrder;
