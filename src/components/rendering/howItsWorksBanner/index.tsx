import React from "react";
import styles from "./howItsWorksBanner.module.scss";
import Link from "next/link";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const HowItsWorksBanner = () => {
  return (
    <div>
      <div className={styles.howItsWorksBannerMainSection}>
        <div className={styles.howItsWorksHeroBannerSectionBottomImage}>
          <img src={"https://stagecdn.waosim.com/landingpage/assets/page_bg_bottom.png"} alt="plans Banner Imag" />
        </div>

        <div className={styles.howItsWorksHeroBannerMain}>
          <div className={styles.backgroundCloudeImage}>
            <img src={cloudLayer} alt="cloudLayer" />
          </div>
          <div className="container-fluid">
            <div className={styles.howItsWorksContainer}>
              <div className={styles.howItsWorksHeroBannerLeft}>
                <div className={styles.howItsWorksHeroBannerTitle}>
                  <h1>How It Works</h1>
                </div>
              </div>

              <div className={styles.howItsWorksHeroBannerRightMain}>
                <div className={styles.howItsWorksHeroBannerRight}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/commonimage/howItsWork_banner_img.png"
                    }
                    alt="how It's Work Banner Image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HowItsWorksBanner;
