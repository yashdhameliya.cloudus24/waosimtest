import React, { useContext, useState } from "react";
import styles from "./contactDetails.module.scss";
import { Row } from "react-bootstrap";
import "react-phone-number-input/style.css";
import { ArrowBack } from "@/assets/images/icons";
import { toastError, toastSuccess } from "@/utils";
import { contactUsFormWithOutLogin, signupNewUser } from "@/utils/api";
import { GlobalContext } from "@/components/contexts/globalContext/context";
import MainTitle from "@/components/modules/mainTitle";

const ContactDetails = () => {
  const [formData, setFormData] = useState<any>({
    name: "",
    email: "",
    phoneNumber: "",
    subject: "",
    message: "",
  });
  const { setIsglobalLoaderActive } = useContext(GlobalContext);

  const handlechange = (e: any) => {
    const { name, value } = e.target;
    const emojiRegex = /[\p{Emoji_Presentation}\p{Extended_Pictographic}]/gu;
    let sanitizedValue = value.replace(emojiRegex, "");

    sanitizedValue = sanitizedValue.trimStart();

    if (
      name !== "message" &&
      name !== "subject" &&
      sanitizedValue?.length > 40
    ) {
      return; // Exit the function if the value exceeds 40 characters
    }

    if (name === "agreeCoditions") {
      setFormData({ ...formData, [name]: !formData?.agreeCoditions });
    } else {
      setFormData({ ...formData, [name]: sanitizedValue });
    }
  };

  const handleOnSubmitForm = async (e: any) => {
    e.preventDefault();

    setIsglobalLoaderActive(true);

    let payload = {
      full_name: formData?.name,
      email: formData?.email,
      phone_number: formData?.phoneNumber,
      subject: formData?.subject,
      message: formData?.message,
    };

    const response = await contactUsFormWithOutLogin({
      method: "POST",
      body: payload,
    });
    if (response?.success) {
      setIsglobalLoaderActive(false);
      setFormData({
        name: "",
        email: "",
        phoneNumber: "",
        subject: "",
        message: "",
      });
      toastSuccess(
        "Thank you for contacting us, we will get back to you shortly."
      );
    } else {
      setIsglobalLoaderActive(false);
      toastError(response?.message);
    }
  };

  return (
    <>
      <div className={styles.contactDetailSection}>
        <div className={styles.contactDetailContainer}>
          <div className={styles.contactDetailTitle}>
            <MainTitle text="Send <span>Message</span> With <span>Us</span>" />
            <p>One of our eSIM experts will help you right away.</p>
          </div>
          <div className={styles.contactDetailFormMain}>
            <form onSubmit={(e) => handleOnSubmitForm(e)}>
              <div className={styles.contactDetailFormInput}>
                <Row className={styles.row}>
                  <div className="col-md-4">
                    <label htmlFor="name">Full Name</label>
                    <input
                      required
                      name="name"
                      className="form-control"
                      placeholder="Enter you full name"
                      type="text"
                      value={formData?.name}
                      onChange={(e) => handlechange(e)}
                    />
                  </div>

                  <div className="col-md-4">
                    <label htmlFor="email">Email address</label>
                    <input
                      required
                      name="email"
                      className="form-control"
                      placeholder="Enter your registered email"
                      type={"email"}
                      value={formData?.email}
                      onChange={(e) => handlechange(e)}
                    />
                  </div>

                  <div className="col-md-4">
                    <label htmlFor="phoneNumber">Phone Number</label>

                    <input
                      // required
                      name="phoneNumber"
                      className="form-control"
                      placeholder="Enter your phone number"
                      type="phone"
                      value={formData?.phoneNumber}
                      onChange={(e) => handlechange(e)}
                    />
                  </div>
                </Row>
                <Row className={styles.row}>
                  <div className="col-md-12">
                    <label htmlFor="subject">Subject</label>

                    <input
                      required
                      name="subject"
                      className="form-control"
                      placeholder="Enter your subject"
                      type="text"
                      value={formData?.subject}
                      onChange={(e) => handlechange(e)}
                    />
                  </div>
                </Row>
                <Row className={styles.row}>
                  <div className="col-md-12">
                    <label htmlFor="message">Message</label>

                    <textarea
                      required
                      name="message"
                      className="form-control"
                      placeholder="Description"
                      value={formData?.message}
                      onChange={(e) => handlechange(e)}
                      rows={4.5}
                    />
                  </div>
                </Row>
              </div>
              <div className={styles.submitButton}>
                <button type="submit">
                  <p>Submit</p>
                  <ArrowBack />
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContactDetails;
