import React, { useRef, useState } from "react";
import styles from "./helpCenterDetail.module.scss";
import classNames from "classnames";
import { CloseIcon, SearchIcon } from "@/assets/images/icons";
import FrequetlyAskedQuestion from "../frequentlyAskedQuestion";
import { useRouter } from "next/router";
import MainTitle from "@/components/modules/mainTitle";
const backarrow = "https://stagecdn.waosim.com/landingpage/assets/icons/help_center_rightArrow.svg";
const leftarrow = "https://stagecdn.waosim.com/landingpage/assets/icons/help_center_left_Arrow.svg";

const HelpCenterDetail = ({ data }: any) => {
  const [showDropdown, setShowDropdown] = useState(true);
  const [searchQuery, setSearchQuery] = useState("");
  const [filteredOptions, setFilteredOptions] = useState<any>([]);
  const router = useRouter();
  const helpCenterDetailRef = useRef<any>(null); // Add ref to helpCenterDetailSection div

  const { activefaq, id } = router?.query;
  const activeFaqData = data?.categoryData?.find(
    (item: any) => item?.status == activefaq
  );

  const handleInputChange = (e: any) => {
    let searchTerm = e.target.value;
    if (searchTerm.length === 1 && searchTerm[0] === " ") {
      return;
    }

    setSearchQuery(e.target.value);
    const lowerCaseSearchTerm = searchTerm.toLowerCase();

    const filteredfaqSet =
      data?.allData
        ?.filter(
          (faqQue: any) =>
            faqQue?.question?.toLowerCase().includes(lowerCaseSearchTerm) &&
            faqQue?.category !== 0
        )
        .map((faqQue: any) => ({
          highlighttitle:
            searchTerm.length > 0
              ? faqQue?.question.replace(
                  new RegExp(searchTerm, "ig"),
                  (match: any) => `<span class="queryHighlight">${match}</span>`
                )
              : faqQue?.question,
          title: faqQue?.question,
          activefaq: faqQue?.category,
          ...faqQue,
        })) || [];

    let finalArray = lowerCaseSearchTerm?.length > 0 ? [...filteredfaqSet] : [];
    setFilteredOptions(finalArray);
    setShowDropdown(true);
  };

  const handleDropdownToggle = () => {
    setShowDropdown(!showDropdown);
    if (searchQuery?.length < 1) {
      setFilteredOptions([]);
    }
  };

  const handleOnSetActivefaq = (id: any, subId: any) => {
    router.push(
      {
        pathname: router.pathname,
        query: {
          ...router.query,
          activefaq: id,
          id: subId,
        },
      },
      undefined,
      { shallow: true }
    );
    if (helpCenterDetailRef.current) {
      const topOffset = 180; // Offset to account for the header
      const elementPosition =
        helpCenterDetailRef.current.getBoundingClientRect().top;
      const offsetPosition = elementPosition + window.pageYOffset - topOffset;

      window.scrollTo({
        top: offsetPosition,
        behavior: "smooth",
      });
    }
  };

  return (
    <div ref={helpCenterDetailRef} className={styles.helpCenterDetailSection}>
      <div className={styles.helpCenterDetailContainer}>
        {activefaq ? (
          <div className={styles.helpCenterCatergoriesDetail}>
            <div className={styles.CatergoriesDetailLeft}>
              <div
                className={styles.backtoHelpCenterDiv}
                onClick={() => handleOnSetActivefaq(null, null)}
              >
                <img src={leftarrow} alt="left arrow" />
                <h4>Back to Helpcenter</h4>
              </div>
              <div className={styles.categoryMainBlockDiv}>
                {data?.categoryData?.map((item: any, index: number) => {
                  return (
                    <div
                      className={classNames(
                        styles.categoryBlock,
                        activefaq == item?.status && styles.active
                      )}
                      key={`categoryData-${item?.name}`}
                      onClick={() => handleOnSetActivefaq(item?.status, null)}
                    >
                      <h4>{item?.name}</h4>
                      <img src={backarrow} alt="back arrow" />
                    </div>
                  );
                })}
              </div>
            </div>
            <div className={styles.CatergoriesDetailRight}>
              <h1>{activeFaqData?.name}</h1>
              <FrequetlyAskedQuestion
                data={activeFaqData?.list}
                isSingleLine={true}
                activeId={id}
              />
            </div>
          </div>
        ) : (
          <div className={styles.howWeCanHelpCenterMain}>
            <div className={styles.mainTitleContainer}>
              <h2>
                How <span>Can</span> We<span> Help?</span>
              </h2>
            </div>
            <div className={styles.helpCenterquestionsMain}>
              <div className={styles.searchinput}>
                <input
                  type="search"
                  className="form-control"
                  placeholder="What do you need help with?"
                  aria-label="Recipient's username"
                  aria-describedby="basic-addon2"
                  value={searchQuery}
                  onFocus={handleDropdownToggle}
                  onBlur={() => {
                    setTimeout(() => setShowDropdown(false), 200);
                  }}
                  onChange={handleInputChange}
                />

                <div className={styles.inputButtons}>
                  {!searchQuery ? (
                    <div className={styles.searchIcon}>
                      <SearchIcon fill="white" />
                    </div>
                  ) : (
                    <div
                      className={styles.closeIcon}
                      onClick={() => {
                        setShowDropdown(!showDropdown);
                        setSearchQuery("");
                        setFilteredOptions([]);
                      }}
                    >
                      <CloseIcon fill="#A9A9A9" />
                    </div>
                  )}
                </div>

                {showDropdown && filteredOptions?.length > 0 && (
                  <ul className={styles.dropdownFaq}>
                    <div className={styles.dropdownmain}>
                      {filteredOptions?.map((option: any, index: any) => (
                        <div
                          key={index}
                          className={styles.dropMenuNew}
                          onClick={() =>
                            handleOnSetActivefaq(option?.activefaq, option?.id)
                          }
                        >
                          <p
                            dangerouslySetInnerHTML={{
                              __html: option?.highlighttitle,
                            }}
                          />
                        </div>
                      ))}
                    </div>
                  </ul>
                )}
              </div>

              <div className={styles.helpCenterCatergories}>
                <h3>Catergories</h3>
                <div className={styles.catergoriesBlockMain}>
                  {data?.categoryData
                    ?.filter((category: any) => category?.icon !== null)
                    ?.map((items: any, index: number) => {
                      return (
                        <div
                          className={styles.catergoriesBlock}
                          key={items?.status}
                          onClick={() =>
                            handleOnSetActivefaq(items?.status, null)
                          }
                        >
                          <div className={styles.catergoriesImage}>
                            <img src={items?.icon} alt={items?.name} />
                          </div>
                          <div className={styles.catergoriesText}>
                            {items?.topName && <h4>{items?.topName}</h4>}
                            {items?.bottomName && <h4>{items?.bottomName}</h4>}
                          </div>
                        </div>
                      );
                    })}
                </div>
              </div>
              <div className={styles.popularQuestionHeading}>
                <MainTitle text="Popular <span>Questions</span>" />
              </div>
              <div className={styles.popularQuestion}>
                <FrequetlyAskedQuestion
                  data={data?.popularfaq}
                  isSingleLine={true}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default HelpCenterDetail;
