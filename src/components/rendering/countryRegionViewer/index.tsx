import React, { useState } from "react";
import styles from "./countryRegionViewer.module.scss";
import MainTitle from "@/components/modules/mainTitle";
import CountryCard from "@/components/modules/countryCard";
import { useRouter } from "next/router";
import CustomAccordion from "@/components/modules/accordion";
import Link from "next/link";
import { slugify } from "@/utils";
import Button from "@/components/modules/button";
const downArrowPlan = "https://stagecdn.waosim.com/landingpage/assets/icons/downArrowPlan.svg";

const CountryRegionViewer = ({
  data,
  loading,
  setPageCount,
  pageCount,
  isNextPage,
}: any) => {
  const router = useRouter();
  const selectedOption = router?.query?.activetab || "countries";

  const handleShowMore = () => {
    setPageCount(pageCount + 1);
  };

  return (
    <div className={styles.countryRegionViewerMain}>
      <div className={styles.downArrowPlanIcon}>
        <img src={downArrowPlan} alt="downArrowPlan" />
      </div>
      <div className={styles.countryRegionViewerContainer}>
        <MainTitle
          text={`All <span> ${
            selectedOption === "countries" ? "Countries" : "Regions"
          }</span>`}
        />

        {selectedOption === "countries" ? (
          <div className={styles.countryGridSection}>
            {data?.sortedCountries?.length > 0 &&
              data?.sortedCountries?.map((country: any, index: any) => (
                <Link
                  href={`/plans/countries/${slugify(
                    country?.country_name
                  )}?mcc=${country?.mcc}`}
                  key={index}
                >
                  <CountryCard
                    countryName={country?.country_name}
                    countryFlag={country?.country_flag}
                    countryImage={country?.country_image}
                  />
                </Link>
              ))}

            {loading &&
              Array.from({ length: 15 }).map((_, index) => (
                <React.Fragment key={index}>
                  <CountryCard isLoading />
                </React.Fragment>
              ))}
          </div>
        ) : null}

        {selectedOption === "regions" ? (
          <CustomAccordion data={data?.sortedRegions} />
        ) : null}

        <div className={styles.shoeMoreButton}>
          {selectedOption === "countries" && !loading && isNextPage ? (
            <Button
              color={"solid"}
              type={"orange"}
              title={`Show More`}
              clickHandler={handleShowMore}
            />
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default CountryRegionViewer;
