import React from "react";
import styles from "./aboutEsim.module.scss";
import MainTitle from "@/components/modules/mainTitle";
const Flexibility = "https://stagecdn.waosim.com/landingpage/assets/aboutEsimIcon/Flexibility.png";
const Convenience = "https://stagecdn.waosim.com/landingpage/assets/aboutEsimIcon/Convenience.png";
const SpaceSaving = "https://stagecdn.waosim.com/landingpage/assets/aboutEsimIcon/SpaceSaving.png";
const GlobalConnectivity = "https://stagecdn.waosim.com/landingpage/assets/aboutEsimIcon/GlobalConnectivity.png";
const arrowPlanFullLength = "https://stagecdn.waosim.com/landingpage/assets/icons/arrowPlanFullLength.svg";
const arrowPlanMulti = "https://stagecdn.waosim.com/landingpage/assets/icons/arrowPlanMulti.svg";

const aboutEsimArray = [
  {
    id: 1,
    icon: Flexibility,
    title: "Flexibility",
    description:
      "Users can easily switch between mobile network operators and<br>plans directly from their device, without needing to visit a store or acquire a physical SIM card.",
  },
  {
    id: 2,
    icon: Convenience,
    title: "Convenience",
    description:
      "With eSIM technology, there's no need to worry about losing or damaging a physical SIM card. ",
  },
  {
    id: 3,
    icon: SpaceSaving,
    title: "Space Saving",
    description:
      "eSIMs eliminate the need for a physical SIM card slot in devices, allowing for slimmer and more compact designs.",
  },
  {
    id: 4,
    icon: GlobalConnectivity,
    title: "Global Connectivity",
    description:
      "eSIM technology enables seamless connectivity across multiple mobile networks worldwide",
  },
];

const AboutEsim = () => {
  return (
    <div className={styles.aboutSimSection}>
      <div className={styles.arrowPlanFullLengthIcon}>
        <img src={arrowPlanFullLength} alt="arrowPlanFullLength" />
      </div>
      <div className={styles.arrowPlanMultiIcon}>
        <img src={arrowPlanMulti} alt="arrowPlanMulti" />
      </div>

      <div className={styles.aboutSimSection_container}>
        <div className="row">
          <MainTitle text="About <span>eSim</span>" />
        </div>
        <div className={styles.aboutSimSection_Description}>
          <p>
            eSIM, is a revolutionary technology that replaces the traditional
            physical SIM 
            card in mobile devices. eSIM technology allows users easily to
            switch between 
            mobile network operators and plans without needing to swap out SIM
            cards.
          </p>
        </div>
        <div className={styles.aboutSimSection_grid}>
          {aboutEsimArray?.map((item, index) => {
            return (
              <div
                className={styles.aboutSimSection_grid_card}
                key={`aboutEsimArray-${index}`}
              >
                <div className={styles.aboutSimSection_grid_card_image}>
                  <img src={item?.icon} alt={item?.title} />
                </div>
                <h3>{item?.title}</h3>
                <p dangerouslySetInnerHTML={{ __html: item?.description }}></p>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default AboutEsim;
