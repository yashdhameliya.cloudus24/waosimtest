import React from "react";
import styles from "./customerSupport.module.scss";
import LazyImage from "@/components/modules/lazyImage";
import Button from "@/components/modules/button";
const customerSupport = "https://stagecdn.waosim.com/landingpage/assets/customerSupport.png";
const supportedDevices = "https://stagecdn.waosim.com/landingpage/assets/supportedDevices.png";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const CustomerSupport = () => {
  return (
    <div className={styles.customerSupportContainer}>
      <div className={styles.customerSupportContainer_leftSection}>
        <div
          className={
            styles.customerSupportContainer_leftSection_cloudLayerImage
          }
        >
          <LazyImage
            image={{
              src: cloudLayer,
              alt: "cloudLayer",
            }}
            className={
              styles.customerSupportContainer_leftSection_cloudLayerImage_image
            }
            disableSkeleton={true}
          />
        </div>

        <div className={styles.customerSupportContainer_leftSection_leftColumn}>
          <h2>
            Customer <span>Support</span>{" "}
          </h2>
          <p>Need help? </p>
          <p>
            Reach out to our support team <br />
            Anytime, 24/7. We're here for you!
          </p>
          <div className={styles.customerSupportContainer_button}>
            <Button color={"solid"} type={"black"} title={`Contact Us`} link="/contact" />
          </div>
        </div>
        <div
          className={styles.customerSupportContainer_leftSection_rightColumn}
        >
          <LazyImage
            image={{
              src: customerSupport,
              alt: "customerSupport",
            }}
            className={
              styles.customerSupportContainer_leftSection_rightColumn_image
            }
            disableSkeleton={true}
          />
        </div>
      </div>
      <div className={styles.customerSupportContainer_rightSection}>
        <div
          className={
            styles.customerSupportContainer_rightSection_cloudLayerImage
          }
        >
          <LazyImage
            image={{
              src: cloudLayer,
              alt: "cloudLayer",
            }}
            className={
              styles.customerSupportContainer_rightSection_cloudLayerImage_image
            }
            disableSkeleton={true}
          />
        </div>

        <div
          className={styles.customerSupportContainer_rightSection_leftColumn}
        >
          <h2>
            Supported <span>Devices</span>{" "}
          </h2>
          <p>
            Over 200+ Devices support eSIM, <br />
            Check Compatibility for Yours!
          </p>
          <div className={styles.customerSupportContainer_button}>
            <Button
              color={"solid"}
              type={"black"}
              title={`Check Supported Device`}
              link="/device"
            />
          </div>
        </div>
        <div
          className={styles.customerSupportContainer_rightSection_rightColumn}
        >
          <LazyImage
            image={{
              src: supportedDevices,
              alt: "supportedDevices",
            }}
            className={
              styles.customerSupportContainer_leftSection_rightColumn_image
            }
            disableSkeleton={true}
          />
        </div>
      </div>
    </div>
  );
};

export default CustomerSupport;
