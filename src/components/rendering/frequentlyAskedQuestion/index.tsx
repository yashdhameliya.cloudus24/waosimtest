import React, { useEffect, useState } from "react";
import styles from "./frequentlyAskedQuestion.module.scss";
import MainTitle from "@/components/modules/mainTitle";
import { Accordion } from "react-bootstrap";
import classNames from "classnames";
const dropDownArrow = "https://stagecdn.waosim.com/landingpage/assets/icons/accordian-down-arrow.svg";

const FrequetlyAskedQuestion = ({
  data,
  isSingleLine = false,
  activeId,
}: any) => {
  const [activeKey, setActiveKey] = useState<any>(activeId);
  const handleToggle = (key: string) => {
    setActiveKey(activeKey === key ? null : key);
  };

  const oddItems = isSingleLine
    ? data
    : data?.filter((_: any, index: number) => index % 2 === 0);
  const evenItems = data?.filter((_: any, index: number) => index % 2 !== 0);

  useEffect(() => {
    if (activeId) {
      setActiveKey(activeId);
    } else {
      setActiveKey(null);
    }
  }, [data]);

  return (
    <div
      className={classNames(
        styles.frequentlyAskedQuestion_section,
        !isSingleLine && styles.isTopPadding
      )}
    >
      {!isSingleLine && (
        <MainTitle text="Frequently <span>Asked</span> Questions" />
      )}
      <div className="container">
        <div
          className={classNames(
            styles.frequently_Asked_Question_main,
            !isSingleLine && styles.isTopPadding
          )}
        >
          {!isSingleLine && (
            <p>Find helpful information to answer your questions</p>
          )}

          <div className="row">
            <div
              className={classNames(isSingleLine ? "col-md-12" : "col-md-6")}
            >
              <div className={styles.frequently_Asked_Question_left_main}>
                <Accordion activeKey={activeKey} className={styles.accordion}>
                  {oddItems?.map((item: any, index: number) => {
                    const adjustedIndex = isSingleLine ? item?.id : index * 2;
                    return (
                      <div
                        key={adjustedIndex}
                        className={`${styles.accordion_item} ${
                          activeKey == adjustedIndex ? styles.active : ""
                        }`}
                      >
                        <div
                          className={classNames(
                            styles.accordion_header,
                            styles.isSingleLineHeight
                          )}
                          onClick={() => handleToggle(`${adjustedIndex}`)}
                        >
                          <h2>{item?.question}</h2>
                          <div
                            className={classNames(
                              styles.dropDownArrowContainer,
                              activeKey == adjustedIndex &&
                                styles.dropDownArrowActive
                            )}
                          >
                            <img src={dropDownArrow} alt="dropDownArrow" />
                          </div>
                        </div>
                        <Accordion.Collapse eventKey={`${adjustedIndex}`}>
                          <div className={styles.accordion_body}>
                            {item?.answer}
                          </div>
                        </Accordion.Collapse>
                      </div>
                    );
                  })}
                </Accordion>
              </div>
            </div>
            {isSingleLine ? null : (
              <div
                className={classNames(isSingleLine ? "col-md-12" : "col-md-6")}
              >
                {" "}
                <div className={styles.frequently_Asked_Question_left_main}>
                  <Accordion activeKey={activeKey} className={styles.accordion}>
                    {evenItems?.map((item: any, index: number) => {
                      const adjustedIndex = index * 2 + 1;
                      return (
                        <div
                          key={adjustedIndex}
                          className={`${styles.accordion_item} ${
                            activeKey == adjustedIndex ? styles.active : ""
                          }`}
                        >
                          <div
                            className={styles.accordion_header}
                            onClick={() => handleToggle(`${adjustedIndex}`)}
                          >
                            <h2>{item?.question}</h2>
                            <div
                              className={classNames(
                                styles.dropDownArrowContainer,
                                activeKey == adjustedIndex &&
                                  styles.dropDownArrowActive
                              )}
                            >
                              <img src={dropDownArrow} alt="dropDownArrow" />
                            </div>
                          </div>
                          <Accordion.Collapse eventKey={`${adjustedIndex}`}>
                            <div className={styles.accordion_body}>
                              {item?.answer}
                            </div>
                          </Accordion.Collapse>
                        </div>
                      );
                    })}
                  </Accordion>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default FrequetlyAskedQuestion;
