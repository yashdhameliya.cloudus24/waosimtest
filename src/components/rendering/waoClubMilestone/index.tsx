import React from "react";
import styles from "./waoClubMilestone.module.scss";
import Button from "@/components/modules/button";
import FullProgressBar from "@/components/modules/milstoneProgressbar";
import { useSidebar } from "@/components/contexts/sidebarContext/context";
import MilestoneSidebarDetails from "@/components/modules/milestoneSidebarDetails";
import { useUser } from "@/components/contexts/userContext/context";
const milestoneSuccess = "https://stagecdn.waosim.com/landingpage/assets/milestoneLevelSuccess_Image.png";

type LevelImageMap = {
  1: string;
  2: string;
  3: string;
  4: string;
};

const levelImageMap: LevelImageMap = {
  1: "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_Blue.svg",
  2: "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_silver.svg",
  3: "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_gold.svg",
  4: "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_daimond.svg",
};

const WaoClubMilestone = ({ token, waoClubResponse, splashResponse }: any) => {
  const { openSidebar, closeSidebar } = useSidebar();
  const { userDetails } = useUser();

  const waoclubLevel = waoClubResponse?.waoclub_leval as
    | keyof LevelImageMap
    | undefined;
  const levelImage = waoclubLevel ? levelImageMap[waoclubLevel] : "";

  const handleOnOpenSideBar = (type: any) => {
    openSidebar(
      <>
        {type === "milestoneSidebarDetails" && (
          <>
            <MilestoneSidebarDetails />
          </>
        )}
      </>
    );
  };
  console.log("splashResponsesplashResponse", splashResponse);
  return (
    <>
      <div className={styles.waoClubMilestoneSection}>
        <div className={styles.waoClubMilestoneMainSection}>
          {/* With Login */}

          {token && (
            <>
              <div className={styles.waoClubMilestoneLevelMain}>
                <div className={styles.NameWithLevel}>
                  <h1>
                    <span>{userDetails?.name} </span>,You’re at Level{" "}
                    {waoClubResponse?.waoclub_leval}!
                  </h1>
                  <p>
                    Enjoy our discounts and travel rewards - you’ve earned them.
                  </p>
                </div>
                <div className={styles.levelImage}>
                  <img src={levelImage} alt="level image" />
                </div>
              </div>
              <div className={styles.waoClubDiscountMain}>
                <div className={styles.waoClubDiscount}>
                  <div className={styles.waoClubDiscountImage}>
                    <img
                      src={
                        "https://stagecdn.waosim.com/landingpage/waoClub/waoClub_with_discount.svg"
                      }
                      alt="with Discount"
                    />
                  </div>
                  <div className={styles.waoClubDiscountText}>
                    <p>
                      You saved <span>¥{waoClubResponse?.total_saved}</span>
                    </p>
                    <p>with WaoClub discounts</p>
                  </div>
                </div>
                <div className={styles.waoClubDiscount}>
                  <div className={styles.waoClubDiscountImage}>
                    <img
                      src={
                        "https://stagecdn.waosim.com/landingpage/waoClub/waoClub_free_discount.svg"
                      }
                      alt="free Discount"
                    />
                  </div>
                  <div className={styles.waoClubDiscountText}>
                    <p>
                      You saved <span>{waoClubResponse?.saved_count}</span>
                    </p>
                    <p>free WaoClub discounts</p>
                  </div>
                </div>
              </div>
              <div className={styles.waoClubMilestoneMain}>
                <div className={styles.MilestoneImage}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/waoClub/waoClub_milestone_viewDetails.png"
                    }
                    alt="milestone Detail Image"
                  ></img>
                </div>
                <div className={styles.MilestoneText}>
                  <h1>Milestone</h1>
                  <p>Amazing , you’re half way! Check your progress</p>
                  <div className={styles.milestoneButton}>
                    <Button
                      color={"orange"}
                      type={"transparent"}
                      title={`View Details`}
                      clickHandler={() =>
                        handleOnOpenSideBar("milestoneSidebarDetails")
                      }
                    />
                  </div>
                </div>
                <div className={styles.milestoneButton}>
                  <FullProgressBar />
                </div>
              </div>
              {/* successsull milestone Complete */}
              <div className={styles.successFullMilestone}>
                <div className={styles.successFullMilestoneImage}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/waoClub/waoClub_milestone_viewDetails.png"
                    }
                    alt="milestone Detail Image"
                  ></img>
                </div>
                <div className={styles.successFullMilestoneText}>
                  <h1>Congratulations</h1>
                  <p>
                    You've reached your highest achievable milestone levels.
                  </p>
                </div>
                <div className={styles.milestoneSuccessImage}>
                  <img
                    src={milestoneSuccess}
                    alt="milestone Success Image"
                  ></img>
                </div>
              </div>
            </>
          )}

          {/* Without login */}
          <div className={styles.waoClubBetterWaoSim}>
            <div className={styles.waoClubBetterWaoSimImage}>
              <img
                src={
                  "https://stagecdn.waosim.com/landingpage/waoClub/waoClub_better_waoSim.png"
                }
                alt="waoClubBetterWaoSim image"
              />
            </div>
            <div className={styles.waoClubBetterWaoSimText}>
              <h1>
                <span>WaoClub</span> is better with WaoSim
              </h1>
              <p>
                Discover and Enjoy exclusive discounts and travel rewards on
                Data Plans worldwide with WaoClub’s loyalty program Discover and
                enjoy the benefits today!
              </p>
            </div>
            <div className={styles.waoClubBetterWaoSimVectore}>
              <img
                src={
                  "https://stagecdn.waosim.com/landingpage/waoClub/waoClub_vector.svg"
                }
                alt="waoClubBetterWaoSim image vector"
              />
            </div>
          </div>
          <div className={styles.waoClubEasyToGet}>
            <div className={styles.waoClubBlock}>
              <div className={styles.waoClubBlockImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/waoClub/waoClub_easyToGet.svg"
                  }
                  alt="WaoClub Easy Get"
                />
              </div>

              <h1>Easy to Get</h1>
              <p>
                Just create a free WAOCLUB account to unlock instant discounts.
              </p>
            </div>
            <div className={styles.waoClubBlock}>
              <div className={styles.waoClubBlockImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/waoClub/waoClub_easyToKeep.svg"
                  }
                  alt="WaoClub Easy Keep"
                />
              </div>

              <h1>Easy to Keep</h1>
              <p>
                After unlocking each WAOCLUB level, the rewards are yours to
                enjoy for life.
              </p>
            </div>
            <div className={styles.waoClubBlock}>
              <div className={styles.waoClubBlockImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/waoClub/waoClub_easyToGrow.svg"
                  }
                  alt="WaoClub Easy Grow"
                />
              </div>

              <h1>Easy to Grow</h1>
              <p>
                The more you purchase, the more you get. The best part? Every
                purchase counts toward your progress.
              </p>
            </div>
          </div>

          {!token && (
            <div className={styles.signUpWao_button}>
              <Button
                color={"solid"}
                type={"white"}
                title="Sign Up - It's free"
                link="/auth/login"
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default WaoClubMilestone;
