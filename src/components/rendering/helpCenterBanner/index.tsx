import React from "react";
import styles from "./helpCenterBanner.module.scss";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const HelpCenterBanner = () => {
  return (
    <div>
      <div className={styles.helpCenterBannerMainSection}>
        <div className={styles.helpCenterBannerSectionBottomImage}>
          <img src={"https://stagecdn.waosim.com/landingpage/assets/page_bg_bottom.png"} alt="plans Banner Imag" />
        </div>

        <div className={styles.helpCenterBannerMain}>
          <div className={styles.backgroundCloudeImage}>
            <img src={cloudLayer} alt="cloudLayer" />
          </div>
          <div className="container-fluid">
            <div className={styles.helpCenterBannerContainer}>
              <div className={styles.helpCenterBannerLeft}>
                <div className={styles.helpCenterBannerTitle}>
                  <h1>We’re Here For You.</h1>
                </div>
              </div>

              <div className={styles.helpCenterBannerRightMain}>
                <div className={styles.helpCenterBannerRight}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/commonimage/help_center_banner_image.png"
                    }
                    alt="Help Center Image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HelpCenterBanner;
