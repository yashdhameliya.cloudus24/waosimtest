import React from "react";
import styles from "./bestDealsPageDetails.module.scss";
import Link from "next/link";
const backBackground = "https://stagecdn.waosim.com/landingpage/assets/nesw_articals_bg.png";
import { slugify } from "@/utils";
import BestDealsDetailCountryCard from "@/components/modules/bestDealsDetailCountryCard";

const BestDealsPageDetails = ({ data }: any) => {
  return (
    <div className={styles.BestDealsPageDetailsSection}>
      <div className={styles.BestDealsPageDetailsContainer}>
        <div className={styles.BestDealsPageBanner}>
          <div className={styles.bestDealsBannerText}>
            <h1>Grab Great Deals From WaoSim!</h1>
            <h4>Get an extra discount on 150+ countries</h4>
          </div>
          <div className={styles.bestDealsBannerImage}>
            <img
              src={
                "https://stagecdn.waosim.com/landingpage/bestdeals/bestDeals_deatils_Banner_img.png"
              }
              alt="Banner Image"
            />
          </div>
        </div>
        <div className={styles.bestDealsDetailBox}>
          <div className={styles.bestDeals_deatils_countryGridSection}>
            {!data
              ? Array.from({ length: 18 })?.map((country: any, index: any) => (
                  <React.Fragment key={index}>
                    <BestDealsDetailCountryCard isLoading />
                  </React.Fragment>
                ))
              : data?.map((country: any, index: any) => (
                  <Link
                    href={`/plans/countries/${slugify(
                      country?.country_name
                    )}?mcc=${country?.mcc}`}
                    key={index}
                  >
                    <BestDealsDetailCountryCard
                      countryName={country?.country_name}
                      countryFlag={country?.country_flag}
                      countryImage={country?.country_image}
                    />
                    <div className={styles.planecoupan}>
                      <p>Discount</p>
                      <h1>10%</h1>
                    </div>
                  </Link>
                ))}
          </div>
        </div>
      </div>
      <div className={styles.backBackgroundImage}>
        <img src={backBackground} alt="Backgroung Image" />
      </div>
      <div className={styles.footer_color}></div>
    </div>
  );
};

export default BestDealsPageDetails;
