import React from "react";
import styles from "./contactBanner.module.scss";
import LazyImage from "@/components/modules/lazyImage";
import Link from "next/link";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const ContactBanner = () => {
  return (
    <div>
      <div className={styles.contactBannerMainSection}>
        <div className={styles.contactBannerSectionBottomImage}>
          <img src={"https://stagecdn.waosim.com/landingpage/assets/page_bg_bottom.png"} alt="plans Banner Imag" />
        </div>

        <div className={styles.contactBannerMain}>
          <div className={styles.backgroundCloudeImage}>
            <img src={cloudLayer} alt="cloudLayer" />
          </div>
          <div className="container-fluid">
            <div className={styles.contactContainer}>
              <div className={styles.contactBannerLeft}>
                <div className={styles.contactBannerTitle}>
                  <h1>Contact Us</h1>
                </div>
              </div>
              <div className={styles.contactBannerRightMain}>
                <div className={styles.contactBannerRight}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/commonimage/contactUs_Banner_Image.png"
                    }
                    alt="contact us Banner Image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactBanner;
