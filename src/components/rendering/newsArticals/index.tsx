import React from "react";
import styles from "./news_articals.module.scss";
import MainTitle from "@/components/modules/mainTitle";
import Link from "next/link";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

interface Testimonial {
  text: string;
  imageSrc: string;
  name: string;
}

const dummyData: Testimonial[] = [
  {
    text: "To take trivial example which of ever undertakes laborious physical exercise, except to obtain some advantage from but who has any right to find fault with man who chooses to enjoy.",
    imageSrc: "https://stagecdn.waosim.com/landingpage/assets/testimonial1.png",
    name: "Christine Eve",
  },
  {
    text: "Fantastic app! Purchasing an electronic SIM was so convenient. The process was smooth and hassle-free.",
    imageSrc: "https://eu.ui-avatars.com/api/?name=John+Doe&size=250",
    name: "John Doe",
  },
  {
    text: "This app saved me a lot of time and effort. I got my eSIM in minutes! Highly recommend it.",
    imageSrc:
      "https://www.gravatar.com/avatar/2c7d99fe281ecd3bcd65ab915bac6dd5?s=250",
    name: "Jane Smith",
  },
  {
    text: "Great service! The app is easy to use and the customer support is responsive.",
    imageSrc: "https://avatar.iran.liara.run/public/boy?username=Ash",
    name: "Alex Johnson",
  },
  {
    text: "I had some issues initially, but the support team was quick to assist me. Overall, good experience.",
    imageSrc:
      "https://api.dicebear.com/7.x/adventurer-neutral/svg?seed=mail@ashallendesign.co.uk",
    name: "Emily Brown",
  },
];

const NewsArticals = () => {
  const settings = {
    dots: false,
    infinite: true,
    speed: 800,
    autoplaySpeed: 5000,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div className={styles.news_artical_section}>
      <div className="container">
        <div className={styles.review_section_main}>
          <div className="row align-items-center">
            <div className="col-md-5">
              <div className={styles.review_image}>
                <div className={styles.review_image_image}>
                  <img
                    src="https://stagecdn.waosim.com/landingpage/assets/review_section_img.png"
                    alt="news_articals_image"
                  />
                </div>
              </div>
            </div>
            <div className="col-md-7">
              <div className={styles.testimonial_review_main}>
                <div className="slider-container">
                  <Slider {...settings}>
                    {dummyData.map((testimonial: any, index: any) => (
                      <div key={index} className={styles.slider_main_div}>
                        <div className={styles.slider_text}>
                          <h2>{testimonial.text}</h2>
                        </div>
                        <div className={styles.slider_image_name}>
                          <div className={styles.slider_image}>
                            <img
                              src={testimonial.imageSrc}
                              alt={`testimonial-${index}`}
                            />
                          </div>
                          <div className={styles.slider_name}>
                            <h2>{testimonial.name}</h2>
                          </div>
                        </div>
                      </div>
                    ))}
                  </Slider>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.news_artical_main}>
          <MainTitle text="Latest <span>News</span> & <span>Articles</span>" />
          <div className={styles.news_artical_card_main_section}>
            <div className="row">
              <div className="col-md-6">
                <div className={styles.news_artical_card_main}>
                  <div className={styles.news_text}>
                    <h1>Things to see and do when visiting Japan</h1>
                    <p>
                      There are many variations of but the majority have simply
                      free text.
                    </p>
                    <Link href="/">Read more</Link>
                  </div>
                  <div className={styles.news_image}>
                    <img
                      src="https://stagecdn.waosim.com/landingpage/assets/news_artical_img.png"
                      alt="news_articals_image"
                      className={styles.news_image_image}
                    />
                    <div className={styles.image_date}>
                      <h1>19</h1>
                      <p>Dec</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className={styles.news_artical_card_main}>
                  <div className={styles.news_text}>
                    <h1>Things to see and do when visiting Japan</h1>
                    <p>
                      There are many variations of but the majority have simply
                      free text.
                    </p>
                    <Link href="/">Read more</Link>
                  </div>
                  <div className={styles.news_image}>
                    <img
                      src="https://stagecdn.waosim.com/landingpage/assets/news_artical_img.png"
                      alt="news_articals_image"
                      className={styles.news_image_image}
                    />
                    <div className={styles.image_date}>
                      <h1>19</h1>
                      <p>Dec</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsArticals;
