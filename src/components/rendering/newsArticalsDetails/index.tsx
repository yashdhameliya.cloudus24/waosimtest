import React from "react";
import styles from "./newsArticalsDetails.module.scss";
import LazyImage from "@/components/modules/lazyImage";
import Link from "next/link";
const backGround = "https://stagecdn.waosim.com/landingpage/assets/nesw_articals_bg.png";
const BgImage = "https://stagecdn.waosim.com/landingpage/assets/afghanistan.png";

const NewsArticalsDetails = () => {
  return (
    <>
      <div className={styles.newsArticalsDetailsSection}>
        <div className={styles.newsArticalsDetailMain}>
          <div className={styles.newsArticalsDetailCard}>
            <div className={styles.newsArticalsDetailCardBackground}>
              <LazyImage
                image={{
                  src: BgImage,
                  alt: "News Details Cover Image",
                }}
                className={styles.newsArticalsDetailCardBackgroundImage}
              />
            </div>
            <div className={styles.planListDetailsBox}>
              <h1>Things to see and do when visiting Japan</h1>
              <div className={styles.newsSocialDetail}>
                <div className={styles.newsDate}>
                  <h3>News </h3>
                  <p>Posted on 27th January 2022</p>
                </div>
                <div className={styles.social_link}>
                  <ul>
                    <li>
                      <Link href="/">
                        <img
                          src="https://stagecdn.waosim.com/landingpage/icons/faceBookIcon.png"
                          alt="facebook"
                        />
                      </Link>
                    </li>
                    <li>
                      <Link href="/">
                        <img
                          src="https://stagecdn.waosim.com/landingpage/icons/instagramIcon.png"
                          alt="instagram"
                        />
                      </Link>
                    </li>
                    <li>
                      <Link href="/">
                        <img
                          src="https://stagecdn.waosim.com/landingpage/icons/twiterIcon.png"
                          alt="twitter"
                        />
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className={styles.newsArticalsDetailContent}>
                <p>
                  Lorem ipsum dolor sit amet consectetur. Nulla turpis eros
                  tortor quam eu. Nibh dictumst eu massa eget. Pellentesque eget
                  blandit ullamcorper rhoncus laoreet ipsum. Ac et egestas
                  aliquet cursus ultricies. Turpis lorem urna scelerisque
                  praesent in molestie bibendum. Semper sit nec nulla
                  ullamcorper pharetra nisl neque sagittis. Sed feugiat
                  consectetur odio eros vehicula gravida phasellus sem ut.
                </p>
                <br />
                <p>
                  Lorem ipsum dolor sit amet consectetur. Nulla turpis eros
                  tortor quam eu. Nibh dictumst eu massa eget. Pellentesque eget
                  blandit ullamcorper rhoncus laoreet ipsum. Ac et egestas
                  aliquet cursus ultricies. Turpis lorem urna scelerisque
                  praesent in molestie bibendum. Semper sit nec nulla
                  ullamcorper pharetra nisl neque sagittis. Sed feugiat
                  consectetur odio eros vehicula gravida phasellus sem ut.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className={styles.newsArticalsDetailsBackground}>
          <img src={backGround} alt="herobanner" />
        </div>
      </div>
      <div className={styles.footer_color}></div>
    </>
  );
};

export default NewsArticalsDetails;
