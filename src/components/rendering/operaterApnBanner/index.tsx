import React from "react";
import styles from "./operaterApnBanner.module.scss";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const OperaterApnBanner = () => {
  return (
    <div>
      <div className={styles.operaterApnBannerMainSection}>
        <div className={styles.operaterApnHeroBannerSectionBottomImage}>
          <img src={"https://stagecdn.waosim.com/landingpage/assets/page_bg_bottom.png"} alt="plans Banner Imag" />
        </div>

        <div className={styles.operaterApnHeroBannerMain}>
          <div className={styles.backgroundCloudeImage}>
            <img src={cloudLayer} alt="cloudLayer" />
          </div>
          <div className="container-fluid">
            <div className={styles.operaterApnHeroBannerContainer}>
              <div className={styles.operaterApnHeroBannerLeft}>
                <div className={styles.operaterApnHeroBannerTitle}>
                  <h1>Operator / APN</h1>
                </div>
              </div>

              <div className={styles.operaterApnHeroBannerRightMain}>
                <div className={styles.operaterApnHeroBannerRight}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/commonimage/operaterApnBanner_Img.png"
                    }
                    alt="operater Apn Banner Image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OperaterApnBanner;
