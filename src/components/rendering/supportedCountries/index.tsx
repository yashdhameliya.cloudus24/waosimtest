import React, { useContext, useEffect, useState } from "react";
import styles from "./supportedCountries.module.scss";
import MainTitle from "@/components/modules/mainTitle";
import Button from "@/components/modules/button";
import LazyImage from "@/components/modules/lazyImage";
import CustomAccordion from "@/components/modules/accordion";
import Link from "next/link";
import { slugify } from "@/utils";
import { GlobalContext } from "@/components/contexts/globalContext/context";
const esim_background = "https://stagecdn.waosim.com/landingpage/assets/esim_background.png";
const esim_icon = "https://stagecdn.waosim.com/landingpage/assets/esim_icon.png";

const SupportedCountries = () => {
  const [selectedOption, setSelectedOption] = useState("countries");
  const [loading, setLoading] = useState(true);
  const { popularData } = useContext(GlobalContext);

  useEffect(() => {
    if (popularData) {
      setLoading(false);
    }
  }, [popularData]);

  const popularCountries = popularData?.countries?.filter(
    (country: any) => country?.is_popular === 1
  );
  const popularRegions = popularData?.regions?.filter(
    (region: any) => region?.is_popular === 1
  );

  const handleOptionChange = (option: any) => {
    setSelectedOption(option);
  };

  console.log("popularDatapopularData",popularData)

  return (
    // <LazyLoad id={"supportedCountries"}>
    <div className={styles.supportedCountriesSection}>
      <div className={styles.supportedCountrie_images}>
        <div className="container">
          <div className={styles.supportedCountriesMain}>
            <MainTitle text="Supported <span>Countries</span>" />

            <div className={styles.sectionDetails}>
              <p>Where are you traveling next?</p>
              <p>
                Choose your destination first, then a data plan according to
                your needs.
              </p>
            </div>

            <div className={styles.toggleSection}>
              <div
                className={`${styles.option} ${
                  selectedOption === "countries" ? styles.selected : ""
                }`}
                onClick={() => handleOptionChange("countries")}
              >
                <span> Explore by countries</span>
              </div>
              <div
                className={`${styles.option} ${
                  selectedOption === "regions" ? styles.selected : ""
                }`}
                onClick={() => handleOptionChange("regions")}
              >
                <span> Explore by regions</span>
              </div>
            </div>

            {selectedOption === "countries" ? (
              <div className={styles.gridSection}>
                <div className="row">
                  {loading
                    ? Array.from({ length: 16 })?.map(
                        (item: any, index: any) => {
                          return (
                            <div
                              key={`countryList-${index}`}
                              className={`col-lg-3 col-md-4 col-sm-6 col-6 mb-4 odd ${
                                index % 2 !== 0 ? styles.odd : styles.even
                              }`}
                            >
                              <div
                                className={styles.gridSection_card}
                                key={`countryList-${index}`}
                              >
                                <div className={styles.gridSection_card_subDiv}>
                                  <LazyImage
                                    image={{
                                      src: item?.country_image,
                                      alt: item?.country_name,
                                    }}
                                    className={styles.gridSection_card_image}
                                  />
                                </div>
                              </div>
                            </div>
                          );
                        }
                      )
                    : popularCountries?.map((item: any, index: any) => {
                        return (
                          <div
                            key={`countryList-${index}`} // Move key to the outermost element
                            className={`col-lg-3 col-md-4 col-sm-6 col-6 mb-4 odd ${
                              index % 2 !== 0 ? styles.odd : styles.even
                            }`}
                          >
                            <Link
                              href={`/plans/countries/${slugify(
                                item?.country_name
                              )}?mcc=${item?.mcc}`}
                              key={index}
                            >
                              <div
                                className={styles.gridSection_card}
                                key={`countryList-${index}`}
                              >
                                <div className={styles.gridSection_card_subDiv}>
                                  <LazyImage
                                    image={{
                                      src: item?.country_image,
                                      alt: item?.country_name,
                                    }}
                                    className={styles.gridSection_card_image}
                                    disableSkeleton
                                  />
                                  <div
                                    className={styles.gridSection_background}
                                  >
                                    <div
                                      className={
                                        styles.gridSection_background_image
                                      }
                                    >
                                      <img
                                        src={esim_background}
                                        alt="esim_background"
                                      />
                                    </div>
                                  </div>
                                  <div className={styles.gridSection_icon}>
                                    <div
                                      className={
                                        styles.gridSection_background_image
                                      }
                                    >
                                      <img src={esim_icon} alt="esim_icon" />
                                    </div>
                                  </div>
                                  <div
                                    className={styles.gridSection_countryName}
                                  >
                                    {item?.country_name && (
                                      <span>{item?.country_name}</span>
                                    )}
                                  </div>

                                  <div
                                    className={styles.gridSection_country_flag}
                                  >
                                    <div className={styles.card_round_item}>
                                      <LazyImage
                                        image={{
                                          src: item?.country_flag,
                                          alt: "afg_flag",
                                        }}
                                        className={
                                          styles.card_round_item_skeleton
                                        }
                                        disableSkeleton
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </Link>
                          </div>
                        );
                      })}
                </div>
              </div>
            ) : (
              <CustomAccordion data={popularRegions} isPopular={true} />
            )}

            <div className={styles.show_country_button}>
              <Button
                color={"solid"}
                type={"white"}
                title={
                  selectedOption === "regions"
                    ? `Show all regions`
                    : `Show all countries`
                }
                link={`plans?activetab=${
                  selectedOption === "regions" ? `regions` : `countries`
                }   `}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
    // </LazyLoad>
  );
};

export default SupportedCountries;
