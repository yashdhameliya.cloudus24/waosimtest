import React from "react";
import styles from "./unlimited_Esim.module.scss";
import LazyLoad from "@/components/modules/lazyLoad";
import Link from "next/link";
import classNames from "classnames";
import Button from "@/components/modules/button";

const UnlimitedEsim = () => {
  return (
    <div className={styles.unlimited_esim_section}>
      <div className="container">
        <div className={styles.unlimited_esim_Main}>
          <div className="row align-items-center">
            <div className="col-md-6">
              <div className={styles.unlimited_esim_Left}>
                <div className={styles.unlimited_esim_Title}>
                  <h4>
                    Got unlimited No. of <span>eSIM</span>
                  </h4>
                  <p>
                    WaoSim is the first application offer unlimited free
                    of charge eSIM
                  </p>
                </div>

                <div className={styles.check_how_it_work_Button}>
                  <Button
                    color={"orange"}
                    type={"transparent"}
                    title={`Check how it works`}
                  />
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className={styles.unlimited_esim_Right}>
                <img src={"https://stagecdn.waosim.com/landingpage/home/unlimited_no_esim.png"} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UnlimitedEsim;
