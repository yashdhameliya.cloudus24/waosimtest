import React from "react";
import styles from "./esimConnectivity.module.scss";
import LazyImage from "@/components/modules/lazyImage";
import Link from "next/link";
import LazyLoad from "@/components/modules/lazyLoad";
import GooglePlayAppStore from "@/components/modules/googlePlayAppStore";
import classNames from "classnames";

const EsimConnectivity = ({BottomSpace = false}:any) => {
  return (
    // <LazyLoad id={"esimConnectivity"}>
    <div className={classNames(styles.esim_connectivity_section ,!BottomSpace && styles.BottomSpace)}>
      <div className="container">
        <div className={styles.esim_connectivity_main}>
          <div className="row align-items-center">
            <div className="col-md-7">
              <div className={styles.esim_connectivity_left}>
                <h1>
                  Unlock Seamless Connectivity <br />
                  Ready to Experience eSIM Magic?
                </h1>
                <p>
                  WaoSim Wonderland Awaits!
                  <br />
                  Download our app to buy and manage your eSIM wherever, <br />
                  whenever!
                </p>
                <div className={styles.socialIconsAlligment}>
                  <GooglePlayAppStore />
                </div>
              </div>
            </div>
            <div className="col-md-5">
              <div className={styles.esim_connectivity_right}>
                <div className={styles.esim_connectivity_right_image}>
                  <img
                    src="https://stagecdn.waosim.com/landingpage/home/esimConnectivity.png"
                    alt="esim_connectivity_img"
                    className={styles.esim_connectivity_img}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    // </LazyLoad>
  );
};

export default EsimConnectivity;
