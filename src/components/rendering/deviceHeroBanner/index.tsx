import React from "react";
import styles from "./deviceHeroBanner.module.scss";
import LazyImage from "@/components/modules/lazyImage";
import Link from "next/link";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const DeviceHeroBanner = () => {
  return (
    <div>
      <div className={styles.deviceHeroBannerMainSection}>
        <div className={styles.deviceHeroBannerSectionBottomImage}>
          <img src={"https://stagecdn.waosim.com/landingpage/assets/page_bg_bottom.png"} alt="plans Banner Imag" />
        </div>

        <div className={styles.deviceHeroBannerMain}>
          <div className={styles.backgroundCloudeImage}>
            <img src={cloudLayer} alt="cloudLayer" />
          </div>
          <div className="container-fluid">
            {/* <div className="row align-items-center">
              <div className="col-md-6">
                <div className={styles.deviceHeroBannerLeft}>
                  <div className={styles.deviceHeroBannerTitle}>
                    <h1>Device Compatibility</h1>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className={styles.deviceHeroBannerRight}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/commonimage/device_banner.png"
                    }
                    alt="plans Banner Imag"
                  />
                </div>
              </div>
            </div> */}
            <div className={styles.deviceHeroContainer}>
              <div className={styles.deviceHeroBannerLeft}>
                <div className={styles.deviceHeroBannerTitle}>
                  <h1>Device Compatibility</h1>
                </div>
              </div>
              <div className={styles.deviceHeroBannerRightMain}>
                <div className={styles.deviceHeroBannerRight}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/commonimage/device_banner.png"
                    }
                    alt="Device Banner Image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeviceHeroBanner;
