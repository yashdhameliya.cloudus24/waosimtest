import React from 'react'
import styles from "./waoClubBanner.module.scss";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";


const WaoClubBanner = () => {
  return (
   <>
      <div className={styles.waoClubBannerMainSection}>
        <div className={styles.waoClubBannerSectionBottomImage}>
          <img src={"https://stagecdn.waosim.com/landingpage/assets/waoclub_bottom_image.png"} alt="plans Banner Imag" />
        </div>

        <div className={styles.waoClubBannerMain}>
          <div className={styles.backgroundCloudeImage}>
            <img src={cloudLayer} alt="cloudLayer" />
          </div>
          <div className="container-fluid">
            <div className={styles.waoClubContainer}>
              <div className={styles.waoClubBannerLeft}>
                <div className={styles.waoClubBannerTitle}>
                  <h3>Get Rewarded for Being You</h3>
                  <h1>WaoClub</h1>
                  <h3>WaoSIM's Exclusive Loyalty Program!</h3>
                </div>
              </div>
              <div className={styles.waoClubBannerRightMain}>
                <div className={styles.waoClubBannerRight}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/home/getInstantDiscount.png"
                    }
                    alt="wao club banner image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div className={styles.waoClubDetailsMainSection}>
          hiii
      </div> */}
   </>
  )
}

export default WaoClubBanner