import React from "react";
import styles from "./deviceCompatibility.module.scss";
const supportedVectore = "https://stagecdn.waosim.com/landingpage/assets/icons/device-compatibility-vectore.svg";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const DeviceCompatibility = () => {
  return (
    <div className={styles.deviceCompatibilitySection}>
      <div className={styles.deviceCompatibilityMain}>
        <div className={styles.deviceSupportedEsimMain}>
          <div className={styles.deviceSupportedVectore}>
            <img src={cloudLayer} alt="cloude layer" />
          </div>
          <div className={styles.deviceSupportedLeft}>
            <div className={styles.deviceSupportedleftVector}>
              <img src={supportedVectore} alt="Device Supported Vector" />
            </div>
            <h1>
              Does my phone Supported <span>eSim?</span>
            </h1>
            <p>
              More than 200+ Devices support eSIM, Look up if your device is
              compatible.
            </p>
            <p>
              To use an eSIM, your device must be carrier-unlocked and
              eSIM-compatible. Please reference the list below to see if your
              device supports eSIM technology. (Note that country and
              carrier-specific restrictions may apply
            </p>
          </div>
          <div className={styles.deviceSupportedright}>
            <div className={styles.deviceSupportedRightImage}>
              <img
                src={"https://stagecdn.waosim.com/landingpage/commonimage/device_image.png"}
                alt="Device Supported Image"
              />
            </div>
          </div>
        </div>
        <div className={styles.deviceSupportedTextMain}>
          <div className={styles.deviceSupportedBlock1}>
            <h3>
              Apple <span>*</span>
            </h3>
            <div className={styles.deviceName}>
              <div className={styles.deviceBlock}>
                <ul>
                  <li>iPhone 14</li>
                  <li>iPhone 14 Plus</li>
                  <li>iPhone 14 Pro</li>
                  <li>iPhone 14 Pro Max</li>
                  <li>iPhone 13</li>
                  <li>iPhone 13 Pro</li>
                  <li>iPhone 13 Pro Max</li>
                  <li>iPhone 13 Mini</li>
                  <li>iPhone 12</li>
                  <li>iPhone 12 Mini</li>
                  <li>iPhone 12 Pro</li>
                  <li>iPhone 12 Pro Max</li>
                  <li>iPhone 11</li>
                </ul>
              </div>
              <div className={styles.deviceBlock}>
                <ul>
                  <li>iPhone 11 Pro</li>
                  <li>iPhone 11 Pro Max</li>
                  <li>iPhone XS</li>
                  <li>iPhone XS Max</li>
                  <li>iPhone XR</li>
                  <li>iPhone SE (2020)</li>
                  <li>iPhone SE (2022)</li>
                  <li>iPad Air (3rd generation)</li>
                  <li>iPad Air (4th generation)</li>
                  <li>iPad Pro 11‑inch (1st generation)</li>
                  <li>iPad Pro 11‑inch (2nd generation)</li>
                  <li>iPad Pro 11-inch (3rd generation)</li>
                </ul>
              </div>
              <div className={styles.deviceBlock}>
                <ul>
                  <li>iPad Pro 12.9‑inch (3rd generation)</li>
                  <li>iPad Pro 12.9‑inch (4th generation)</li>
                  <li>iPad Pro 12.9-inch (5th generation)</li>
                  <li>iPad (7th generation)</li>
                  <li>iPad (8th generation)</li>
                  <li>iPad (9th generation)</li>
                  <li>iPad Mini (5th generation)</li>
                  <li>iPad Mini (6th generation)</li>
                </ul>
              </div>
            </div>
            <h4>
              <span>* </span>The following Apple devices DO NOT have eSIM
              capability:
            </h4>
            <div className={styles.deviceInnerMenu}>
              <ul>
                <li>iPhone from mainland China.</li>
                <li>
                  iPhone devices from Hong Kong and Macao (except for iPhone 13
                  mini, iPhone 12 mini, iPhone SE 2020, and iPhone XS).
                </li>
              </ul>
            </div>
            <h4>
              <span>* </span>Only iPad devices with Wi-Fi + Cellular features
              are supported.
            </h4>
          </div>
          <div className={styles.deviceSupportedBlock1}>
            <h3>
              Samsung <span>*</span>
            </h3>
            <div className={styles.deviceName}>
              <div className={styles.deviceBlock}>
                <ul>
                  <li>Samsung Galaxy S22 Ultra 5G</li>
                  <li>Samsung Galaxy S22+ 5G</li>
                  <li>Samsung Galaxy S22 5G</li>
                  <li>Samsung Galaxy S21 Ultra 5G</li>
                  <li>Samsung Galaxy S21 5G</li>
                  <li>Samsung Galaxy S21+ 5Go</li>
                </ul>
              </div>
              <div className={styles.deviceBlock}>
                <ul>
                  <li>Samsung Galaxy S20</li>
                  <li>Samsung Galaxy S20+</li>
                  <li>Samsung Galaxy Z Flip</li>
                  <li>Samsung Galaxy Z Flip3 5G</li>
                  <li>Samsung Galaxy Z Fold3</li>
                  <li>Samsung Galaxy Z Fold2</li>
                </ul>
              </div>
              <div className={styles.deviceBlock}>
                <ul>
                  <li>Samsung Galaxy Fold</li>
                  <li>Samsung Galaxy Note 20 Ultra</li>
                  <li>Samsung Galaxy Note 20+</li>
                  <li>Samsung Galaxy Note 20)</li>
                </ul>
              </div>
            </div>
            <h4>
              <span>* </span>The following Samsung devices DO NOT have eSIM
              capability:
            </h4>
            <div className={styles.deviceInnerMenu}>
              <ul>
                <li>All Samsung S20 FE/S21 FE models</li>
                <li>US versions of Samsung S20/S21 and Galaxy Z Flip 5G</li>
                <li>
                  USA and Hong Kong versions of the Samsung Note 20 Ultra and
                  Samsung Galaxy Z Fold 2
                </li>
                <li>
                  Most Samsung devices purchased in South Korea do not support
                  eSIMs. Only Samsung Galaxy Z Flip4 and Samsung Galaxy Z Fold4
                  purchased in South Korea are eSIM compatible.
                </li>
              </ul>
            </div>
            <h4>
              Samsung S21 series devices (with the exemption of FE models) from
              Canada and the USA may have eSIM capability as long as the One UI
              4 update is installed. Please get in touch with your carrier or
              device manufacturer to confirm that your Samsung device is eSIM
              capable.
            </h4>
          </div>
          <div className={styles.deviceSupportedBlock1}>
            <h3>Google Pixel</h3>
            <div className={styles.deviceName}>
              <div className={styles.deviceBlock}>
                <ul>
                  <li>
                    Google Pixel 3 & 3 XL<span>*</span>
                  </li>
                  <li>
                    Google Pixel 3a & 3a XL<span>*</span>
                  </li>
                  <li>Google Pixel 4, 4a & 4 XL</li>
                  <li>Google Pixel 5</li>
                  <li>Google Pixel 6</li>
                  <li>Google Pixel 6 Pro</li>
                </ul>
              </div>
            </div>
            <h4>
              <span>* </span>The following Google devices DO NOT have eSIM
              capability:
            </h4>
            <div className={styles.deviceInnerMenu}>
              <ul>
                <li>
                  Pixel 3 models originating from: Australia, Taiwan, and Japan;
                  and those bought with service from the US or Canadian carriers
                  other than Sprint and Google Fi.
                </li>
                <li>
                  Pixel 3a bought in South East Asia and with Verizon service.
                </li>
              </ul>
            </div>
          </div>
          <div className={styles.deviceSupportedBlock2}>
            <div className={styles.deviceBlock2Main1}>
              <h3>HAMMER</h3>
              <ul>
                <li>HAMMER Explorer PRO</li>
                <li>HAMMER Blade 3</li>
                <li>HAMMER Blade 5G</li>
                <li>myPhone NOW eSIM</li>
              </ul>
            </div>
            <div className={styles.deviceBlock2Main1}>
              <h3>Motorola</h3>
              <ul>
                <li>Motorola Razr 2019</li>
                <li>Motorola Razr 5G</li>
              </ul>
            </div>
            <div className={styles.deviceBlock2Main1}>
              <h3>Huawei</h3>
              <ul>
                <li>Huawei P40</li>
                <li>
                  Huawei P40 Pro<span>*</span>
                </li>
                <li>Huawei Mate 40 Pro</li>
              </ul>
              <h4>
                <span>*</span> Huawei P40 Pro+ is not compatible with eSIMs.
              </h4>
            </div>
          </div>
          <div className={styles.deviceSupportedBlock1}>
            <h3>OPPO</h3>
            <div className={styles.deviceName}>
              <div className={styles.deviceBlock}>
                <ul>
                  <li>
                    OPPO Find X3 Pro
                    <span>*</span>
                  </li>
                  <li>
                    OPPO Find X5 Pro
                    <span>*</span>
                  </li>
                  <li>
                    OPPO Find X5 <span>*</span>
                  </li>
                </ul>
              </div>
            </div>
            <h4>The following OPPO devices DO NOT have eSIM capability:</h4>
            <div className={styles.deviceInnerMenu}>
              <ul>
                <li>
                  eSIM support depends on carriers and coverage options.
                  Coverage may vary. Known regions that do not support dual SIM
                  cards: Australia (Telstra and Optus), and Japan (KDDI).
                </li>
                <li>
                  Functions available on the live network depend on the
                  carrier’s network and related services deployment.
                </li>
              </ul>
            </div>
          </div>
          <div className={styles.deviceSupportedBlock3}>
            <div className={styles.deviceBlock2Main1}>
              <h3>SONY</h3>
              <ul>
                <li>Sony Xperia 10 III Lite</li>
                <li>Sony Xperia 10 IV</li>
                <li>Sony Xperia 1 IV</li>
              </ul>
            </div>
            <div className={styles.deviceBlock2Main1}>
              <ul>
                <li>Nuu Mobile X5</li>
                <li>Gemini PDA 4G+Wi-Fi</li>
              </ul>
            </div>
          </div>
          <h1>
            Windows 10<span>*</span>/ Windows 11
          </h1>
          <div className={styles.deviceSupportedBlock4}>
            <div className={styles.deviceBlock4}>
              <div className={styles.deviceBlock2Main1}>
                <h3>ACER</h3>
                <ul>
                  <li>ACER Swift 3</li>
                  <li>ACER Swift 7</li>
                </ul>
              </div>
              <div className={styles.deviceBlock2Main1}>
                <h3>HP</h3>
                <ul>
                  <li>HP Elitebook G5</li>
                  <li>HP Probook G5</li>
                  <li>HP Zbook G5</li>
                  <li>HP Spectre Folio 13</li>
                </ul>
              </div>
              <div className={styles.deviceBlock2Main1}>
                <h3>ASUS</h3>
                <ul>
                  <li>ASUS Mini Transformer T103HAF</li>
                  <li>ASUS NovaGo TP370QL</li>
                  <li>ASUS Vivobook Flip 14 TP401NA</li>
                </ul>
              </div>
            </div>
            <div className={styles.deviceBlock4}>
              <div className={styles.deviceBlock2Main1}>
                <h3>Dell</h3>
                <ul>
                  <li>Dell Latitude 9510</li>
                  <li>Dell Latitude 7410</li>
                  <li>Dell Latitude 7310</li>
                  <li>Dell Latitude 9410</li>
                  <li>Dell Latitude 7210 2-in-1</li>
                </ul>
              </div>
              <div className={styles.deviceBlock2Main1}>
                <h3>LENOVO</h3>
                <ul>
                  <li>Lenovo Yoga C630</li>
                  <li>Lenovo Miix 630</li>
                  <li>Lenovo Yoga 520</li>
                  <li>Lenovo Yoga 720 convertible laptops</li>
                </ul>
              </div>
              <div className={styles.deviceBlock2Main1}>
                <h3>
                  SURFACE<span>*</span>
                </h3>
                <ul>
                  <li>Surface Go 3</li>
                  <li>Surface Pro X</li>
                  <li>Surface Duo 2</li>
                  <li>Surface Duo</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeviceCompatibility;
