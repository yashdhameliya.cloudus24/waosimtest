import React from "react";
import styles from "./newsArticalsBanner.module.scss";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const NewsArticalesBanner = () => {
  return (
    <div>
      <div className={styles.newsarticalsBannerMainSection}>
        <div className={styles.newsarticalsHeroBannerSectionBottomImage}>
          <img src={"https://stagecdn.waosim.com/landingpage/assets/page_bg_bottom.png"} alt="plans Banner Imag" />
        </div>

        <div className={styles.newsarticalsHeroBannerMain}>
          <div className={styles.backgroundCloudeImage}>
            <img src={cloudLayer} alt="cloudLayer" />
          </div>
          <div className="container-fluid">
            <div className={styles.newsarticalsContainer}>
              <div className={styles.newsarticalsHeroBannerLeft}>
                <div className={styles.newsarticalsHeroBannerTitle}>
                  <h1>News & Articles</h1>
                </div>
              </div>
              <div className={styles.newsarticalsHeroBannerRightMain}>
                <div className={styles.newsarticalsHeroBannerRight}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/commonimage/newArtical_Banner_image.png"
                    }
                    alt="News & Articles Image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsArticalesBanner;
