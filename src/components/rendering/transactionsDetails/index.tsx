import React from "react";
import styles from "./transactionsDetails.module.scss";
import Link from "next/link";

const TransactionsDetails = () => {
  return (
    <>
      <div className={styles.transactionsDetailsSection}>
        <div className={styles.transactionsDetailsContainer}>
          <div className={styles.transactionsDetailsTitle}>
            <h2>Display based on the Specified Commercial Transactions Law</h2>
          </div>
          <div className={styles.transactionsDetailsContent}>
            <h3>1. Business name, address, and telephone number</h3>
            <div className={styles.addressDetail}>
              <p>
                <label>Sales company name</label>
                <span>: Zaina</span>
              </p>
              <p>
                <label>location </label>
                <span>: 3-17-11 Shibaura, Minato-ku, Tokyo</span>
              </p>
              <p>
                <label>telephone number</label>
                <span>: 03-4574-5520</span>
              </p>
              <p>
                <label>email address </label>
                <span>
                  <Link href="">: info@zaina.com</Link>
                </span>
              </p>
              <p>
                <label>Sales manager name </label>
                <span>: Hama Mostafa</span>
              </p>
            </div>
            <h3>2. Sales price</h3>
            <p>
              The price of each product plan is listed on the purchase page
              within the app.
            </p>
            <h3>3. Charges other than the sales price</h3>
            <p>
              The customer is responsible for the internet connection
              environment, communication fees, etc. required to use the app.
            </p>
            <h3>4. Payment method and timing of product price</h3>
            <p>Based on the payment method specified by your in-app store.</p>
            <h4>5. Extradition period</h4>
            <p>
              You can use it immediately after completing the payment procedure.
            </p>
            <h4>6. Matters regarding returns, cancellation, etc.</h4>
            <p>
              Due to the nature of digital content, we are unable to accept
              returns or exchanges after purchase is confirmed.However, we will
              accept returns based on Article 13, Paragraph 2 of the "WaoSim
              Terms of Use" only for reasons attributable to our company .
            </p>
            <h3>7. Operating environment/compatible models</h3>
            <p>
              Please check the official website of the app and thepage of the
              downloaded app for the terminals (smartphones, tablets, etc.) and
              operating environment on which the target service operates.
            </p>
            <h3>8. Contact information</h3>
            <p className={styles.link}><Link href={"#"}>Click here for inquiries</Link></p>
          </div>
        </div>
      </div>
    </>
  );
};

export default TransactionsDetails;
