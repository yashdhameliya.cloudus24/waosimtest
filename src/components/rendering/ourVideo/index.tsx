import React from "react";
import styles from "./ourVideo.module.scss";
import VideoPlayer from "@/components/modules/videoPlayer";
import MainTitle from "@/components/modules/mainTitle";

const OurVideo = () => {
  return (
    <div className={styles.ourVideoMain_section}>
      <div className={styles.ourVideoMain}>
        <MainTitle text="Company <span>Overview</span>" />

        <div className="container">
          <div className={styles.videoplayer_main}>
            <VideoPlayer
              // url={`${process.env.NEXT_PUBLIC_S3BUCKET_URL}/landingpage/home/video.mp4`}
              url={`https://stagecdn.waosim.com/landingpage/home/video.mp4`}
              videoAutoPlay={true}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurVideo;
