import React from "react";
import styles from "./termsCondition.module.scss";
import MainTitle from "@/components/modules/mainTitle";

const TermsConditions = () => {
  return (
    <div className={styles.termsConditionSection}>
      <div className={styles.termsConditionTitle}>
        <MainTitle text="Terms and <span>Condition</span>" />
      </div>
      <div className={styles.termsConditionContainer}>
        <div className={styles.termsConditionData}>
          <p>
            By using the services provided by Zaina relating to 'WaoSim,' the
            customer (referred to as 'the User') agrees to be bound by these
            Terms of Use (referred to as 'these Terms'). These Terms establish
            the conditions for the use of all services (referred to as 'the
            Service') provided by the Company.
          </p>

          <h4>Article 1 (Definitions)</h4>
          <p>
            The definitions of the terms used this Agreement are as follows:
          </p>

          <h6>(1) 'This Service' </h6>
          <p>The service and related services operated by our company.</p>

          <h6>(2) 'This Site'</h6>
          <p>The website on which the contents of this Service are posted.</p>

          <h6>(3) 'This Content'</h6>
          <p>
            The collective term for the text, sound, still images, moving
            images, software programs, codes, and other materials provided on
            this Service (including posted information).
          </p>

          <h6>(4) 'User'</h6>
          <p>All individuals who use this Service.</p>

          <h6>(5) 'Registered User'</h6>
          <p>An individual who has completed registration on this Site.</p>

          <h6>(6) 'ID'</h6>
          <p>
            A unique string of characters held by a registered user for the
            purpose of using this Service.
          </p>

          <h6>(7) 'Password'</h6>
          <p>A code set by a registered user corresponding to their ID.</p>

          <h6>(8) 'Personal Information'</h6>
          <p>
            The collective term for information that can identify an individual,
            such as their address, name, occupation, and telephone number.
          </p>

          <h6>(9) 'Registered Information'</h6>
          <p>
            The collective term for information that a registered user has
            registered on this Site (excluding posted information).
          </p>

          <h6>(10) 'Intellectual Property'</h6>
          <p>
            Anything created by human creativity, such as inventions, plant
            varieties, designs, copyrighted works, and other things that can be
            utilized for industrial purposes, as well as trademarks, trade
            names, and other things used to represent goods or services for
            business activities, and technical or business information useful
            for business activities, including trade secrets and other
            proprietary information.
          </p>

          <h6>(11) 'Intellectual Property Rights'</h6>
          <p>
            Rights defined by law or legally protected interests related to
            patents, utility model rights, breeder's rights, design rights,
            copyright, trademark rights, and other intellectual property.
          </p>

          <h4>Article 2 (Agreement to this Agreement)</h4>
          <p>
            1. The user may use this service after agreeing to this agreement.
          </p>
          <p>
            2. Once the user downloads the service onto their smartphone or
            other information terminal and completes the agreement process, a
            usage contract in accordance with the terms of the agreement is
            considered established between them and the company.
          </p>
          <p>
            3. If the user is under the age of 18, it is required to obtain
            consent from their legal representative or guardian before utilizing
            this service.
          </p>
          <p>
            4. If a minor user utilizes this service without the consent of
            their legal representative, falsely represents that they have
            obtained such consent, misrepresents their legal age, or employs
            fraudulent means to mislead others into believing that they have
            legal capacity, they will be unable to rescind any legal actions
            associated with this service.
          </p>
          <p>
            5. If a user, who was a minor at the time of agreeing to this
            agreement, continues to use this service after reaching legal age,
            they are considered to have ratified all legal actions related to
            this service.
          </p>

          <h4>Article 3 (Modification of the Agreement)</h4>
          <p>
            1. We reserve the right to modify these terms of use without user
            consent, and the user is required to accept any revisions without
            objection.
          </p>
          <p>
            2. We will notify the user of any changes to these terms of use
            through the communication method specified by us.
          </p>
          <p>
            3. The revised terms of use will become effective once we have
            informed the user in the preceding paragraph.
          </p>
          <p>
            4. By using the service after the revision of these terms of use,
            the user shall be considered to have accepted the updated terms
            without objection.
          </p>

          <h4>Article 4 (Management of Accounts)</h4>
          <p>
            1. Users shall voluntarily register and manage their information
            (hereinafter referred to as 'registered information,' including
            email address, ID, password, etc.) at their own responsibility when
            using the service.{" "}
          </p>
          <p>
            Users shall not allow third parties to use, loan, transfer, change
            the name, sell, or otherwise dispose of the registered information.
          </p>

          <p>
            2. If the service is used according to the registered information,
            the user who registered the information will be held responsible for
            any resulting consequences and associated responsibilities
          </p>

          <p>
            3. If a user causes damages to the company or a third party due to
            the unauthorized use of the registered information, the user shall
            compensate for the damages.
          </p>

          <p>
            4. The user shall manage the registered information at their own
            responsibility, and the company shall not be responsible for any
            disadvantages or damages incurred by the user due to inaccurate or
            false registered information.
          </p>

          <p>
            5. If the registered information is found to have been stolen or
            used by a third party, the user shall immediately notify the company
            and follow the company's instructions.
          </p>

          <h4>Article 5 (Handling of Personal Information, etc.)</h4>
          <p>
            Regarding personal information and user information, we will handle
            them appropriately in accordance with the 'WaoSim Privacy Policy'
            separately established by our company.
          </p>

          <h4>Article 6 (Prohibited Acts)</h4>
          <p>
            In using this service, the Company prohibits the following actions
            by users (hereinafter referred to as 'users'). In the event that the
            Company determines that a user has violated any of these prohibited
            actions, the Company may take measures it deems necessary, such as
            suspending use ,and the user agrees to this without objection.
          </p>
          <p>
            (1) Infringement of the intellectual property rights of the Company
            or third parties.
          </p>
          <p>
            (2) Acts that slander or unjustly discriminate against the honor or
            reputation of the Company or third parties, or defame them.
          </p>
          <p>
            (3) Acts that infringe on the property of the Company or third
            parties, or that may infringe on them.
          </p>
          <p>
            (4) Acts that cause economic damage to the Company or third parties.
          </p>
          <p>(5) Threatening acts against the Company or third parties.</p>
          <p>
            (6) Acts of using or inducing the use of computer viruses or harmful
            programs.
          </p>
          <p>
            (7) Acts that put excessive stress on the infrastructure facilities
            of this service.
          </p>
          <p>(8) Attacks on the servers, systems, or security of this site.</p>
          <p>
            (9) Attempts to access the Company's service by any means other than
            the interfaces provided by the Company.
          </p>
          <p>
            (10) Use of terminals that do not comply with technical standards
            specified by related laws and regulations.
          </p>
          <p>(11) Use of terminals that this service does not support.</p>
          <p>
            (12) Acts prohibited by laws, regulations, etc. in the country where
            the subscriber uses this service (e.g., data communication and
            permanent roaming to other countries are prohibited).
          </p>
          <p>
            (13) Acts of using this service for purposes that the Company deems
            inappropriate, such as sending spam or SMS messages, for viewing or
            obtaining child prostitution or child pornography.
          </p>
          <p>
            (14) Replicating all or part of the service provision information,
            including user qualifications for this system, for use by third
            parties.
          </p>
          <p>(15) Causing a third party to engage in any of the above acts.</p>
          <p>
            (16) In addition to the above, acts that the Company deems
            inappropriate.
          </p>

          <h4>Article 7 (Handling of Content)</h4>
          <p>
            {" "}
            (1) Users may only use the content of this service within the range
            specified by the Company.
          </p>
          <p>
            {" "}
            (2) The Company owns all rights related to the content provided
            through this service, does not grant users the right to exercise or
            license the Company's patent rights, utility model rights, design
            rights, trademark rights, copyright, or any other intellectual
            property rights.
          </p>
          <p>
            {" "}
            (3) Users are prohibited from reproducing, transmitting,
            transferring (including buying and selling among users), lending,
            translating, creating derivative works, reprinting without
            permission, using for commercial purposes, modifying,
            reverse-assembling,reverse-compiling, reverse-engineering or
            engaging in any other unauthorized use beyond the scope determined
            by the Company.
          </p>
          <p>
            {" "}
            (4) Notwithstanding the previous paragraph, if a user loses their
            membership status due to withdrawal or other reasons, their right to
            use the provided content will also cease.
          </p>

          <h4>Article 8 (Compensation, Modification, and Payment Delay)</h4>
          <p>
            1. This service can be used by paying a fee on service stores such
            as App Store and Google Play. The amount of the fee, payment method,
            and other matters will be separately determined and displayed by our
            company on this service or our provided services.
          </p>

          <p>
            2. Our company may change the price of the contents that are charged
            in this service at our discretion.
          </p>
          <p>
            3. If the user delays payment of the fee for this service, the user
            shall pay our company a delay penalty of 14.6% per annum.
          </p>
          <p>
            4. This service is licensed for use only with the registered
            information of the user.
          </p>

          <h4>Article 9 (Disclaimer)</h4>
          <p>
            1. Our company shall not be liable for any damages arising from the
            modification, interruption, or termination of this service.
          </p>
          <p>
            2. Our company shall not be involved in the user's usage environment
            of this service and shall not be liable for any responsibility for
            it.
          </p>
          <p>
            3. Our company does not guarantee that this service is suitable for
            the user's specific purpose, has the expected functions, product
            value, accuracy, usefulness, or that the user's use of this service
            complies with applicable laws or internal rules of industry
            associations, and that no malfunctions occur.
          </p>
          <p>
            4. Users shall agree in advance that the use of this service may be
            partially or completely restricted due to changes in the usage rules
            and operating policies of service stores such as App Store and
            Google Play.
          </p>
          <p>
            5. Our company shall not be liable for any direct or indirect
            damages incurred by users as a result of using this service.
          </p>
          <p>
            6. Our company shall not be liable for any loss of opportunity,
            interruption of business, or other damages (including indirect
            damages and loss of profits) incurred by users or any third party,
            even if our company has been notified of the possibility of such
            damages in advance.
          </p>
          <p>
            7. The provisions of the first paragraph to the preceding paragraph
            do not apply if our company has intentional or gross negligence or
            if the contract is deemed to be a consumer contract under the
            Consumer Contract Act.
          </p>
          <p>
            8. Even if the preceding article is applicable, we shall not be
            responsible for compensating any damages arising from special
            circumstances among the damages incurred by the user due to acts of
            negligence (excluding gross negligence).
          </p>
          <p>
            9. If we are liable for damages due to the use of this service, we
            will be responsible for compensation up to the amount of the usage
            fee received from the user in the month in which the damage
            occurred.{" "}
          </p>
          <p>
            10. We shall not be responsible for any disputes or troubles between
            users. If users have a dispute with each other, they shall resolve
            it at their own responsibility, and shall not make any claims
            against us.
          </p>
          <p>
            11. If a user causes damage to another user or has a dispute with a
            third party in connection with the use of this service, the user
            shall be responsible for compensating for such damage or resolving
            the dispute at their own expense and responsibility and shall not
            cause any inconvenience or damage to us.
          </p>
          <p>
            12. If we are claimed for damages or other compensation by a third
            party due to a user's actions, the user shall resolve it at their
            own expense and responsibility (including legal fees). If we pay
            damages to the third party, the user shall pay us all expenses
            including the compensation for damages (including legal fees and
            lost profits).
          </p>
          <p>
            13. If a user causes damage to us in connection with the use of this
            service, the user shall compensate us for the damage (including
            litigation costs and legal fees) at their own expense and
            responsibility.
          </p>
          <p>
            14. If the user causes damage to the Company in connection with the
            use of this service, the user shall compensate the Company for the
            damage (including legal fees and attorney fees) at the user's
            expense and responsibility.
          </p>

          <h4>Article 10 (Advertisement Posting)</h4>
          <p>
            Users acknowledge and accept that various advertisements may be
            included on the Service, and that the Company or its partners may
            post various advertisements. The form and scope of advertisements on
            the Service may be changed by the Company at any time.
          </p>

          <h4>
            Article 11 (Restriction, Suspension, Interruption, and Termination
            of Use)
          </h4>
          <p>
            1. In case of a natural disaster, emergency, or any other event that
            may occur or has occurred, the Company may limit the use of this
            service in orderto prioritize communication necessary for disaster
            prevention or relief, transportation, communication, power supply,
            or maintaining public order and other public interests.
          </p>
          <p>
            2. The Company may control the speed and amount of communication by
            detecting the use of communication procedures or applications that
            continuously and heavily occupy the bandwidth assigned to the
            communication.
          </p>
          <p>
            3. The Company may suspend or restrict the use of this service by
            the subscriber in the following cases:
          </p>
          <div className="inner-div-space">
            <p>
              (1) When the subscriber delays the performance of their
              obligations under this agreement, such as payment of fees.
            </p>
            <p>
              (2) When the subscriber provides false information to the Company.
            </p>
            <p>
              (3) When the Company confirms that the subscriber violated the
              provisions of Article 6 (Prohibited Conduct).{" "}
            </p>
            <p>
              {" "}
              (4) When the subscriber is unable to use the designated credit
              card.
            </p>
          </div>
          <p>
            4. The Company may take the following measures regarding the
            communication of this service:
          </p>
          <div className="inner-div-space">
            <p>
              (1) The Company may disconnect the communication if it recognizes
              that the subscriber's line has continuously remained in a state of
              being able to conduct data communication (hereinafter referred to
              as 'session') for a long time.
            </p>
            <p>
              (2) The Company may disconnect the communication if it recognizes
              that there has been a large amount of communication within the
              same session.
            </p>
          </div>
          <p>
            5. The Company may suspend the provision of this service in the
            following cases:
          </p>
          <div className="inner-div-space">
            <p>
              (1) When it is necessary for the maintenance or construction of
              the Company's communication facilities or systems.
            </p>
            <p>
              (2) When the communication carrier suspends the provision of
              communication services to the Company.
            </p>
            <p>
              (3) When the cloud provider suspends the provision of cloud
              services to the Company.
            </p>
            <p>
              (4) When it becomes necessary to suspend the service due to
              regulations, etc.
            </p>
          </div>

          <p>
            6. When the Company suspends the use of this service under the
            provisions of the preceding paragraph, it shall announce this in
            advance on the Company's application or website, etc.However, in
            cases of emergencies, this may not be the case.
          </p>
          <p>
            7. The Company may abolish all or part of this service if it is
            necessary due to technical reasons, business operations, or
            regulations, etc.
          </p>

          <h4>Article 12 (Termination of Service)</h4>
          <p>
            1. The company may terminate this service by notifying the users
            through an appropriate method.
          </p>
          <p>
            2. If this service is terminated, the user agrees in advance to lose
            all rights to use the paid content and will no longer be able to use
            such content.
          </p>
          <p>
            3. The company shall not be liable for any damages incurred by users
            or third parties due to the termination of this service, regardless
            of the cause.
          </p>

          <h4>Article 13 (Refunds, Cancellations, and Changes)</h4>
          <p>
            1. Users have the right to request a refund or change if they are
            unable to install or use the eSIM due to technical problems arising
            from this service.
          </p>
          <div className="inner-div-space">
            <p>
              (1) In order to identify whether the problem is due to technical
              issues arising from this service, the user should follow the
              customer support's guidance for requesting information or images
              when a problem occurs.
            </p>
            <p>
              (2) If the information provided by customer support is not
              sufficient, no refund will be given.
            </p>
          </div>
          <p>
            2. If activation is still impossible after customer support
            guidance, the user may request a refund within 30 days of purchase.
          </p>
          <p>3. No refunds will be accepted for expired usage.</p>
          <p>
            4. No refunds will be given for fees incurred with alternative
            phones or SIM cards, hotel phones, or fees not directly related to
            the user's eSIM account. (Please refer to Article 9 (Disclaimer) for
            details.)
          </p>
          <p>
            5. If there is evidence of misuse, violation of the terms of use, or
            fraudulent activities related to the use of the product or service,
            the company reserves the right to refuse a refund.
          </p>
          <p>
            6. Confirming whether the user's device is 'eSIM compatible' or
            'unlocked' is the responsibility of the user. Refunds and
            cancellations after purchase will not be accepted in the following
            cases:
          </p>
          <div className="inner-div-space">
            <p>
              (1) When the user's device is found to be incompatible with eSIM.
            </p>
            <p>
              (2) When the user's device is found to be unable to unlock the
              SIM.
            </p>
            <p>(3) When the service is not used.</p>
            <p>
              (4) When the service is not used due to the effects of infectious
              diseases such as COVID-19.
            </p>
            <p></p>
          </div>

          <p>
            7. Device compatibility varies by carrier, country of manufacture,
            and manufacturer, and the list of eSIM compatible devices does not
            cover all models.
          </p>

          <h4> 14 (Prohibition of Assignment of Rights)</h4>
          <p>
            1. Unless otherwise approved in writing by our company beforehand,
            the user shall not assign to any third party the position, rights,
            or obligations under this agreement, in whole or in part, based on
            this agreement.
          </p>
          <p>
            2. Our company may, at its discretion, assign all or part of this
            service to a third party. In that case, all the user's rights
            related to the use of this service, including the user's account,
            within the scope of the assigned rights shall be transferred to the
            assignee.
          </p>

          <h4>Article 15 (Separability)</h4>
          <p>
            Even if any provision or part of this agreement is judged to be
            invalid or unenforceable under the Consumer Contract Act or any
            other laws and regulations, the remaining provisions of this
            agreement and the remaining parts of the provision judged to be
            invalid or unenforceable shall continue to have full effect.
          </p>

          <h4>Article 16 (Contacting Us)</h4>
          <p>
            For any inquiries or communication related to the use of this
            service, users should use the contact form installed in appropriate
            locations on this service or on our website, or use any other method
            specified by us.
          </p>
          <h4>Article 17 (Applicable Law and Jurisdiction)</h4>
          <p>
            1. The validity, interpretation, and performance of these terms and
            conditions shall be governed by Japanese law and interpreted in
            accordance with Japanese law.
          </p>
          <p>
            2. All disputes, discussions, or legal actions between us and the
            user shall be under the exclusive jurisdiction of the Tokyo Summary
            Court or the Tokyo District Court, depending on the amount of the
            claim.
          </p>

          <h6>Contact information</h6>
          <p>
            If you have any questions regarding these terms and conditions,
            please contact us here:
          </p>
          <br />
          <p>
            Mail Address：{" "}
            <span>
              <a href="mailto:support@waosim.com" target="_blank">
                support@waosim.com
              </a>
            </span>
          </p>
          <p>Phone Number：03-4574-5520</p>
          <p>Address：10F, Tensho Tamachi Bldg,</p>
          <p>3-17-11 Shibaura,</p>
          <p>Minato-ku, Tokyo</p>
          <br />
          <p>
            Before we respond to your inquiry, please understand that we may ask
            for proof of your identity. Thank you for your understanding in
            advance.
          </p>
        </div>
      </div>
    </div>
  );
};

export default TermsConditions;
