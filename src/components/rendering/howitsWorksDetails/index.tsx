import React from "react";
import styles from "./howitsWorksDetails.module.scss";
import GooglePlayAppStore from "@/components/modules/googlePlayAppStore";
import classNames from "classnames";
const vectore_imag1 = "https://stagecdn.waosim.com/landingpage/assets/icons/howItsWork_vectore_image1.svg";
const vectore_imag2 = "https://stagecdn.waosim.com/landingpage/assets/icons/howItsWork_vectore_image2.svg";
const vectore_step1_imag1 = "https://stagecdn.waosim.com/landingpage/assets/icons/howItsWork_vectore_image1_step1.svg";
const vectore_step2_imag2 = "https://stagecdn.waosim.com/landingpage/assets/icons/howItsWork_vectore_image1_step2.svg";
const step_middal_Image = "https://stagecdn.waosim.com/landingpage/assets/icons/howWorkStep_middal_left_vectore.svg";
const step_middal_right_Image =
  "https://stagecdn.waosim.com/landingpage/assets/icons/howWorkStep_middal_right_vectore.svg";


const HowitsWorksDetails = () => {
  return (
    <div className={styles.howItsWorksDetailSection}>
      <div className={styles.howItsWorksDetailContainer}>
        <div className={styles.howItsWorkAllSteps}>
          {/* step 1 */}
          <div className={styles.howItsWorkSteps1}>
            <div className={styles.stepImageDiv}>
              <img src={vectore_imag1} alt="step Image" />
              <div className={styles.stepMobileImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/commonimage/howItsWorks_s1.png"
                  }
                  alt="step1 Image"
                />
              </div>
            </div>
            <div className={styles.steptexteDiv}>
              <img src={vectore_imag2} alt="step Image" />
              <div className={styles.stepInnerText}>
                <h1>Download the app</h1>
                <GooglePlayAppStore buttonBorder = {true}/>
              </div>
              <div className={styles.stepNumberText}>
                <h4>Step 1</h4>
              </div>
            </div>
          </div>
          <div className={styles.howItsWorkMiddleImage}>
            <img src={step_middal_Image} alt="step Image left" />
          </div>
          {/* step 2 */}
          <div className={styles.howItsWorkSteps2}>
            <div className={styles.steptexteDiv}>
              <img src={vectore_step1_imag1} alt="step Image" />
              <div className={styles.stepInnerText}>
                <h1>Create Account</h1>
                <h3>
                  Welcome to Waosim, your gateway to global connectivity! 🚀
                </h3>
              </div>
              <div className={styles.stepNumberText}>
                <h4>Step 2</h4>
              </div>
            </div>
            <div className={styles.stepImageDiv}>
              <img src={vectore_step2_imag2} alt="step Image" />
              <div className={styles.stepMobileImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/commonimage/howItsWorks_s2.png"
                  }
                  alt="step2 Image"
                />
              </div>
            </div>
          </div>
          <div className={styles.howItsWorkMiddlerightImage}>
            <img src={step_middal_right_Image} alt="step Image right" />
          </div>
          {/* step 3 */}
          <div className={styles.howItsWorkSteps1}>
            <div className={styles.stepImageDiv}>
              <img src={vectore_imag1} alt="step Image" />
              <div className={styles.stepMobileImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/commonimage/howItsWorks_s3.png"
                  }
                  alt="step3 Image"
                />
              </div>
            </div>
            <div className={styles.steptexteDiv}>
              <img src={vectore_imag2} alt="step Image" />
              <div className={styles.stepInnerText}>
                <h1>Search Plan</h1>
                <h3>
                  Explore plans from 150+ countries and unlock a world of
                  connectivity! 🌍
                </h3>
              </div>
              <div className={styles.stepNumberText}>
                <h4>Step 3</h4>
              </div>
            </div>
          </div>
          <div className={styles.howItsWorkMiddleImage}>
            <img src={step_middal_Image} alt="step Image left" />
          </div>
          {/* step 4 */}
          <div className={styles.howItsWorkSteps2}>
            <div className={styles.steptexteDiv}>
              <img src={vectore_step1_imag1} alt="step Image" />
              <div
                className={classNames(
                  styles.stepInnerText,
                  styles.stepInnerTextWidth
                )}
              >
                <h1>Purchase Plan</h1>
                <h3>
                  Find your perfect fit with our amazing packages designed to
                  cater to your every need! 🎁
                </h3>
              </div>
              <div className={styles.stepNumberText}>
                <h4>Step 4</h4>
              </div>
            </div>
            <div className={styles.stepImageDiv}>
              <img src={vectore_step2_imag2} alt="step Image" />
              <div className={styles.stepMobileImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/commonimage/howItsWorks_s4.png"
                  }
                  alt="step4 Image"
                />
              </div>
            </div>
          </div>
          <div className={styles.howItsWorkMiddlerightImage}>
            <img src={step_middal_right_Image} alt="step Image right" />
          </div>
          {/* step 5 */}
          <div className={styles.howItsWorkSteps1}>
            <div className={styles.stepImageDiv}>
              <img src={vectore_imag1} alt="step Image" />
              <div className={styles.stepMobileImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/commonimage/howItsWorks_s5.png"
                  }
                  alt="step5 Image"
                />
              </div>
            </div>
            <div className={styles.steptexteDiv}>
              <img src={vectore_imag2} alt="step Image" />
              <div
                className={classNames(
                  styles.stepInnerText,
                  styles.stepInners5Text
                )}
              >
                <h1>Install eSIM</h1>
                <h3>
                  Quick and simple Install and manage your eSIM with ease! 📲
                </h3>
              </div>
              <div className={styles.stepNumberText}>
                <h4>Step 5</h4>
              </div>
            </div>
          </div>
          <div className={styles.howItsWorkMiddleImage}>
            <img src={step_middal_Image} alt="step Image left" />
          </div>
          {/* step 6 */}
          <div className={styles.howItsWorkSteps2}>
            <div className={styles.steptexteDiv}>
              <img src={vectore_step1_imag1} alt="step Image" />
              <div
                className={classNames(
                  styles.stepInnerText,
                  styles.stepInners6Text
                )}
              >
                <h1>Active Plan</h1>
                <h3>
                  Spark the Connectivity Activate, Monitor, and Ride the
                  Internet Wave! 🌟
                </h3>
              </div>
              <div className={styles.stepNumberText}>
                <h4>Step 6</h4>
              </div>
            </div>
            <div className={styles.stepImageDiv}>
              <img src={vectore_step2_imag2} alt="step Image" />
              <div className={styles.stepMobileImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/commonimage/howItsWorks_s6.png"
                  }
                  alt="step6 Image"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HowitsWorksDetails;
