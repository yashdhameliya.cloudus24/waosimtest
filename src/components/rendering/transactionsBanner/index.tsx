import React from "react";
import styles from "./transactionsBanner.module.scss";
import Link from "next/link";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const TransactionsBanner = () => {
  return (
    <div>
      <div className={styles.transactionsBannerMainSection}>
        <div className={styles.transactionsBannerSectionBottomImage}>
          <img src={"https://stagecdn.waosim.com/landingpage/assets/page_bg_bottom.png"} alt="plans Banner Imag" />
        </div>

        <div className={styles.transactionsBannerMain}>
          <div className={styles.backgroundCloudeImage}>
            <img src={cloudLayer} alt="cloudLayer" />
          </div>
          <div className="container-fluid">
            <div className={styles.transactionsContainer}>
              <div className={styles.transactionsBannerLeft}>
                <div className={styles.transactionsBannerTitle}>
                  <h1>Transactions</h1>
                </div>
              </div>
              <div className={styles.transactionsBannerRightMain}>
                <div className={styles.transactionsBannerRight}>
                  <img
                    src={
                      "https://stagecdn.waosim.com/landingpage/commonimage/transactions_Banner_Image.png"
                    }
                    alt="transaction Banner Image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TransactionsBanner;
