import React from "react";
import LazyLoad from "@/components/modules/lazyLoad";
import DesktopHeader from "@/components/modules/desktopHeader";
import { useRouter } from "next/router";
import SideBarDrawer from "@/components/modules/sideBarDrawer";

const Header = ({ headerRef, isTransparentBackground }: any) => {
  const router = useRouter();

  return (
    <>
      {/* {isDesktop ? */}
      <DesktopHeader
        isTransparentBackground={isTransparentBackground}
        headerRef={headerRef}
      />
      <SideBarDrawer />
    </>
  );
};

export default Header;
