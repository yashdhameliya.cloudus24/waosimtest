import React from "react";
import styles from "./operaterApnDetail.module.scss";
import Table from "react-bootstrap/Table";
import { CopyIcon } from "@/assets/images/icons";
import { toastError, toastSuccess } from "@/utils";

const OperaterApnDetail = ({ data }: any) => {
  const handleOnCopy = (textToCopy: any) => {
    if (textToCopy) {
      navigator.clipboard
        .writeText(textToCopy)
        .then(() => {
          toastSuccess("copied successfully!");
        })
        .catch((err) => {
          toastError("Failed to copy!");
        });
    }
  };

  return (
    <div>
      <div className={styles.operaterApnDetailSection}>
        <div className={styles.operaterApnDetailContainer}>
          <div className={styles.operaterTable}>
            <Table responsive="md" className={styles.apnTable}>
              <thead>
                <tr>
                  <th>
                    <h1>Country</h1>
                  </th>
                  <th>
                    <h1>Operator</h1>
                  </th>
                  <th>
                    <h1>APN</h1>
                  </th>
                </tr>
              </thead>
              <tbody>
                {data?.map((item: any, index: number) => {
                  return (
                    <tr key={index}>
                      <td>
                        <div className={styles.countryName} key={index}>
                          <p>{item?.country_name}</p>
                        </div>
                      </td>
                      <td>
                        <div className={styles.operaterName}>
                          <p>{item?.operators}</p>
                        </div>
                      </td>
                      <td>
                        <div className={styles.ApnName}>
                          <p>{item?.apns}</p>
                          <div
                            className={styles.apnCopyImage}
                            onClick={() => handleOnCopy(item?.apns)}
                          >
                            <CopyIcon fill="#000" />
                          </div>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OperaterApnDetail;
