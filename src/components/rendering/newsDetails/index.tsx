import React from "react";
import styles from "./newsDetails.module.scss";
import Link from "next/link";
import Pagination from "react-bootstrap/Pagination";
import classNames from "classnames";
import { Dropdown } from "react-bootstrap";
const first_arrow = "https://stagecdn.waosim.com/landingpage/assets/icons/pagination_first_arrow.svg";
const last_arrow = "https://stagecdn.waosim.com/landingpage/assets/icons/pagination_last_arrow.svg";
const prev_arrow = "https://stagecdn.waosim.com/landingpage/assets/icons/pagination_prev_arrow.svg";
const next_arrow = "https://stagecdn.waosim.com/landingpage/assets/icons/pagination_next_arrow.svg";

const NewsDetails = () => {
  return (
    <div>
      <div className={styles.newsDetailsSection}>
        <div className={styles.newsDetailsContainer}>
          <div className={styles.newsArticalDetailGridSection}>
            {[0, 1, 2, 3, 4, 5, 6].map((items, index) => {
              return (
                <div className={styles.newsArticalCardmain} key={index}>
                  <div className={styles.newsArticalsText}>
                    <h1>Things to see and do when visiting Japan</h1>
                    <p>
                      There are many variations of but the majority have simply
                      free text.
                    </p>
                    <Link href={`/news/${index}`}>Read more</Link>
                  </div>
                  <div className={styles.newsArticalsImage}>
                    <img
                      src="https://stagecdn.waosim.com/landingpage/assets/news_artical_img.png"
                      alt="news_articals_image"
                      className={styles.newsArticalsInnerImage}
                    />
                    <div className={styles.newsImageDate}>
                      <h1>19</h1>
                      <p>Dec</p>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
          <div className={styles.newsDetailsPaginationMain}>
            <nav className={styles.Pager1} aria-label="pagination example">
              <ul className={styles.pagination}>
                {/* <!--First--> */}
                <li className={styles.pageItem}>
                  <img src={first_arrow} alt="first arrow" />
                </li>

                {/* <!--Arrow left--> */}
                <li className={styles.pageItem}>
                  <a className={styles.pageLink} aria-label="Previous">
                    <img src={prev_arrow} alt="prev arrow" />
                    {/* <span className={styles.srOnly}>Previous</span> */}
                  </a>
                </li>

                {/* <!--Numbers--> */}
                <li className={classNames(styles.pageItem, styles.active)}>
                  <a className={styles.pageLink}>1</a>
                </li>
                <li className={styles.pageItem}>
                  <a className={styles.pageLink}>2</a>
                </li>
                <li className={styles.pageItem}>
                  <a className={styles.pageLink}>3</a>
                </li>
                <li className={classNames(styles.pageItem, styles.pageDots)}>
                  <a className={styles.pageLink}>...</a>
                </li>
                <li className={styles.pageItem}>
                  <a className={styles.pageLink}>10</a>
                </li>

                {/* <!--Arrow right--> */}
                <li className={styles.pageItem}>
                  <a className={styles.pageLink} aria-label="Next">
                    <img src={next_arrow} alt="next arrow" />
                    {/* <span className={styles.srOnly}>Next</span> */}
                  </a>
                </li>

                {/* <!--Last--> */}
                <li className={styles.pageItem}>
                  <img src={last_arrow} alt="last arrow" />
                </li>
              </ul>
            </nav>
            <div className={styles.newsDetailsDropdownPagination}>
              <h4>Page</h4>
              <Dropdown className={styles.dropdown}>
                <Dropdown.Toggle
                  id="dropdown-basic"
                  className={styles.dropdownToggle}
                >
                  <h4>1</h4>
                </Dropdown.Toggle>
                <Dropdown.Menu className={styles.dropdownMenu}>
                  <Dropdown.Item
                    href="#/action-1"
                    className={styles.dropdownItem}
                  >
                    <h4>2</h4>
                  </Dropdown.Item>
                  <Dropdown.Item
                    href="#/action-2"
                    className={styles.dropdownItem}
                  >
                    <h4>3</h4>
                  </Dropdown.Item>
                  <Dropdown.Item
                    href="#/action-2"
                    className={styles.dropdownItem}
                  >
                    <h4>4</h4>
                  </Dropdown.Item>
                  <Dropdown.Item
                    href="#/action-2"
                    className={styles.dropdownItem}
                  >
                    <h4>5</h4>
                  </Dropdown.Item>
                  <Dropdown.Item
                    href="#/action-2"
                    className={styles.dropdownItem}
                  >
                    <h4>6</h4>
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
              <div>
                <h4>of 10</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsDetails;
