import React, { useContext, useEffect, useState } from "react";
import { CloseIcon, SearchIcon } from "@/assets/images/icons";
import styles from "./plansHeroBanner.module.scss";
import LazyImage from "@/components/modules/lazyImage";
import { useRouter } from "next/router";
import { slugify } from "@/utils";
import Link from "next/link";
import useDebounce from "@/utils/useHooks/useDebounce";
import { getAllCountryRegion } from "@/utils/api/countryRegion";
import Skeleton from "react-loading-skeleton";
import classNames from "classnames";
const cloudLayer = "https://stagecdn.waosim.com/landingpage/assets/cloudLayer.png";

const PlansHeroBanner = () => {
  const router = useRouter();
  const selectedOption = router?.query?.activetab || "countries";
  const [showDropdown, setShowDropdown] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [filteredOptions, setFilteredOptions] = useState<any>([]);
  const debouncedSearch = useDebounce(searchQuery, 300);
  const [loading, setLoading] = useState(true);

  console.log("showDropdownshowDropdown", showDropdown);

  const handleInputChange = (e: any) => {
    let searchTerm = e.target.value;
    if (searchTerm.length === 1 && searchTerm[0] === " ") {
      return;
    }
    setLoading(true);
    setShowDropdown(true);
    setSearchQuery(e.target.value);
  };

  const handleSearch = async (term: string) => {
    setLoading(true);
    try {
      const { isError, response } = await getAllCountryRegion({
        is_popular: false,
        search: term,
      });

      const filteredCountries =
        response?.data?.countries?.map((country: any) => ({
          highlighttitle:
            term.length > 0
              ? country?.country_name.replace(
                  new RegExp(term, "ig"),
                  (match: any) => `<span class="queryHighlight">${match}</span>`
                )
              : country?.country_name,
          title: country?.country_name,
          imgUrl: country?.country_plan_image,
          planListLink: `/plans/countries/${slugify(
            country?.country_name
          )}?mcc=${country?.mcc}`,
        })) || [];

      const filteredPlans =
        response?.data?.regions?.map((plan: any) => ({
          highlighttitle:
            term.length > 0
              ? plan?.name.replace(
                  new RegExp(term, "ig"),
                  (match: any) => `<span class="queryHighlight">${match}</span>`
                )
              : plan?.name,
          title: plan?.name,
          imgUrl: plan?.plan_image,
          planListLink: `/plans/regions/${slugify(plan?.name)}?id=${plan?.id}`,
        })) || [];

      let finalArray = [...filteredCountries, ...filteredPlans];

      console.log("finalArrayfinalArray", finalArray);
      setFilteredOptions(finalArray);
      setShowDropdown(true);
      setLoading(false);
    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (debouncedSearch?.length > 0) {
      handleSearch(debouncedSearch);
    } else {
      setShowDropdown(false);
      setLoading(false);
    }
  }, [debouncedSearch]);

  const handleDropdownToggle = () => {
    setShowDropdown(!showDropdown);
    if (searchQuery?.length < 1) {
      setFilteredOptions([]);
    }
  };

  const handleOptionChange = (option: any) => {
    router.push(
      {
        pathname: router.pathname,
        query: {
          ...router.query,
          activetab: option,
        },
      },
      undefined,
      { shallow: true }
    );
  };

  return (
    <div className={styles.plansHeroBannerMainSection}>
      <div className={styles.plansHeroBannerSectionBottomImage}>
        <img src={"https://stagecdn.waosim.com/landingpage/assets/page_bg_bottom.png"} alt="plans Banner Imag" />
      </div>

      <div
        className={
          selectedOption === "countries"
            ? `${styles.plansHeroBannerMain} ${styles.countriesHeroBannerMain}`
            : `${styles.plansHeroBannerMain} ${styles.regionsHeroBannerMain}`
        }
      >
        <div className={styles.backgroundCloudeImage}>
          <img src={cloudLayer} alt="cloudLayer" />
        </div>
        <div className="container-fluid">
          <div className="row align-items-center">
            <div className="col-md-7 ">
              <div className={styles.plansHeroBannerLeftSide}>
                <div className={styles.plansHeroBannerLeft}>
                  <div className={styles.plansHeroBannerTitle}>
                    <h1>Coverage</h1>
                  </div>
                  <div className={styles.searchinput}>
                    <input
                      type="search"
                      className={"form-control"}
                      placeholder="Data plans for over 150+ countries and regions"
                      aria-label="Recipient's username"
                      aria-describedby="basic-addon2"
                      value={searchQuery}
                      onFocus={handleDropdownToggle}
                      onBlur={() => {
                        setTimeout(() => setShowDropdown(false), 200);
                      }}
                      onChange={handleInputChange}
                    />

                    <div className={styles.inputButtons}>
                      {!searchQuery ? (
                        <div className={styles.searchIcon}>
                          <SearchIcon fill="white" />
                        </div>
                      ) : (
                        <div
                          className={styles.closeIcon}
                          onClick={() => {
                            setShowDropdown(!showDropdown);
                            setSearchQuery("");
                            setFilteredOptions([]);
                          }}
                        >
                          <CloseIcon fill="#A9A9A9" />
                        </div>
                      )}
                    </div>

                    {showDropdown && loading && (
                      <ul className={styles.dropdown}>
                        <div className={styles.dropdownmain}>
                          {[0, 1, 2, 3, 4, 5, 6]?.map(
                            (option: any, index: any) => (
                              <li key={index} className={styles.dstextSkeleton}>
                                <div
                                  className={styles.textSkeleton}
                                  key={index}
                                >
                                  <Skeleton
                                    className={classNames(
                                      styles.dropdown_image,
                                      styles.textSkeleton
                                    )}
                                  />
                                </div>
                              </li>
                            )
                          )}
                        </div>
                      </ul>
                    )}

                    {!loading &&
                      showDropdown &&
                      filteredOptions?.length > 0 && (
                        <ul className={styles.dropdown}>
                          <div className={styles.dropdownmain}>
                            {filteredOptions?.map((option: any, index: any) => (
                              <Link href={option?.planListLink} key={index}>
                                <li>
                                  <div className={styles.countryImage}>
                                    <LazyImage
                                      image={{
                                        src: option?.imgUrl,
                                        alt: option?.title,
                                      }}
                                      className={styles.dropdown_image}
                                    />

                                    <div
                                      className={styles.gridSection_countryName}
                                    >
                                      {option?.title}
                                    </div>
                                  </div>
                                  <p
                                    dangerouslySetInnerHTML={{
                                      __html: option?.highlighttitle,
                                    }}
                                  />
                                </li>
                              </Link>
                            ))}
                          </div>
                        </ul>
                      )}
                  </div>
                  <div className={styles.toggleSection}>
                    <div
                      className={`${styles.option} ${
                        selectedOption === "countries" ? styles.selected : ""
                      }`}
                      onClick={() => handleOptionChange("countries")}
                    >
                      <span> Explore by countries</span>
                    </div>
                    <div
                      className={`${styles.option} ${
                        selectedOption === "regions" ? styles.selected : ""
                      }`}
                      onClick={() => handleOptionChange("regions")}
                    >
                      <span> Explore by regions</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-5">
              <div
                className={
                  selectedOption === "countries"
                    ? `${styles.plansHeroBannerRight} ${styles.countriesClassName}`
                    : `${styles.plansHeroBannerRight} ${styles.regionsBannerRight}`
                }
              >
                <img
                  src={
                    selectedOption === "countries"
                      ? "https://stagecdn.waosim.com/landingpage/home/heroBannerPlanPageCountry.png"
                      : "https://stagecdn.waosim.com/landingpage/home/heroBannerPlanPageRegion.png"
                  }
                  alt="plans Banner Image"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PlansHeroBanner;
