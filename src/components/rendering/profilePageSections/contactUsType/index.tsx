import React, { useState } from "react";
import styles from "./contactUsType.module.scss";
import Button from "@/components/modules/button";
import classNames from "classnames";
const backArrow = "https://stagecdn.waosim.com/landingpage/assets/icons/lets-icons_back.svg";
import Dropdown from "react-bootstrap/Dropdown";
import { Row } from "react-bootstrap";
import Link from "next/link";
const aboutPlan = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/about_Plan.svg";
const about_eSIM = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/about_eSIM.svg";
const about_cancellation = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/about_cancellation.svg";
const about_waoClub = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/about_waoClub.svg";
const about_Payment = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/about_Payment.svg";
const activation_assistance = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/activation_assistance.svg";
const device_compatibility = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/device_compatibility.svg";
const network_coverage = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/network_coverage.svg";
const data_Usage_Monitoring = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/data_Usage_Monitoring.svg";
const transfer_eSIM = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/transfer_eSIM.svg";
const others = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/others.svg";
const upload = "https://stagecdn.waosim.com/landingpage/assets/icons/inquiry/file_upload_icon.svg";
const deleteIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/delete_icon_light.png";
const messageIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/MessageAttachFile_icon.svg";
import { usePathname } from "next/navigation";

const ContactUsType = ({ eSimList, setESimList, iccid }: any) => {
  const pathname = usePathname();
  const activeRoute = pathname?.split("/")[2];
  const inquiryArray = [
    {
      inquiryImage: aboutPlan,
      alt: "About Plan",
      title: "About Plan",
      description:
        "Inquire about the details of your eSIM plan, including data allowances, validity period, and any included features.",
    },
    {
      inquiryImage: about_eSIM,
      alt: "About eSIM",
      title: "About eSIM",
      description:
        "Seek information about the eSIM technology, its compatibility with your device, and how to activate it.",
    },
    {
      inquiryImage: about_cancellation,
      alt: "About Cancellation ",
      title: "About Cancellation ",
      description:
        "Inquire about the process and conditions for canceling your eSIM plan, including any associated fees or penalties.",
    },
    {
      inquiryImage: about_waoClub,
      alt: "About WaoClub",
      title: "About WaoClub",
      description:
        "Learn about the benefits and perks of joining the WaoClub program, such as exclusive offers, discounts, and rewards.",
    },
    {
      inquiryImage: about_Payment,
      alt: "About Payment",
      title: "About Payment  ",
      description:
        "Seek clarification on payment methods, billing cycles, and any charges associated with your eSIM plan.",
    },
    {
      inquiryImage: activation_assistance,
      alt: "Activation Assistance",
      title: "Activation Assistance",
      description:
        "Request assistance with activating your eSIM, including step-by-step guidance or troubleshooting any issues encountered during the activation process.",
    },
    {
      inquiryImage: device_compatibility,
      alt: "Device Compatibility",
      title: "Device Compatibility",
      description:
        "Inquire about the compatibility of your device with eSIM technology and verify whether your device supports eSIM functionality.",
    },
    {
      inquiryImage: network_coverage,
      alt: "Network Coverage",
      title: "Network Coverage",
      description:
        "Seek information about the network coverage and availability of service providers offering eSIM plans in your area or destination.",
    },
    {
      inquiryImage: data_Usage_Monitoring,
      alt: "Data Usage Monitoring",
      title: "Data Usage Monitoring",
      description:
        "Request guidance on how to monitor your data usage, check remaining data balance, and manage data usage settings with your eSIM plan.",
    },
    {
      inquiryImage: transfer_eSIM,
      alt: "Transfer of eSIM",
      title: "Transfer of eSIM",
      description:
        "Inquire about the process for transferring your eSIM to a new device or replacing your device while retaining the same eSIM plan.",
    },
    {
      inquiryImage: others,
      alt: "Others",
      title: "Others",
      description:
        "For any other inquiries not covered by the above options, such as troubleshooting issues, technical support, or general questions about eSIM usage.",
    },
  ];
  return (
    <>
      <div className={styles.contactUsTypeSection}>
        <div className={styles.contactUsTypefirstMain}>
          <button className={styles.contactUsBackDiv}>
            <div className={styles.BackButton}>
              <img src={backArrow} alt="backArrow"></img>
            </div>
            <h2>Back</h2>
          </button>
          <div className={styles.caseDetailToggleMain}>
            <div className={styles.toggleSection}>
              <Link
                href={"#"}
                className={`${styles.option} ${
                  activeRoute === "" ? styles.selected : ""
                }`}
              >
                <span>Case Details</span>
              </Link>
              <Link
                href={"#"}
                className={`${styles.option} ${
                  activeRoute === "" ? styles.selected : ""
                }`}
              >
                <span>Message History</span>
              </Link>
            </div>
          </div>
        </div>

        {/* case Detail */}
        <div className={styles.contactUsDropdownMain}>
          <label htmlFor="InquiryType">Inquiry Type </label>
          <div className={styles.inquiryTypeDropdown}>
            <Dropdown className={styles.dropdown}>
              <Dropdown.Toggle
                id="dropdown-basic"
                className={styles.dropdownToggle}
              >
                <h5 style={{ cursor: "pointer" }}>Choose</h5>
              </Dropdown.Toggle>
              <Dropdown.Menu className={styles.dropdownMenu}>
                {inquiryArray.map((items, index) => (
                  <Dropdown.Item className={styles.dropdownItem} key={index}>
                    <div className={styles.dropdownPlaneImage}>
                      <img src={items.inquiryImage} alt={items.alt} />
                    </div>
                    <div className={styles.planContact}>
                      <h3>{items.title}</h3>
                      <p>{items.description}</p>
                    </div>
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </div>

          <label htmlFor="Select The Plan">Select The Plan </label>
          <div className={styles.selectPlanTypeDropdown}>
            <Dropdown className={styles.dropdown}>
              <Dropdown.Toggle
                id="dropdown-basic"
                className={styles.dropdownToggle}
              >
                <h5 style={{ cursor: "pointer" }}>Choose</h5>
              </Dropdown.Toggle>
              <Dropdown.Menu className={styles.dropdownMenu}>
                <div className={styles.dropdownItemSerach}>
                  <input type="search" placeholder="Search" id="search" />
                </div>
                {[0, 1, 2, 3].map((items, index) => (
                  <Dropdown.Item className={styles.dropdownItem} key={index}>
                    <div className={styles.planContactDetailMain}>
                      <div className={styles.planContact}>
                        <h3>Japan</h3>
                      </div>
                      <div className={styles.planDisplayData}>
                        <div className={styles.planData}>
                          <p>Data</p>
                          <h3>1GB/Day</h3>
                        </div>
                        <div className={styles.planData}>
                          <p>Validity</p>
                          <h3>3 Days</h3>
                        </div>
                      </div>
                    </div>
                    <div className={styles.borderDiv}></div>
                    <div className={styles.planContactDetaiEsimlMain}>
                      <div className={styles.planContacteSimName}>
                        <p>eSIM Name:</p>
                        <h3>eSim Name 1</h3>
                      </div>
                      <div className={styles.IccidData}>
                        <p>ICCID:</p>
                        <h3>89852342022034934967</h3>
                      </div>
                    </div>
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
        <div className={styles.contactUSMessage}>
          <label htmlFor="Message">Message </label>
          <textarea
            className="form-control"
            placeholder="Description"
            rows={3.5}
          />
          <label htmlFor="AttachedFiles">Attached Files </label>
          <div className={styles.fileUpload}>
            <input type="file" id="file" multiple />
            <button type="submit" className={styles.uploadButton}>
              <img src={upload} alt="upload" />
              <span>File Upload</span>
            </button>
          </div>
          <div className={styles.fileUploadDisplayMain}>
            <div className={styles.fileUploadDisplayInnerMain}>
              <div className={styles.fileUploadDisplay}>
                <span>File1.jpg</span>
                <img src={deleteIcon} alt="delete icon" />
              </div>
            </div>
            <div className={styles.sendMessageButton}>
              <Button color={"orange"} type={"solid"} title={`Send Message`} />
            </div>
          </div>
        </div>

        {/* Message History */}
        {/* <div className={styles.MessageHistoryMain}>
          <div className={styles.messageHistoryBox}>
            <div className={styles.messageNameDate}>
              <h1>WaoSIM Team</h1>
              <p>2024-07/12 18:00</p>
            </div>

            <label htmlFor="Message">Message </label>
            <textarea
              className="form-control"
              placeholder="Description"
              rows={3.5}
            />
            <label htmlFor="AttachedFiles">Attached Files </label>

            <div className={styles.fileUploadDisplayMain}>
              <div className={styles.fileUploadDisplayInnerMain}>
                <div className={styles.fileUploadDisplay}>
                  <span>File1.jpg</span>
                  <img src={messageIcon} alt="delete icon" />
                </div>
              </div>
            </div>
          </div>
        </div> */}
      </div>
    </>
  );
};

export default ContactUsType;
