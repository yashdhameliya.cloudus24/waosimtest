import React, { useState } from "react";
import styles from "./settingAccountInfo.module.scss";
import { isValidPhoneNumber, parsePhoneNumber } from "react-phone-number-input";
import { toastError, toastSuccess } from "@/utils";
import { signupNewUser } from "@/utils/api";
import { signInWithPhoneNumber } from "firebase/auth";
import { setItemInSession } from "@/utils/useHooks/useStorage";
import { useRouter } from "next/router";
import PhoneInputCommon from "@/components/modules/phoneInputCommon";
import { auth } from "@/utils/socialLogin/Firebase";
const name = "https://stagecdn.waosim.com/landingpage/assets/icons/name.svg";
const email = "https://stagecdn.waosim.com/landingpage/assets/icons/email.svg";
const arrow_back = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back.svg";
const arrow_back_orange = "https://stagecdn.waosim.com/landingpage/assets/icons/bx-arrow-back-orange.svg";

const SettingAccountInfo = ({ userLocationData }: any) => {
  const [loginData, setLoginData] = useState<any>({ agreeCoditions: false });
  const [eyeOpen, setEyeOpen] = useState(false);
  const [loginViePhone, setLoginViePhone] = useState(false);
  const [loading, setLoging] = useState(false);
  const [updateLoader, setUpdateLoader] = useState(false);
  const router = useRouter();

  const handleOnSendOtp = async (data: any) => {
    const phoneNumber = loginData?.phoneNumber;
    const appVerifier = window.recaptchaVerifier;

    await signInWithPhoneNumber(auth, phoneNumber, appVerifier)
      .then((confirmationResult) => {
        window.confirmationResult = confirmationResult;
        setLoging(false);
        data?.message && toastSuccess(data?.message);
        setItemInSession("isFormSigninSinup", true);

        router.push(
          `/auth/otp-verify?user_id=${data?.userID}&phone=${loginData?.mobile}&country_code=${loginData?.country_code}`
        );
      })
      .catch((error) => {
        setLoging(false);
        toastError("Something went wrong");
      });
  };

  const handleOnSubmitForm = async (e: any) => {
    e.preventDefault();

    if (loginViePhone && !isValidPhoneNumber(loginData?.phoneNumber)) {
      // setLoginData({
      //   ...loginData,
      //   country_code: "",
      //   mobile: "",
      //   phoneNumber: "",
      // });
      toastError("Please Enter valid number");
    } else {
      setLoging(true);
      // const fcmToken = await requestNotificationPermission();
      const fcmToken =
        "f7I7KPWww7vjRV-JLa8ZYM:APA91bHx9uDcLvWC4zNXKNFuXRt1KJ6Rs_sIjAbM8pwto1cic2KJgDg7ylWJe_0TwHB4avAGLGIeknujj3UVq0Acq0UFawx0r7gnJzpoNNpIm8bdYvDqHaOqrme7HLHCq377cnyMTlHw";

      let payload = {
        account_type: loginViePhone ? 2 : 1, //1 => email, 2 =>phone number, 3 => google login, 4 => apple login
        full_name: loginData?.name,
        email: loginData?.email,
        mobile: loginData?.mobile,
        country_code: loginData?.country_code,
        password: loginData?.password,
        fcm_id: fcmToken || "",
        device: 3,
      };

      const signup = await signupNewUser({
        method: "POST",
        body: payload,
      });
      if (signup?.success) {
        if (loginViePhone) {
          let data = {
            message: signup?.message,
            userID: signup?.data?.user_id,
          };

          handleOnSendOtp(data);
        } else {
          signup?.message && toastSuccess(signup?.message);
          setLoging(false);
          setItemInSession("isFormSigninSinup", true);

          router.push(
            `/auth/otp-verify?user_id=${signup?.data?.user_id}&email=${loginData?.email}`
          );
        }
      } else {
        setLoging(false);
        toastError(signup?.message);
      }
    }
  };

  const handlechange = (e: any) => {
    const { name, value } = e.target;
    const emojiRegex = /[\p{Emoji_Presentation}\p{Extended_Pictographic}]/gu;
    let sanitizedValue = value.replace(emojiRegex, "");

    sanitizedValue = sanitizedValue.trimStart();

    const finalValue =
      name === "password" ? sanitizedValue.replace(/\s/g, "") : sanitizedValue;

    if (finalValue?.length > 40) {
      return; // Exit the function if the value exceeds 40 characters
    }

    if (name === "agreeCoditions") {
      setLoginData({ ...loginData, [name]: !loginData?.agreeCoditions });
    } else {
      setLoginData({ ...loginData, [name]: finalValue });
    }
  };

  const handlePhoneNumber = (value: any) => {
    const parsedNumber = value && parsePhoneNumber(value);

    setLoginData({
      ...loginData,
      country_code: parsedNumber?.countryCallingCode,
      mobile: parsedNumber?.nationalNumber,
      phoneNumber: value,
    });
  };

  return (
    <>
      <div className={styles.SettingAccountInfoSection}>
        <div className={styles.heading}>
          <h1>Settings</h1>
        </div>
        <div className={styles.AccountInfoMainFiled}>
          <form onSubmit={(e) => handleOnSubmitForm(e)}>
            <div className={styles.signupForm}>
              <label htmlFor="name">Account Information</label>
              <div className={styles.formInput}>
                <input
                  required
                  name="name"
                  id="name"
                  placeholder="Enter your full name"
                  type="name"
                  value={loginData?.name}
                  onChange={(e) => handlechange(e)}
                  // pattern="^[a-zA-Z0-9](_(?!(\.|_))|\.(?!(_|\.))|[a-zA-Z0-9]){0,16}[a-zA-Z0-9]$"
                  // title="User name can only use letters, numbers, underscores and minimum length is 2 characters"
                />
                <div className={styles.inputTextIcon}>
                  <img src={name} alt="Full Name" />
                </div>
              </div>

              {loginViePhone ? (
                <div className={styles.formInput}>
                  <PhoneInputCommon
                    handlePhoneNumber={handlePhoneNumber}
                    value={loginData?.phoneNumber}
                    required={true}
                    name="phoneNumber"
                    selectedCountryCode={userLocationData?.country_code}
                  />
                </div>
              ) : (
                <div className={styles.formInput}>
                  <input
                    required
                    name="email"
                    id="email"
                    placeholder="Enter mail address"
                    type={!loginData?.email ? "text" : "email"}
                    value={loginData?.email}
                    onChange={(e) => handlechange(e)}
                  />

                  <div className={styles.inputTextIcon}>
                    <img src={email} alt="Email" />
                  </div>
                </div>
              )}

              {/* <div className={styles.formInput}>
                <label htmlFor="password">Password</label>
                <input
                  required
                  name="password"
                  id="password"
                  className={styles.passwoed_input}
                  pattern=".{6,}"
                  title="Must contain at least 6 or more characters"
                  placeholder="Enter your password"
                  type={eyeOpen ? " text" : "password"}
                  value={loginData?.password}
                  onChange={(e) => handlechange(e)}
                />

                <div className={styles.inputTextIcon}>
                  <img src={password} alt="password" />
                </div>
                <div
                  className={styles.paaswordHideShow}
                  onClick={() => setEyeOpen(!eyeOpen)}
                >
                  <img src={eyeOpen ? passwordShow : passwordHide} alt="eye" />
                </div>
              </div> */}

              <div className={styles.updateInfo}>
                <button type="submit">
                  {updateLoader ? (
                    <p>hii</p>
                  ) : (
                    <>
                      <p>Update</p>
                      <img
                        src={arrow_back}
                        alt="back arrow"
                        className={styles.arrowBack}
                      />
                      <img
                        src={arrow_back_orange}
                        alt="back arrow"
                        className={styles.arrowBackOrange}
                      />
                    </>
                  )}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default SettingAccountInfo;
