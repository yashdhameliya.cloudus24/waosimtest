import React, { useState } from "react";
import styles from "./referAndEarnDetails.module.scss";
import LazyImage from "@/components/modules/lazyImage";
import { CopyIcon } from "@/assets/images/icons";
import { toastError, toastSuccess } from "@/utils";
const newSimVector = "https://stagecdn.waosim.com/landingpage/assets/newSimVector.png";
const referralCode = "https://stagecdn.waosim.com/landingpage/assets/referralCode.png";

const ReferAndEarnDetails = ({ eSimList, setESimList, iccid }: any) => {
  const handleOnCopy = (textToCopy: any) => {
    if (textToCopy) {
      navigator.clipboard
        .writeText(textToCopy)
        .then(() => {
          toastSuccess("copied successfully!");
        })
        .catch((err) => {
          toastError("Failed to copy!");
        });
    }
  };
  return (
    <div className={styles.referAndEarnDetailsContainer}>
      <h1>Refer & Earn</h1>

      <div className={styles.referalCodeSection}>
        <LazyImage
          image={{
            src: "https://stagecdn.waosim.com/myprofile/referAndEarnCardImage.png",
            alt: "referAndEarnCardImage",
          }}
          className={styles.referalCodeSection_image}
          disableSkeleton
        />

        <div className={styles.referalCodeCopy}>
          <img src={referralCode} alt="referralCode" />

          <div className={styles.referalCodeCopyText}>
            <h4>your referral code</h4>
            <p>FXKBOH44</p>
          </div>

          <div
            className={styles.copyIcon}
            onClick={() => handleOnCopy("FXKBOH44")}
          >
            <CopyIcon fill="#F47500" isRoundShape={true} />
          </div>
        </div>

        <div className={styles.shareText}>
          <p>Share Amazing eSIM Technology with Friends</p>
        </div>

        <div className={styles.youAndYourFriendText}>
          <p>You and your friends will get 10%</p>
        </div>

        <div className={styles.shareTextMain}>
          <h3>
            Share Your <span>Referral</span> Code
          </h3>
          <p>
            Get 10% dicount for every friend that signs up and completes a
            purchase. Your friends get 10% Discount for their first purchase.
          </p>

          <h5>Share to</h5>
          <div className={styles.socialIconsSection}>
            <img
              src="https://stagecdn.waosim.com/landingpage/icons/faceBookIcon.png"
              alt="facebook"
            />
            <img
              src="https://stagecdn.waosim.com/landingpage/icons/instagramIcon.png"
              alt="instagramIcon"
            />
            <img
              src="https://stagecdn.waosim.com/landingpage/icons/twiterIcon.png"
              alt="twiterIcon"
            />
            <img
              src="https://stagecdn.waosim.com/landingpage/icons/whatAppIcon.png"
              alt="whatAppIcon"
            />
            <img
              src="https://stagecdn.waosim.com/landingpage/icons/telegramIcon.png"
              alt="telegramIcon"
            />
          </div>
        </div>

        <div className={styles.yourInvitesSection}>
          <h3>Your Invites</h3>

          <div className={styles.yourInvitesFriend}></div>
        </div>
      </div>
    </div>
  );
};

export default ReferAndEarnDetails;
