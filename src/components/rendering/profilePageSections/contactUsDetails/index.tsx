import React, { useState } from "react";
import styles from "./contactUsDetails.module.scss";
import Button from "@/components/modules/button";
import classNames from "classnames";
import { Dropdown } from "react-bootstrap";
const searchNormal = "https://stagecdn.waosim.com/landingpage/assets/icons/search-normal.svg";
const searchFilter = "https://stagecdn.waosim.com/landingpage/assets/icons/icon_filter.svg";

const ContactUsDetails = ({ eSimList, setESimList, iccid }: any) => {
  return (
    <>
      <div className={styles.contactUsDetailsSection}>
        <div className={styles.contactUSDatailsMainTitle}>
          <h2>Inquiry</h2>
          <div className={styles.createNewCaseButton}>
            <Button color={"orange"} type={"solid"} title={`Create New Case`} />
          </div>
        </div>
        <div className={styles.contactUsSearchFilterMain}>
          <div className={styles.contactSearchMain}>
            <img src={searchNormal} alt="search normal" />
            <input type="text" className="form-control" placeholder="Search" />
          </div>
          <div className={styles.contactFilterMain}>
            <Dropdown className={styles.dropdown}>
              <Dropdown.Toggle
                id="dropdown-basic"
                className={styles.dropdownToggle}
              >
                <h5 style={{ cursor: "pointer" }}>All Cases</h5>
                <img src={searchFilter} alt="search Filter"/>
              </Dropdown.Toggle>
              <Dropdown.Menu className={styles.dropdownMenu}>
                <Dropdown.Item className={styles.dropdownItem}>
                  <h3>All Cases</h3>
                </Dropdown.Item>
                <Dropdown.Item className={styles.dropdownItem}>
                  <h3>Solved</h3>
                </Dropdown.Item>
                <Dropdown.Item className={styles.dropdownItem}>
                  <h3>Pending</h3>
                </Dropdown.Item>
                <Dropdown.Item className={styles.dropdownItem}>
                  <h3>Inprogress</h3>
                </Dropdown.Item>
                <Dropdown.Item className={styles.dropdownItem}>
                  <h3>Closed</h3>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
        <div className={styles.contactUsCreateCaseMain}>
          {/* pending status */}
          <div className={styles.contactUsCreateCase}>
            <div className={styles.aboutPlanTitle}>
              <h2>About Plan</h2>
            </div>
            <div className={styles.inquiryStatusDiv}>
              <p>2024/04/12 13:00</p>
              <div
                className={classNames(styles.inquiryStatus, styles.redColor)}
              >
                <h2>Pending </h2>
              </div>
              <span>3</span>
            </div>
          </div>
          {/* solved status */}
          <div className={styles.contactUsCreateCase}>
            <div className={styles.aboutPlanTitle}>
              <h2>About Plan</h2>
            </div>
            <div className={styles.inquiryStatusDiv}>
              <p>2024/04/12 13:00</p>
              <div
                className={classNames(styles.inquiryStatus, styles.GreenColor)}
              >
                <h2>Solved </h2>
              </div>
            </div>
          </div>
          {/* Closed status */}
          <div className={styles.contactUsCreateCase}>
            <div className={styles.aboutPlanTitle}>
              <h2>About Plan</h2>
            </div>
            <div className={styles.inquiryStatusDiv}>
              <p>2024/04/12 13:00</p>
              <div
                className={classNames(styles.inquiryStatus, styles.BlueColor)}
              >
                <h2>Closed </h2>
              </div>
            </div>
          </div>
          {/* InProgress status */}
          <div className={styles.contactUsCreateCase}>
            <div className={styles.aboutPlanTitle}>
              <h2>About Plan</h2>
            </div>
            <div className={styles.inquiryStatusDiv}>
              <p>2024/04/12 13:00</p>
              <div
                className={classNames(styles.inquiryStatus, styles.OrangeColor)}
              >
                <h2>Inprogress </h2>
              </div>
            </div>
          </div>      
        </div>
      </div>
    </>
  );
};

export default ContactUsDetails;
