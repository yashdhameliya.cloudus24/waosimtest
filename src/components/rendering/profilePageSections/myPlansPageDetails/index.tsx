import React, { useContext, useEffect, useState } from "react";
import styles from "./myPlansPageDetails.module.scss";
import { MyPlanActiveIcon, RecycleIcon } from "@/assets/images/icons";
import NetworkIcon from "@/components/modules/IconsToggle/networkIcon";
import Image from "next/image";
import classNames from "classnames";
import PlanDetailsModule from "@/components/modules/planDetails";
import { Tooltip } from "@/components/modules/toolTip";
import MyEsimList from "@/components/modules/myEsimList";
import { useSidebar } from "@/components/contexts/sidebarContext/context";
import { getCookie } from "@/utils/useHooks/useCookies";
import { getEsimPlans } from "@/utils/api/auth";
import NoProfileMenuData from "@/components/modules/noProfileMenuData";
import { GlobalContext } from "@/components/contexts/globalContext/context";
import { slugify } from "@/utils";
import { useRouter } from "next/router";
const dropdownIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/dropdown_icon.svg";
const plandetailList_map_img = "https://stagecdn.waosim.com/landingpage/assets/icons/plandetailList _map_img.svg";
const validityPlan = "https://stagecdn.waosim.com/landingpage/assets/icons/validity_plan.svg";
const dataValidateIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/myProfile/dataValidateIcon.png";
const informationIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/informationIcon.svg";
const buyAgainIcon = "https://stagecdn.waosim.com/landingpage/assets/icons/buyAgainIcon.png";
const buyAgainIconOrange = "https://stagecdn.waosim.com/landingpage/assets/icons/buyAgainIconOrange.png";
import ArcProgress from "react-arc-progress";

const MyPlansPageDetails = ({ eSimList, eSimPlansData, iccid }: any) => {
  const router = useRouter();
  const { setIsglobalLoaderActive } = useContext(GlobalContext);
  const [selectedOption, setSelectedOption] = useState("currentPlan");
  const [planDetailsOpen, setPlanDetailsOpen] = useState(false);
  const { openSidebar, closeSidebar } = useSidebar();
  const [selectedESim, setSelectedESim] = useState<any>(
    iccid
      ? eSimList?.find((item: any, index: any) => item?.iccid === iccid)
      : eSimList[0]
  );
  const [selectedESimSub, setSelectedESimSub] = useState<any>(
    iccid
      ? eSimList?.find((item: any, index: any) => item?.iccid === iccid)
      : eSimList[0]
  );
  const [progress, setProgress] = useState(0);

  const [selectedESimPlansData, setSelectedESimPlansData] =
    useState<any>(eSimPlansData);
  const [selectedPlanDetailsData, setSelectedPlanDetailsData] =
    useState<any>(null);

  const filteredEsimPlans = selectedESimPlansData?.filter((obj: any) => {
    const targetStatus = selectedOption === "usedPlan" ? [2] : [1, 3];
    return targetStatus.includes(obj?.status);
  });

  const handleOnOpenPlanDetail = (item: any) => {
    setSelectedPlanDetailsData(item);
    setPlanDetailsOpen(true);
  };

  const handleOnClosePlanDetail = (e: any) => {
    e.preventDefault();
    setSelectedPlanDetailsData(null);
    setPlanDetailsOpen(false);
  };

  const handleOptionChange = (option: any) => {
    setSelectedOption(option);
  };

  const handleOnfetch = async () => {
    setIsglobalLoaderActive(true);
    const token = await getCookie("waotoken");

    let payload = {
      waotoken: token,
      iccid: await selectedESim?.iccid,
    };
    const { isErrors, esimPlansResponse } = await getEsimPlans(payload);
    setSelectedOption("currentPlan");
    setSelectedESimPlansData(esimPlansResponse);
    setIsglobalLoaderActive(false);
  };

  useEffect(() => {
    if (!selectedESimSub) {
      handleOnfetch();
    }
  }, [selectedESim]);

  const handleOnDeleteClose = (e: any) => {
    handleOnClosePlanDetail(e);
    handleOnfetch();
  };

  const handleOnOpenSideBar = (type: any) => {
    openSidebar(
      <>
        {type === "myESim" && (
          <MyEsimList
            closeSidebar={closeSidebar}
            eSimList={eSimList}
            setSelectedESim={setSelectedESim}
            selectedESim={selectedESim}
            isProfileScreen={true}
            setSelectedESimSub={setSelectedESimSub}
          />
        )}
      </>
    );
  };

  const handleOnTopUp = ({ e, item }: any) => {
    e.preventDefault();
    e.stopPropagation();

    if (item?.region_id) {
      router.push(
        `/plans/regions/${slugify(item?.name)}?id=${item?.region_id}&topupId=${
          item?.iccid
        }`
      );
    } else if (item?.mcc) {
      router.push(
        `/plans/countries/${slugify(item?.name)}?mcc=${item?.mcc}&topupId=${
          item?.iccid
        }`
      );
    } else {
      router.push(`/plans`);
    }
  };

  const handleOnBuyAgain = ({ e, item }: any) => {
    e.stopPropagation();

    let redirectLink = item?.region_id
      ? `/plans/review-order?region_id=${item?.region_id}&plan_id=${item?.plan_id}`
      : `/plans/review-order?mcc=${item?.mcc}&plan_id=${item?.plan_id}`;

    router.push(redirectLink);
  };

  const customText = [
    { text: "dsdsdsdsd", size: "34px", color: "red", x: 100, y: 94 },
    { text: "/100", size: "14px", color: "red", x: 100, y: 122 },
  ];

  return (
    <div className={styles.MyPlansPageDetailsContainer}>
      {planDetailsOpen ? (
        <PlanDetailsModule
          handleOnClosePlanDetail={handleOnClosePlanDetail}
          selectedPlanDetailsData={selectedPlanDetailsData}
          handleOnDeleteClose={handleOnDeleteClose}
          selectedESimPlansData={selectedESimPlansData}
          setSelectedESimPlansData={setSelectedESimPlansData}
          setPlanDetailsOpen={setPlanDetailsOpen}
        />
      ) : (
        <>
          <div className={styles.myPlanTopSection}>
            <h1>Plans</h1>
            <div
              className={styles.myPlanDropDown}
              onClick={() => {
                handleOnOpenSideBar("myESim");
              }}
            >
              <MyPlanActiveIcon fill={"#F47500"} />
              {selectedESim?.esim_name && (
                <span>{selectedESim?.esim_name}</span>
              )}
              <img src={dropdownIcon} alt="dropdownIcon" />
            </div>
          </div>

          <div className={styles.myPlanTopToggleSection}>
            <div
              className={`${styles.option} ${
                selectedOption === "currentPlan" ? styles.selected : ""
              }`}
              onClick={() => handleOptionChange("currentPlan")}
            >
              <span>
                <MyPlanActiveIcon
                  fill={
                    selectedOption === "currentPlan" ? "#ffffff" : "#F47500"
                  }
                />{" "}
                Current Plans
              </span>
            </div>
            <div
              className={`${styles.option} ${
                selectedOption === "usedPlan" ? styles.selected : ""
              }`}
              onClick={() => handleOptionChange("usedPlan")}
            >
              <span>
                {" "}
                <MyPlanActiveIcon
                  fill={selectedOption === "usedPlan" ? "#ffffff" : "#F47500"}
                />{" "}
                Used Plans
              </span>
            </div>
          </div>

          {filteredEsimPlans?.length > 0 ? (
            <div className={styles.curentPlanUsedPlansList}>
              {filteredEsimPlans?.map((item: any, index: number) => {
                return (
                  <div
                    className={styles.curentPlanUsedPlansListItem}
                    key={`selectedESimPlansData=${index}`}
                    onClick={() => handleOnOpenPlanDetail(item)}
                  >
                    {item?.is_topup && (
                      <div className={styles.planPopup}>
                        <Tooltip
                          title="Top Up Details"
                          content="This plan will automatically active once previous plan consumed!"
                          position="top"
                        >
                          <img src={informationIcon} alt="informationIcon" />
                        </Tooltip>

                        <div className={styles.planPopupTitle}>
                          <p>Top Up</p>
                        </div>
                      </div>
                    )}

                    <div className={styles.backgroundSection}>
                      <img
                        src={plandetailList_map_img}
                        alt="plandetailList_map_img"
                      />
                    </div>
                    <div className={styles.planListGbDetailsItem}>
                      <div className={styles.planListCountryCard}>
                        <div className={styles.planListCountryCard_image}>
                          <img src={item?.card_image} alt={item?.name} />
                        </div>
                        <p>{item?.name}</p>

                        {/* {planItem?.is_popular_deal === 1 && (
                      <div className={styles.planecoupan}>
                        <p>Discount</p>
                        <h1>{`10%`}</h1>
                      </div>
                       )} */}
                      </div>
                      <div className={styles.planListDetailsMain}>
                        <div className={styles.planListPlanDetails}>
                          <div className={styles.planListPlanDetailsCard}>
                            <div className={styles.iconsContainer}>
                              <NetworkIcon isActive={true} />
                            </div>

                            <div className={styles.planDetails}>
                              <p>Data</p>
                              <span>
                                {`${item?.plan_name} ${
                                  item?.type === 1 ? `` : `/ Day`
                                } `}
                              </span>
                            </div>
                          </div>
                          <div className={styles.planListPlanDetailsCard}>
                            <img src={validityPlan} alt="validity" />
                            <div className={styles.planDetails}>
                              <p>Validity</p>
                              <span>{`${item?.plan_days} ${
                                item?.plan_days > 1 ? "Days" : "Day"
                              }`}</span>
                            </div>
                          </div>
                          <div className={styles.unUsedtopUpButton}>
                            {selectedOption === "currentPlan" && (
                              <>
                                {item?.status === 3 ? (
                                  <button
                                    className={styles.blueButton}
                                    onClick={(e) => e.stopPropagation()}
                                  >
                                    <RecycleIcon fill="white" />
                                    <span>In-Use</span>
                                  </button>
                                ) : (
                                  <button
                                    className={styles.yellowButton}
                                    onClick={(e) => e.stopPropagation()}
                                  >
                                    <RecycleIcon fill="black" />
                                    <span>Unused</span>
                                  </button>
                                )}
                                {/* {item?.isSupportFuelpack && ( */}
                                <button
                                  className={styles.plainButton}
                                  onClick={(e) => handleOnTopUp({ e, item })}
                                >
                                  <span>Top Up</span>
                                </button>
                                {/* )} */}
                              </>
                            )}

                            {selectedOption === "usedPlan" && (
                              <>
                                <button
                                  className={styles.orangeButton}
                                  onClick={(e) => handleOnBuyAgain({ e, item })}
                                >
                                  <img src={buyAgainIcon} alt="buyAgainIcon" className={styles.whiteImage}/>
                                  <img src={buyAgainIconOrange} alt="buyAgainIconOrange" className={styles.OrangeImage} />
                                  <span>Buy Again</span>
                                </button>
                                {item?.status === 2 && (
                                  <button
                                    className={styles.redButton}
                                    onClick={(e) => e.stopPropagation()}
                                  >
                                    <span>
                                      {item?.activeTime?.length > 0
                                        ? `Used on : ${item?.expireTime}`
                                        : `Expired On : ${item?.endTime}`}
                                    </span>
                                  </button>
                                )}
                              </>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                    {item?.status === 3 && (
                      <div className={styles.inUseSection}>
                        <div className={styles.inUseSectionLeft}>
                          <div className={styles.dataUsage}>
                            <h4>
                              {" "}
                              <Image
                                src={dataValidateIcon}
                                width={18}
                                height={18}
                                alt="dataValidateIcon"
                              />
                              Data Validity{" "}
                            </h4>

                            {item?.activeDay ? (
                              <p>
                                <span>{item?.activeDay} </span>
                                {`/ ${item?.plan_days} ${
                                  item?.plan_days > 1 ? "Days" : "Day"
                                }`}{" "}
                              </p>
                            ) : (
                              <b> -- / --</b>
                            )}
                          </div>
                        </div>
                        <div className={styles.inUseSectionRight}>
                          <div className={styles.progressBar}>
                            <>
                              {/* <div
                              style={{
                                // height: "100%",
                                // width: "100%",
                                borderRadius: 50,
                                position: "absolute",
                                left: 58,
                                marginTop: 50,
                                boxShadow: `1px 3px 1px rgba(0,0,0,0.1)`,
                              }}
                            >
                            </div> */}

                              <ArcProgress
                                progress={item?.used_percentage / 100}
                                fillColor={"#f47500"}
                                text={`${progress}%`}
                                size={73}
                                textStyle={{
                                  color: "white",
                                  size: "16px",
                                  length: 10,
                                }}
                                emptyColor={"#F2F2F2"}
                                thickness={8}
                                customText={customText}
                                lineCap="round"
                                observer={(current) => {
                                  const { percentage, currentText } = current;
                                  setProgress(percentage);
                                }}
                                animationEnd={({ progress, text }) => {}}
                              />
                            </>
                          </div>
                          <div className={styles.usedAndRemaingSectoion}>
                            <div className={styles.usedSection}>
                              <p>
                                {item?.used_data} <span> Used</span>
                              </p>
                            </div>
                            <div
                              className={classNames(
                                styles.usedSection,
                                styles.greenBox
                              )}
                            >
                              <p>
                                {item?.remain_data} <span> Remaining</span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                );
              })}
            </div>
          ) : (
            <NoProfileMenuData
              backgroundImage={
                "https://stagecdn.waosim.com/myprofile/noPlanCardImage.png"
              }
              buttonTitle={`Discover Now`}
              buttonLink={`/plans`}
            />
          )}
        </>
      )}
    </div>
  );
};

export default MyPlansPageDetails;
