import React, { useState } from "react";
import styles from "./myEsimDetails.module.scss";
import Button from "@/components/modules/button";
import OneEsimDetails from "@/components/modules/oneEsimDetails";
const esim_icon = "https://stagecdn.waosim.com/landingpage/assets/icons/my_new_esim-icon.svg";
const newSimVector = "https://stagecdn.waosim.com/landingpage/assets/newSimVector.png";

const MyeSIMDetails = ({ eSimList, setESimList, iccid }: any) => {
  const [openSectionDetails, setOpenSectionDetails] = useState(
    iccid
      ? eSimList?.find((item: any, index: any) => item?.iccid === iccid)
      : null
  );

  return (
    <>
      {openSectionDetails ? (
        <OneEsimDetails
          openSectionDetails={openSectionDetails}
          setOpenSectionDetails={setOpenSectionDetails}
          setESimList={setESimList}
          eSimList={eSimList}
        />
      ) : (
        <>
          <div className={styles.myEsimDetailsContainer}>
            <h2>My eSIM</h2>
            <div className={styles.myEsimListContainer}>
              {eSimList?.map((item: any, index: any) => {
                return (
                  <div className={styles.myEsimListCard} key={index}>
                    <div className={styles.myEsimListCard_image}>
                      <img src={esim_icon} alt="esim icon" />
                    </div>

                    <div className={styles.myEsimListCardDetail}>
                      <h4>{item?.esim_name}</h4>
                      <p>
                        ICCID : <span>{item?.iccid}</span>
                      </p>
                    </div>

                    <div
                      className={styles.myEsimListCardManageEsimButton}
                      onClick={() => setOpenSectionDetails(item)}
                    >
                      <img src={esim_icon} alt="esim icon" />

                      <p>Manage eSIM</p>
                    </div>
                  </div>
                );
              })}
            </div>

            <div className={styles.getNewSimSection}>
              <div className={styles.newSimVector}>
                <img src={newSimVector} alt="newSimVector" />
              </div>
              <div className={styles.newSimImage}>
                <img
                  src="https://stagecdn.waosim.com/myprofile/noWishlistCardImage.png"
                  alt="noWishlistCardImage"
                />
              </div>

              <p>Do you want a new eSIM?</p>

              <Button
                color={"orange"}
                type={"transparent"}
                title={`GOT IT NOW`}
                link="/plans"
              />
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default MyeSIMDetails;
