import React, { useState } from "react";
import styles from "./planList.module.scss";
import LazyImage from "@/components/modules/lazyImage";
import Button from "@/components/modules/button";
import classNames from "classnames";
import LikeIcon from "@/components/modules/IconsToggle/likeIcon";
import NetworkIcon from "@/components/modules/IconsToggle/networkIcon";
import { setItemInSession } from "@/utils/useHooks/useStorage";
import { useRouter } from "next/router";
import { useUser } from "@/components/contexts/userContext/context";
import { handleOnWhishList } from "@/utils/api/faq";
import { toastError, toastSuccess } from "@/utils";
const backGround = "https://stagecdn.waosim.com/landingpage/assets/nesw_articals_bg.png";
const validityPlan = "https://stagecdn.waosim.com/landingpage/assets/icons/validity_plan.svg";
const totalPlan = "https://stagecdn.waosim.com/landingpage/assets/icons/total_plan.svg";
const pricePlan = "https://stagecdn.waosim.com/landingpage/assets/icons/price_plan.svg";

const PlanList = ({ response }: any) => {
  const router = useRouter();
  const { id, mcc, topupId } = router?.query;
  const { userDetails } = useUser();
  const [data, setData] = useState(response);

  const [activePlan, setActivePlan] = useState(data?.popularPlanName);
  let activePlanList = data?.plans?.find(
    (item: any) => item?.data_type === activePlan
  );

  const handleOnSignIn = (planItem: any) => {
    const planId = planItem?.plan_id;

    let reviewOrder = {
      name: data?.name,
      plan_name: data?.plan_name,
      network: data?.network,
      providers: data?.providers,
      coverage_data: data?.coverage_data,
      phone_number_sms: data?.phone_number_sms,
      plan_type_text: data?.plan_type_text,
      planId: planId,
      plans: [
        {
          data_type: activePlan,
          data: [planItem],
          data_name: activePlanList?.data_name,
        },
      ],
    };

    let redirectLink = id
      ? `/plans/review-order?region_id=${id}&plan_id=${planId}`
      : `/plans/review-order?mcc=${mcc}&plan_id=${planId}`;

    if (topupId) {
      redirectLink += `&topupId=${topupId}`;
    }

    if (userDetails) {
      setItemInSession("reviewOrder", reviewOrder);
      router.push(redirectLink);
    } else {
      setItemInSession("reviewOrder", reviewOrder);
      if (
        router?.asPath !== "/auth/login" &&
        router?.asPath !== "/auth/signup"
      ) {
        setItemInSession("lastroute", redirectLink);
      }
      router.push("/auth/login");
    }
  };

  const handleOnWhishListClick = async (obje: any) => {
    let redirectLink = router.asPath;
    if (!userDetails) {
      if (
        router?.asPath !== "/auth/login" &&
        router?.asPath !== "/auth/signup"
      ) {
        setItemInSession("lastroute", redirectLink);
      }
      router.push("/auth/login");
    } else {
      let payload = {
        plan_id: obje?.plan_id,
        is_add: obje?.isChecked ? 0 : 1,
      };

      const updatedPlans = data?.plans?.map((plan: any) => {
        if (activePlanList?.data_type === plan?.data_type) {
          const updatedData = plan?.data?.map((obj: any) => {
            if (obj?.plan_id === obje?.plan_id) {
              return {
                ...obj,
                is_wishlist_added: obje?.isChecked == "1" ? 0 : 1,
              };
            } else {
              return obj;
            }
          });

          return { ...plan, data: updatedData };
        } else {
          return plan;
        }
      });

      const { isError, response } = await handleOnWhishList({
        body: payload,
      });

      if (response?.success) {
        setData({ ...data, plans: updatedPlans });
        if (obje?.isChecked) {
          toastError(response?.message);
        } else {
          toastSuccess(response?.message);
        }
      } else {
        toastError(response?.message);
      }
    }
  };

  return (
    <>
      <div className={styles.planListSection}>
        <div className={styles.planListDetailsSection}>
          <div className={styles.planListcard}>
            <div className={styles.planListcardBackground}>
              {/* <img src={data?.background_image} alt={data?.name} /> */}

              <LazyImage
                image={{
                  src: data?.background_image,
                  alt: data?.name,
                }}
                className={styles.planListcardBackgroundImage}
              />

              <div className={styles.countryFlagName}>
                {data?.country_flag && (
                  <div className={styles.countryFlagSection}>
                    <div className={styles.card_round_item}>
                      <img src={data?.country_flag} alt={data?.name} />
                    </div>
                  </div>
                )}

                <h2>{data?.name}</h2>
              </div>
            </div>

            <div className={styles.planListDetailsBox}>
              <div className={styles.planListInGb}>
                {data?.plans?.map((item: any, index: number) => {
                  return (
                    <div
                      className={classNames(
                        styles.planListInGbCard,
                        activePlan === item?.data_type && styles.active
                      )}
                      key={`planList-${index}`}
                      onClick={() => setActivePlan(item?.data_type)}
                    >
                      <input
                        name="plan"
                        className="radio"
                        type="radio"
                        checked={activePlan === item?.data_type}
                      />

                      <label htmlFor="plan4"></label>

                      <span className={styles.planDetails}>
                        <NetworkIcon
                          isActive={activePlan === item?.data_type}
                        />
                        <span>{item?.data_name}</span>
                        <p>
                          {`${item?.data_type === "Fix" ? `Data` : `Per Day`}`}
                        </p>

                        {item?.data_type === data?.popularPlanName &&
                          data?.popularPlanName !== "Fix" && (
                            <div className={styles.popularPlan}>
                              <h3>Popular</h3>
                            </div>
                          )}
                      </span>
                    </div>
                  );
                })}
              </div>
              <div className={styles.planListNotification}>
                <p>
                  {activePlan === "Fix"
                    ? `Enjoy FIX DATA of high-speed internet after that the data service will be turned off.`
                    : `ENJOY ${activePlanList?.data_name} OF HIGH-SPEED INTERNET PER
                    DAY, AFTER THAT THE SPEED WILL BE REDUCED TO 128KBPS.`}
                </p>
              </div>
              <div className={styles.planListGbDetailsMain}>
                {activePlanList?.data?.map((planItem: any, index: number) => (
                  <div className={styles.planListGbDetails} key={index}>
                    <div className={styles.planListGbDetailsItem}>
                      <div className={styles.planListCountryCard}>
                        <LazyImage
                          image={{
                            src: data?.card_image,
                            alt: data?.name,
                          }}
                          className={styles.planListCountryCard_image}
                        />
                        <p>{data?.name}</p>

                        {planItem?.is_popular_deal === 1 && (
                          <div className={styles.planecoupan}>
                            <p>Discount</p>
                            <h1>{`${planItem?.deal_percentage}%`}</h1>
                          </div>
                        )}
                      </div>
                      <div className={styles.planListDetailsMain}>
                        <div className={styles.planListPlanDetails}>
                          <div className={styles.planListPlanDetailsCard}>
                            <div className={styles.iconsContainer}>
                              <NetworkIcon isActive={true} />
                            </div>

                            <div>
                              <p>Data</p>
                              <span>
                                {`${planItem?.plan_name} ${
                                  activePlanList?.data_type === "Fix"
                                    ? ``
                                    : `/ Day`
                                } `}
                              </span>
                            </div>
                          </div>
                          <div className={styles.planListPlanDetailsCard}>
                            <img src={validityPlan} alt="validity" />
                            <div>
                              <p>Validity</p>
                              <span>{`${planItem?.plan_days} ${
                                planItem?.plan_days > 1 ? "Days" : "Day"
                              }`}</span>
                            </div>
                          </div>
                          <div className={styles.planListPlanDetailsCard}>
                            <img src={totalPlan} alt="Total plan" />
                            <div>
                              <p>Total</p>
                              <span>
                                {" "}
                                {planItem?.totalData === 0.5
                                  ? `500 MB`
                                  : `${planItem?.totalData} GB`}{" "}
                              </span>
                            </div>
                          </div>
                          <div className={styles.planListPlanDetailsCard}>
                            <img src={pricePlan} alt="Price plan" />
                            <div>
                              <p>Price</p>
                              <span>
                                <del>{`$ ${planItem?.core_price}`}</del>{" "}
                                {`$ ${planItem?.price}`}
                              </span>
                            </div>
                          </div>
                        </div>

                        <div className={styles.planListBuyNowButton}>
                          <LikeIcon
                            isChecked={planItem?.is_wishlist_added}
                            handleOnWhishListClick={() => {
                              let obje = {
                                isChecked: planItem?.is_wishlist_added,
                                plan_id: planItem?.plan_id,
                              };

                              handleOnWhishListClick(obje);
                            }}
                            plan_id={planItem?.plan_id}
                          />
                          <div className={styles.planListBuyNowButtonGlobal}>
                            <Button
                              color={"orange"}
                              type={"solid"}
                              title={`Buy now`}
                              clickHandler={() => handleOnSignIn(planItem)}
                              // link="/auth/signup"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className={styles.planListBackground}>
          <img src={backGround} alt="herobanner" />
        </div>
      </div>
      <div className={styles.footer_color}></div>
    </>
  );
};

export default PlanList;
