import React from "react";
import styles from "./milestoneLevel.module.scss";
import Link from "next/link";
const arrowPlanFullLength = "https://stagecdn.waosim.com/landingpage/assets/icons/arrowPlanFullLength.svg";
const arrowPlanMulti = "https://stagecdn.waosim.com/landingpage/assets/icons/arrowPlanMulti.svg";
const milstoneLevel_object = "https://stagecdn.waosim.com/landingpage/assets/icons/milstoneLevel_object.svg";
const milstoneBottomLevel_object = "https://stagecdn.waosim.com/landingpage/assets/icons/milstoneBottom_object.svg";

const MilestoneLevel = ({ splashResponse }: any) => {
  console.log("splashRessplashResponseponse", splashResponse);

  let levelDataArray = [
    {
      levelName: "Blue",
      levelImageUrl:
        "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_Blue.svg",
      discountPercentage: splashResponse?.blue_leval_percentage,
      cancellationMonth: splashResponse?.blue_level_cancellation_month,
    },
    {
      levelName: "Silver",
      levelImageUrl:
        "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_silver.svg",
      discountPercentage: splashResponse?.silver_leval_percentage,
      cancellationMonth: splashResponse?.silver_level_cancellation_month,
    },
    {
      levelName: "Gold",
      levelImageUrl:
        "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_gold.svg",
      discountPercentage: splashResponse?.gold_leval_percentage,
      cancellationMonth: splashResponse?.gold_level_cancellation_month,
    },
    {
      levelName: "Daimond",
      levelImageUrl:
        "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_daimond.svg",
      discountPercentage: splashResponse?.diamond_leval_percentage,
      cancellationMonth: splashResponse?.diamond_level_cancellation_month,
    },
  ];
  return (
    <>
      <div className={styles.MilestoneLevelSection}>
        <div className={styles.arrowPlanFullLengthIcon}>
          <img src={arrowPlanFullLength} alt="arrowPlanFullLength" />
        </div>
        <div className={styles.arrowPlanMultiIcon}>
          <img src={arrowPlanMulti} alt="arrowPlanMulti" />
        </div>
        <div className={styles.MilestoneLevelContainer}>
          <div className={styles.milestoneLevelHeader}>
            <h1>
              Adventure Awaits: Dive into Wao <span>Rewards!</span>
            </h1>
          </div>
          <div className={styles.milestoneLevelContent}>
            <div className={styles.levelOfferMain}>
              <h3>
                <span>WaoClub</span> offers four distinct levels.
              </h3>
              <img
                src={
                  "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_Blue.svg"
                }
                alt="milestoneLevel Blue"
              ></img>
              <p>
                Unlock WaoClub Level 1 and enjoy <span>5% discounts</span> on
                select plans by creating a free account at{" "}
                <span>
                  <Link href={"/"}>waosim.com</Link>
                </span>
              </p>
            </div>
            <div className={styles.levelOfferStepsMain}>
              <div className={styles.OfferSteps}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_silver.svg"
                  }
                  alt="milestoneLevel silver"
                ></img>
                <p>
                  To advance to Level 2, simply complete 5 purchases within two
                  years.
                </p>
              </div>
              <div className={styles.OfferSteps}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_gold.svg"
                  }
                  alt="milestoneLevel gold"
                ></img>
                <p>
                  Reach WaoClub Level 3 by completing 15 purchases within two
                  years.
                </p>
              </div>
              <div className={styles.OfferSteps}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_daimond.svg"
                  }
                  alt="milestoneLevel daimond"
                ></img>
                <p>
                  Progress to WaoClub Level 4 by completing 30 purchases within
                  two years.
                </p>
              </div>
              <div className={styles.levelInfoDiv}>
                <p>Each level unlocks new rewards and even greater discounts</p>
              </div>
              <div className={styles.levelImage}>
                <img
                  src={
                    "https://stagecdn.waosim.com/landingpage/waoClub/milestoneLevel_Img.png"
                  }
                  alt="milestoneLevel Img"
                ></img>
              </div>
            </div>
          </div>

          <div className={styles.milestoneLevelBlockMain}>
            {levelDataArray?.map((item: any, index: number) => {
              return (
                <div
                  className={styles.milestoneLevelBlock}
                  key={`level-${item?.levelName}`}
                >
                  <div className={styles.milestoneLevelImage}>
                    <img src={item?.levelImageUrl} alt={item?.levelName} />
                  </div>
                  <h1>{item?.levelName}</h1>
                  <div className={styles.milestoneLevelDiscount}>
                    <h1>{`${item?.discountPercentage}%`}</h1>
                    <p>Discount</p>
                  </div>
                  <h4>Free Cancellation</h4>
                  <p>{item?.cancellationMonth} Month from the purchase date </p>
                  <div className={styles.objectImage}>
                    <img src={milstoneLevel_object} alt="object Image"></img>
                  </div>
                  <div className={styles.BottomobjectImage}>
                    <img
                      src={milstoneBottomLevel_object}
                      alt="Bottom object Image"
                    ></img>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
};

export default MilestoneLevel;
