import moment from "moment-timezone";

const getPlanDataValue = (planData: any) => {
  switch (planData) {
    case "500MB/day":
      return 0.5;
    case "1GB/day":
      return 1;
    case "2GB/day":
      return 2;
    case "Fix":
      return 1;
    default:
      return 0;
  }
};

interface DataType {
  data_type: string;
}

const findHighestPriority = (arr: DataType[]): string | null => {
  const priorities: { [key: string]: number } = {
    "1GB/day": 1,
    "2GB/day": 2,
    "500MB/day": 3,
    Fix: 4,
  };

  let highestPriorityItem: string | null = null;
  let highestPriorityValue = Infinity;

  for (let item of arr) {
    const priorityValue = priorities[item?.data_type];
    if (priorityValue && priorityValue < highestPriorityValue) {
      highestPriorityValue = priorityValue;
      highestPriorityItem = item?.data_type;
    }
  }

  return highestPriorityItem;
};

const formatMBtoGB = (valueInMB: any) => {
  // Ensure valueInMB is a number
  if (typeof valueInMB === "string") {
    valueInMB = Number(valueInMB);
  }

  if (valueInMB < 1000) {
    return `${valueInMB}MB`;
  } else {
    const valueInGB = valueInMB / 1024;
    // Check if valueInGB is an integer
    if (Number.isInteger(valueInGB)) {
      return `${valueInGB}GB`;
    } else {
      return `${valueInGB.toFixed(2)}GB`;
    }
  }
};

function convertToUserTimeZone(originalDateString: any) {
  // Fetch the user's time zone
  const userTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;

  // Parse the original date string and set it to the user's time zone
  const parsedDate = moment.tz(
    originalDateString,
    "YYYY-MM-DD HH:mm:ss",
    userTimeZone
  );

  // Format the date to the desired format
  const formattedDateString = parsedDate.format("YYYY/MM/DD HH:mm");

  return formattedDateString;
}

export const transformPlanDetailsCountryRegion = async (items: any) => {
  const response: any = {
    // background_image: items?.background_2x_image || items?.background_image,
    background_image: items?.background_image,
    card_image: items?.card_image,
    country_flag: items?.country_flag,
    name: items?.name,
    popularPlanName: findHighestPriority(items?.plans) ?? "",
    network: items?.network ?? "",
    plan_type_text: items?.plan_type_text ?? "",
    phone_number_sms: items?.phone_number_sms ?? "",
    providers: items?.providers ?? null,
    coverage_data: items?.coverage_data ?? null,

    plans:
      items?.plans
        ?.filter((filterItem: any) => filterItem?.data_type !== "Unlimited")
        ?.map((subItem: any) => ({
          data_type: subItem?.data_type ?? "",
          data_name: subItem?.data_type?.split("/")[0],
          data:
            subItem?.data?.map((obj: any) => ({
              core_price: Number(obj?.core_price),
              totalData:
                subItem?.data_type === "Fix"
                  ? parseInt(obj?.plan_data?.split("/")[0]?.match(/\d+/)[0])
                  : Number(obj?.plan_days) * getPlanDataValue(obj?.plan_data),
              deals_discount: Number(obj?.deals_discount),
              is_popular_deal: obj?.is_popular_deal,
              is_wishlist_added: obj?.is_wishlist_added,
              deal_percentage: obj?.deal_percentage ?? null,
              plan_data: obj?.plan_data,
              plan_name: obj?.plan_data?.split("/")[0],
              plan_days: obj?.plan_days,
              plan_id: obj?.plan_id,
              price: Number(obj?.price),
              waoclub_discount: Number(obj?.waoclub_discount),
            })) ?? [],
        })) ?? [],
  };

  return response;
};

export const transformGetEsimPlans = async (items: any) => {
  let newArray = items?.map((item: any, index: any) => {
    return {
      items: items,
      id: item?.id ?? "",
      plan_id: item?.plan_id ?? "",
      plan_days: item?.plan_days ?? null,
      plan_data: item?.plan_data ?? "",
      plan_name: item?.plan_data?.split("/")[0],
      name: item?.name ?? "",
      type: item?.type ?? null,
      is_multi_country_plan: item?.is_multi_country_plan ?? null,
      regional_id: item?.regional_id ?? null,
      card_image: item?.card_image ?? "",
      providers: item?.providers ?? null,
      coverage_data: item?.coverage_data ?? [],
      plan_type_text: item?.plan_type_text ?? "",
      phone_number_sms: item?.phone_number_sms ?? "",
      network: item?.network ?? "",
      iccid: item?.iccid ?? "",
      country_id: item?.country_id ?? null,
      order_id: item?.order_id ?? "",
      mcc: item?.mcc ?? "",
      region_id: item?.region_id ?? null,
      is_topup: item?.is_topup === 1 ? true : false ?? null,
      status: item?.status ?? 1,
      createTime: item?.createTime ?? "",
      expireTime: convertToUserTimeZone(item?.expireTime) ?? "",
      endTime: convertToUserTimeZone(item?.endTime) ?? "",
      activeTime: item?.activeTime ?? "",
      activeDay: item?.activeDay ?? "",
      isSupportFuelpack: item?.isSupportFuelpack === "1" ? true : false ?? "",
      used_data: formatMBtoGB(item?.used_data) ?? null,
      remain_data: formatMBtoGB(item?.remain_data) ?? null,
      sm_dp_address: item?.sm_dp_address ?? "",
      activation_code: item?.activation_code ?? "",
      ios_activation_code: item?.ios_activation_code ?? "",
      used_percentage: item?.used_percentage ?? null,
      achieve_status: item?.achieve_status ?? null,
    };
  });

  return newArray;
};

export const transformFaqData = async (items: any) => {
  let newArray = {
    popularfaq: items?.filter(
      (item: any) => item?.status === 3 || item?.status === 4
    ),
    categoryData: [
      {
        status: 1,
        topName: "About eSIM",
        bottomName: "Installation & Setting",
        name: "About eSIM Installation & Setting",
        list: items?.filter((item: any) => item?.category === 1),
        icon: "https://stagecdn.waosim.com/landingpage/icons/aboutEsimInstallSetting.png",
      },
      {
        status: 2,
        topName: "About",
        bottomName: "Refund & Cancellation",
        name: "About Refund & Cancellation",
        list: items?.filter((item: any) => item?.category === 2),
        icon: "https://stagecdn.waosim.com/landingpage/icons/refundAndCancellation.png",
      },
      {
        status: 3,
        topName: "General",
        bottomName: "Setting & Question",
        name: "General Setting & Question",
        list: items?.filter((item: any) => item?.category === 3),
        icon: "https://stagecdn.waosim.com/landingpage/icons/settingsAndQuestions.png",
      },
      {
        status: 4,
        topName: "About",
        bottomName: "Fees & Costs & Usage",
        name: "About Fees & Costs & Usage",
        list: items?.filter((item: any) => item?.category === 4),
        icon: "https://stagecdn.waosim.com/landingpage/icons/costsAndCharges.png",
      },
      {
        status: 5,
        topName: "About Errors",
        bottomName: "",
        name: "About Errors",
        list: items?.filter((item: any) => item?.category === 5),
        icon: "https://stagecdn.waosim.com/landingpage/icons/abloutErrorDetails.png",
      },
      {
        status: 6,
        topName: "",
        bottomName: "",
        name: "Account Deletion",
        list: items?.filter((item: any) => item?.category === 6),
        icon: null,
      },
    ]?.filter((item: any) => item?.list?.length > 0),
    allData: items,
  };

  return newArray;
};
