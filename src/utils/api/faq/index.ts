import { getCookie } from "@/utils/useHooks/useCookies";
import { restBaseUrl, restBcHeaders } from "..";
import { transformFaqData } from "@/utils/transform";

export async function fetchFaqData(payload: any) {
  const url = `${restBaseUrl}/home/get-faqs?status=${payload?.status ?? 0}`;
  const token = await getCookie("waotoken");

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${payload?.waotoken ?? token}`, // Include token here
    },
    method: "GET",
  });

  const res = await response.json();
  const transformFaqDataRes = payload?.status
    ? res?.data
    : await transformFaqData(res?.data);

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: transformFaqDataRes,
  };
}

export async function handleOnWhishList({ body }: { body: any }) {
  const url = `${restBaseUrl}/user/wishlist-add-remove`;
  const token = await getCookie("waotoken");

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}
