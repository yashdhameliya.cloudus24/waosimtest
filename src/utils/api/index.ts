import logger from "@/utils/logger";
import { getCookie } from "../useHooks/useCookies";

// export const restBaseUrl = `${NEXT_PUBLIC_API_BASEURL}/api`;
export const restBaseUrl = `https://stagingapi.waosim.com/api`;
// export const restBaseUrl = `https://phpstack-1223521-4570264.cloudwaysapps.com/api`;
// export const restBaseUrl = `http://192.168.1.14:8090/api`;

export const restBcHeaders = {
  "content-type": "application/json",
  Accept: "application/json",
  language: "en",
  //   "X-Auth-Token": `${BIGCOMMERCE_ACCESS_TOKEN!}`,
  //   "Access-Control-Allow-Origin": "*",
  Authorization: `Bearer ${getCookie("waotoken")}`, // Include token here
};

export const restBcHeadersFun = async () => {
  const token = await getCookie("waotoken");

  const header = {
    "content-type": "application/json",
    Accept: "application/json",
    language: "en",
    //   "X-Auth-Token": `${BIGCOMMERCE_ACCESS_TOKEN!}`,
    //   "Access-Control-Allow-Origin": "*",
    Authorization: `Bearer ${token}`, // Include token here
  };

  return header;
};

export async function signupNewUser({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "authentication",
      action: "signupNewUser",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await authApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function emailOtpVerify({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "authentication",
      action: "emailOtpVerify",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await authApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function signIn({ method, body }: { method: string; body?: any }) {
  try {
    const json = {
      api: "authentication",
      action: "signin",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await authApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function forgotPassword({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "authentication",
      action: "forgotPassword",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await authApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function resetPassword({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "authentication",
      action: "resetPassword",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await authApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function resendOtp({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "authentication",
      action: "resendOtp",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await authApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function applyCoupon({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "couponCode",
      action: "applyCoupon",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await orderApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function paymentIntent({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "order",
      action: "paymentIntent",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await orderApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function ArchivePlanRoute({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "profile",
      action: "archive-plan",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await profileApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function DeletePlanRoute({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "profile",
      action: "delete-plan",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await profileApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function CancelPlanRoute({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "profile",
      action: "cancel-plan",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await profileApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function UpateEsimName({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "eSIM",
      action: "update-esim-name",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await profileApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function DeleteEsim({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "eSIM",
      action: "delete-esim",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await profileApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function getEsimStatusRouteApi({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "eSIM",
      action: "get-esim-status",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await profileApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function TransferEsimToNewAccountRouteApi({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "eSIM",
      action: "esim-transfer",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await profileApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function HideUnHideEsim({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "eSIM",
      action: "hide-unhide-esim",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await profileApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function getReferralBalanceHistoryRouteApi({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "refer-and-earn",
      action: "get-referral-balance-history",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await profileApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

export async function contactUsFormWithOutLogin({
  method,
  body,
}: {
  method: string;
  body?: any;
}) {
  try {
    const json = {
      api: "authentication",
      action: "contact-us",
    };

    const config = {
      method: method,
      body: JSON.stringify(body),
    };

    const queryString = new URLSearchParams(json).toString();
    const res = await authApi(queryString, config);
    return res.response;
  } catch (err) {
    logger.error("API threw Error", err);
    throw err;
  }
}

// This called to send request `api/bigcommerce/api` inside next setup.

const authApi = async (query: string, config: any) =>
  await (await fetch(`/api/auth/auth?${query}`, config)).json();

const orderApi = async (query: string, config: any) =>
  await (await fetch(`/api/order/order?${query}`, config)).json();

const profileApi = async (query: string, config: any) =>
  await (await fetch(`/api/profile/profile?${query}`, config)).json();
