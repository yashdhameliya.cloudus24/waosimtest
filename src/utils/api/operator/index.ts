import { restBaseUrl, restBcHeaders } from "..";

export async function fetchOperatorData({ token }: { token: any }) {
  const url = `${restBaseUrl}/home/get-apn-operators`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`,
    },
    method: "GET",
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}
