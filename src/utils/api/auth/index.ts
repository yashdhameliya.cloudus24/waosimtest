import { getCookie } from "@/utils/useHooks/useCookies";
import { restBaseUrl, restBcHeaders } from "..";
import { transformGetEsimPlans } from "@/utils/transform";

export async function handleOnSingUp({ body }: { body: any }) {
  const url = `${restBaseUrl}/authentication/register`;

  const response = await fetch(url, {
    headers: restBcHeaders,
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnOtpVerify({ body }: { body: any }) {
  const url = `${restBaseUrl}/authentication/verifyEmailMobile`;

  const response = await fetch(url, {
    headers: restBcHeaders,
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnSingIn({ body }: { body: any }) {
  const url = `${restBaseUrl}/authentication/login`;

  const response = await fetch(url, {
    headers: restBcHeaders,
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnForgotPassword({ body }: { body: any }) {
  const url = `${restBaseUrl}/authentication/forgotPassword`;

  const response = await fetch(url, {
    headers: restBcHeaders,
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnResetPassword({ body }: { body: any }) {
  const url = `${restBaseUrl}/authentication/resetPassword`;

  const response = await fetch(url, {
    headers: restBcHeaders,
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnResendOtp({ body }: { body: any }) {
  const url = `${restBaseUrl}/authentication/resendOTP`;

  const response = await fetch(url, {
    headers: restBcHeaders,
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function getReferralBalance() {
  const url = `${restBaseUrl}/user/referral-balance-get`;
  const token = await getCookie("waotoken");

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
  });

  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function getESimList(payload: any) {
  const token = payload?.waotoken
    ? payload?.waotoken
    : await getCookie("waotoken");
  const url = `${restBaseUrl}/esim/get-list`;
  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify({
      is_setting: payload?.is_setting,
    }),
  });

  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function getEsimPlans(payload: any) {
  const token = payload?.waotoken
    ? payload?.waotoken
    : await getCookie("waotoken");
  const url = `${restBaseUrl}/esim/get-esim-plans`;
  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify({
      iccid: payload?.iccid,
    }),
  });

  const res = await response.json();
  const finalRes = await transformGetEsimPlans(res?.data);

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isErrors: false,
    esimPlansResponse: finalRes,
  };
}


export async function handleOnContactUsFormWithOutLogin({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/home/contact-us`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}
