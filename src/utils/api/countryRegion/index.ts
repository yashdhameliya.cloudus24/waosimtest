import { transformPlanDetailsCountryRegion } from "@/utils/transform";
import { restBaseUrl, restBcHeaders } from "..";
import { getCookie } from "@/utils/useHooks/useCookies";

export async function getAllCountryRegion({ is_popular, page, search }: any) {
  const url = page
    ? `${restBaseUrl}/home/countries-regions?is_popular=${
        is_popular ? 1 : 0
      }&page=${page}`
    : `${restBaseUrl}/home/countries-regions?is_popular=${
        is_popular ? 1 : 0
      }&search=${search}`;

  const token = await getCookie("waotoken");

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "GET",
  });

  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function getPlanDetailsCountryRegion({
  mcc,
  regionId,
  plan_id,
  waotoken,
}: any) {
  let url = mcc
    ? `${restBaseUrl}/home/get-plans?mcc=${mcc}`
    : regionId
    ? `${restBaseUrl}/home/get-plans?region_id=${regionId}`
    : "";

  if (plan_id) {
    url = `${url}&plan_id=${plan_id}`;
  }
  const token = waotoken ? waotoken : await getCookie("waotoken");

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "GET",
  });

  const res = await response.json();
  const finalRes = await transformPlanDetailsCountryRegion(res?.data);

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: finalRes,
  };
}
