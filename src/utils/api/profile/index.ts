import { getCookie } from "@/utils/useHooks/useCookies";
import { restBaseUrl, restBcHeaders } from "..";
import { parseCookies } from "nookies";

export async function handleOnAarchivePlan({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/esim/archive-plan`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnDeletePlan({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/esim/delete-plan`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnCancelPlan({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/esim/cancel-plan`;
  const cookies = parseCookies();
  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnUpateEsimName({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/esim/change-esim-name`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnDeleteEsim({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/esim/delete`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnGetEsimStatusRouteApi({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/esim/get-esim-status`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
    newResponse: res,
  };
}

export async function handleOnTransferEsimToNewAccount({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/esim/transfer`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnHideUnhideEsim({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/esim/hide`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnReferralBalanceHistory({
  body,
  token,
}: {
  body: any;
  token: any;
}) {
  const url = `${restBaseUrl}/user/referral-balance-history`;

  const response = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${token}`, // Include token here
    },
    method: "POST",
    body: JSON.stringify(body),
  });
  const res = await response.json();

  if (response.status == 404) {
    return { isError: true, response: response };
  }

  return {
    isError: false,
    response: res,
  };
}

export async function handleOnFetchWaoClubData({
  body,
  waotoken,
}: {
  body?: any;
  waotoken: any;
}) {
  const url = `${restBaseUrl}/user/waoclub`;

  const waoClubResponse = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${waotoken}`, // Include token here
    },
    method: "POST",
    // body: JSON.stringify(body),
  });
  const res = await waoClubResponse.json();

  if (waoClubResponse.status == 404) {
    return { isError: true, waoClubResponse: waoClubResponse };
  }

  return {
    isError: false,
    waoClubResponse: res,
  };
}

export async function splashAPICall({
  body,
  waotoken,
}: {
  body?: any;
  waotoken: any;
}) {
  const url = `${restBaseUrl}/home/splash`;

  const splashResponse = await fetch(url, {
    headers: {
      ...restBcHeaders,
      Authorization: `Bearer ${waotoken}`, // Include token here
    },
    method: "POST",
    // body: JSON.stringify(body),
  });
  const res = await splashResponse.json();

  if (splashResponse.status == 404) {
    return { isError: true, splashResponse: splashResponse };
  }

  return {
    isError: false,
    splashResponse: res,
  };
}
