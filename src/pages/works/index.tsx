
import HowItsWorksBanner from "@/components/rendering/howItsWorksBanner";
import HowitsWorksDetails from "@/components/rendering/howitsWorksDetails";
import Head from "next/head";

export default function HowitsWorksDetailsPage({ response }: any) {
  return (
    <>
      <Head>
        <title>WaoSim | Works</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/faviconIcon.svg" />
      </Head>
      <main>
        <HowItsWorksBanner/>
        <HowitsWorksDetails/>
      </main>
    </>
  );
}

// export async function getServerSideProps({ req }: any) {
//   const { isError, response } = await getAllCountryRegion();

//   return {
//     props: {
//       response: response?.data,
//     },
//   };
// }
