import React from "react";
import Head from "next/head";
import ProfileLayout from "@/components/modules/profileLayout";
import NoProfileMenuData from "@/components/modules/noProfileMenuData";

export default function InvoicePage() {
  return (
    <>
      <Head>
        <title>WaoSim | Invoice</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/faviconIcon.svg" />
      </Head>
      <main>
        <ProfileLayout>
          <NoProfileMenuData
            mainTitle={`Invoice`}
            backgroundImage={
              "https://stagecdn.waosim.com/myprofile/noInvoiceCardImage.png"
            }
            buttonTitle={`Discover Now`}
            buttonLink={`/plans`}
          />
        </ProfileLayout>{" "}
      </main>
    </>
  );
}
