import {
  handleOnContactUsFormWithOutLogin,
  handleOnForgotPassword,
  handleOnOtpVerify,
  handleOnResendOtp,
  handleOnResetPassword,
  handleOnSingIn,
  handleOnSingUp,
} from "@/utils/api/auth";
import { NextApiRequest, NextApiResponse } from "next";
import { parseCookies } from "nookies";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const requestMethod = req.method;
  const body = req?.body !== "" ? JSON.parse(req?.body) : null;
  const query = req.query.api ?? "";
  const action = req.query.action ?? "";
  const cookies = parseCookies({ req });

  switch (query) {
    case "authentication":
      try {
        if (requestMethod === "POST" && action === "forgotPassword") {
          const { isError, response } = await handleOnForgotPassword({
            body: body,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "resendOtp") {
          const { isError, response } = await handleOnResendOtp({
            body: body,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "resetPassword") {
          const { isError, response } = await handleOnResetPassword({
            body: body,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "signin") {
          const { isError, response } = await handleOnSingIn({
            body: body,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "signupNewUser") {
          const { isError, response } = await handleOnSingUp({
            body: body,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "emailOtpVerify") {
          const { isError, response } = await handleOnOtpVerify({
            body: body,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "contact-us") {
          const { isError, response } = await handleOnContactUsFormWithOutLogin(
            {
              body: body,
              token: cookies?.waotoken,
            }
          );

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }
      } catch (err) {
        console.error(err);

        return {
          statusCode: 500,
          body: JSON.stringify({ msg: err }),
        };
      }
      break;
    default:
      return res.status(400).json({ isError: true });
  }
};

export default handler;
