import {
  handleOnAarchivePlan,
  handleOnCancelPlan,
  handleOnDeleteEsim,
  handleOnDeletePlan,
  handleOnGetEsimStatusRouteApi,
  handleOnHideUnhideEsim,
  handleOnReferralBalanceHistory,
  handleOnTransferEsimToNewAccount,
  handleOnUpateEsimName,
} from "@/utils/api/profile";
import { NextApiRequest, NextApiResponse } from "next";
import { parseCookies } from "nookies";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const requestMethod = req.method;
  const body = req?.body !== "" ? JSON.parse(req?.body) : null;
  const query = req.query.api ?? "";
  const action = req.query.action ?? "";
  const cookies = parseCookies({ req });

  switch (query) {
    case "profile":
      try {
        if (requestMethod === "POST" && action === "archive-plan") {
          const { isError, response } = await handleOnAarchivePlan({
            body: body,
            token: cookies?.waotoken,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "delete-plan") {
          const { isError, response } = await handleOnDeletePlan({
            body: body,
            token: cookies?.waotoken,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "cancel-plan") {
          const { isError, response } = await handleOnCancelPlan({
            body: body,
            token: cookies?.waotoken,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }
      } catch (err) {
        console.error(err);

        return {
          statusCode: 500,
          body: JSON.stringify({ msg: err }),
        };
      }
      break;

    case "eSIM":
      try {
        if (requestMethod === "POST" && action === "update-esim-name") {
          const { isError, response } = await handleOnUpateEsimName({
            body: body,
            token: cookies?.waotoken,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "delete-esim") {
          const { isError, response } = await handleOnDeleteEsim({
            body: body,
            token: cookies?.waotoken,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "get-esim-status") {
          const { isError, response } = await handleOnGetEsimStatusRouteApi({
            body: body,
            token: cookies?.waotoken,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "esim-transfer") {
          const { isError, response } = await handleOnTransferEsimToNewAccount({
            body: body,
            token: cookies?.waotoken,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }

        if (requestMethod === "POST" && action === "hide-unhide-esim") {
          const { isError, response } = await handleOnHideUnhideEsim({
            body: body,
            token: cookies?.waotoken,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }
      } catch (err) {
        console.error(err);

        return {
          statusCode: 500,
          body: JSON.stringify({ msg: err }),
        };
      }
      break;

    case "refer-and-earn":
      try {
        if (
          requestMethod === "POST" &&
          action === "get-referral-balance-history"
        ) {
          const { isError, response } = await handleOnReferralBalanceHistory({
            body: body,
            token: cookies?.waotoken,
          });

          if (!isError) {
            return res.status(200).json({ isError: false, response: response });
          }
          return res.status(500).json({ isError: true, response: response });
        }
      } catch (err) {
        console.error(err);

        return {
          statusCode: 500,
          body: JSON.stringify({ msg: err }),
        };
      }
      break;
    default:
      return res.status(400).json({ isError: true });
  }
};

export default handler;
