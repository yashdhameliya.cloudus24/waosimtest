import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx: any) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps, locale: ctx?.locale || "en" };
  }

  render() {
    const lang = this.props.locale;

    return (
      <Html lang={lang}>
        <Head>
          <link
            href="/faviconIcon.svg"
            rel="icon"
            type="image/ico"
            sizes="16x16"
          />
          <meta property="og:title" content="WaoSim" />
          <meta
            property="og:description"
            content="Description of your website."
          />

          <meta property="og:url" content="https://waosim.com/" />

          <meta
            property="og:image"
            content="https://stagecdn.waosim.com/landingpage/thumbnail.png?v=1"
          />
          <meta property="og:type" content="website" />

          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:title" content="WaoSim" />
          <meta
            name="twitter:description"
            content="Description of your website."
          />
          <meta
            name="twitter:image"
            content="https://stagecdn.waosim.com/landingpage/thumbnail.png?v=1"
          />

          <meta name="theme-color" content="#F8F3ED" />
        </Head>
        <body className="qdf-dokkan">
          <Main />
          <NextScript />
          <div id="modal-root"></div>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
