
import DeviceCompatibility from "@/components/rendering/deviceCompatibility";
import DeviceHeroBanner from "@/components/rendering/deviceHeroBanner";
import Head from "next/head";

export default function DeviceCompatibilityPage({ response }: any) {
  return (
    <>
      <Head>
        <title>WaoSim | Device Compatibility</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/faviconIcon.svg" />
      </Head>
      <main>
        <DeviceHeroBanner/>
       <DeviceCompatibility/>
      </main>
    </>
  );
}

// export async function getServerSideProps({ req }: any) {
//   const { isError, response } = await getAllCountryRegion();

//   return {
//     props: {
//       response: response?.data,
//     },
//   };
// }
